﻿using PlatformDiag.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlatformDiag
{
    public partial class DevicesForm : Form
    {
        //TODO: may be to stop listening insted of use static field
        public static bool _listen = false;

        public DevicesForm()
        {
            InitializeComponent();
        }

        private void DevicesForm_Load(object sender, EventArgs e)
        {
            if (!_listen)
            {
                TDSServiceAPI.StartListenPort(Settings.Default.ListeningPort);
                _listen = true;
            }

            ucDevice1.DeviceIP = Settings.Default.DeviceIP1;
            ucDevice2.DeviceIP = Settings.Default.DeviceIP2;
            ucDevice3.DeviceIP = Settings.Default.DeviceIP3;
            ucDevice4.DeviceIP = Settings.Default.DeviceIP4;

            ucDevice1.DeviceType = Settings.Default.DeviceType1;
            ucDevice2.DeviceType = Settings.Default.DeviceType2;
            ucDevice3.DeviceType = Settings.Default.DeviceType3;
            ucDevice4.DeviceType = Settings.Default.DeviceType4;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Settings.Default.DeviceIP1 = ucDevice1.DeviceIP;
            Settings.Default.DeviceIP2 = ucDevice2.DeviceIP;
            Settings.Default.DeviceIP3 = ucDevice3.DeviceIP;
            Settings.Default.DeviceIP4 = ucDevice4.DeviceIP;

            Settings.Default.DeviceType1 = ucDevice1.DeviceType;
            Settings.Default.DeviceType2 = ucDevice2.DeviceType;
            Settings.Default.DeviceType3 = ucDevice3.DeviceType;
            Settings.Default.DeviceType4 = ucDevice4.DeviceType;

            Settings.Default.Save();

            base.OnFormClosing(e);
        }
    }
}
