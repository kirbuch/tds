﻿namespace PlatformDiag
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnTest = new System.Windows.Forms.Button();
            this.groupBoxParams = new System.Windows.Forms.GroupBox();
            this.tbHeightPosition = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbTangag = new System.Windows.Forms.TextBox();
            this.tbKren = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbHeight = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSpeed = new System.Windows.Forms.TextBox();
            this.tbDirection = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numAzimuth = new System.Windows.Forms.NumericUpDown();
            this.numAngleSpeed = new System.Windows.Forms.NumericUpDown();
            this.btnSetAzimuth = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.numTangag = new System.Windows.Forms.NumericUpDown();
            this.numKren = new System.Windows.Forms.NumericUpDown();
            this.numHeight = new System.Windows.Forms.NumericUpDown();
            this.numHeightSpeed = new System.Windows.Forms.NumericUpDown();
            this.btnMoveAngle = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btnMoveHeight = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numRotation = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.numNewSpeed = new System.Windows.Forms.NumericUpDown();
            this.btnJump = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnBackword = new System.Windows.Forms.Button();
            this.cbAutoMotion = new System.Windows.Forms.CheckBox();
            this.btnForword = new System.Windows.Forms.Button();
            this.numJump = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSetKren = new System.Windows.Forms.Button();
            this.tbHost = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbGetLegParameters = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tbScale = new System.Windows.Forms.TextBox();
            this.tbLegSpeed = new System.Windows.Forms.TextBox();
            this.textBoxRightLeg = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxLeftLeg = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tbTangagMax = new System.Windows.Forms.TextBox();
            this.tbTangagMin = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnSetTangag = new System.Windows.Forms.Button();
            this.tbKrenMax = new System.Windows.Forms.TextBox();
            this.tbKrenMin = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnTestMode = new System.Windows.Forms.Button();
            this.btnWorkMode = new System.Windows.Forms.Button();
            this.btnUnblock = new System.Windows.Forms.Button();
            this.btnBlock = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.cbDirectConnection = new System.Windows.Forms.CheckBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbLog = new System.Windows.Forms.ListBox();
            this.timerUpdateLegs = new System.Windows.Forms.Timer(this.components);
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnSupplyComposition = new System.Windows.Forms.Button();
            this.btnSupplyWater = new System.Windows.Forms.Button();
            this.btnSupplyOff = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.tbСomposition = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbDispenser = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbValve = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbWaterP = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tbAirP1 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tbPulse1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tbAirP2 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tbPulse2 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tbWaterPDevice = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.tbValveDevice = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tbPump = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tbWaterV = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.btnDevice = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.tbSenderID = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tbMsgID = new System.Windows.Forms.TextBox();
            this.tbMsgParameter = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cbRecieveMsg = new System.Windows.Forms.CheckBox();
            this.btSendMsg = new System.Windows.Forms.Button();
            this.groupBoxParams.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAzimuth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAngleSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTangag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numKren)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeightSpeed)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRotation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNewSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numJump)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(275, 10);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(163, 23);
            this.btnTest.TabIndex = 0;
            this.btnTest.Text = "Подключиться";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // groupBoxParams
            // 
            this.groupBoxParams.Controls.Add(this.tbHeightPosition);
            this.groupBoxParams.Controls.Add(this.label4);
            this.groupBoxParams.Controls.Add(this.tbTangag);
            this.groupBoxParams.Controls.Add(this.tbKren);
            this.groupBoxParams.Controls.Add(this.label10);
            this.groupBoxParams.Controls.Add(this.label11);
            this.groupBoxParams.Controls.Add(this.tbHeight);
            this.groupBoxParams.Controls.Add(this.label3);
            this.groupBoxParams.Controls.Add(this.tbSpeed);
            this.groupBoxParams.Controls.Add(this.tbDirection);
            this.groupBoxParams.Controls.Add(this.label2);
            this.groupBoxParams.Controls.Add(this.label1);
            this.groupBoxParams.Location = new System.Drawing.Point(12, 41);
            this.groupBoxParams.Name = "groupBoxParams";
            this.groupBoxParams.Size = new System.Drawing.Size(432, 101);
            this.groupBoxParams.TabIndex = 1;
            this.groupBoxParams.TabStop = false;
            this.groupBoxParams.Text = "Параметры";
            // 
            // tbHeightPosition
            // 
            this.tbHeightPosition.Location = new System.Drawing.Point(326, 72);
            this.tbHeightPosition.Name = "tbHeightPosition";
            this.tbHeightPosition.ReadOnly = true;
            this.tbHeightPosition.Size = new System.Drawing.Size(100, 20);
            this.tbHeightPosition.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(241, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Высота пояса:";
            // 
            // tbTangag
            // 
            this.tbTangag.Location = new System.Drawing.Point(326, 46);
            this.tbTangag.Name = "tbTangag";
            this.tbTangag.ReadOnly = true;
            this.tbTangag.Size = new System.Drawing.Size(100, 20);
            this.tbTangag.TabIndex = 9;
            // 
            // tbKren
            // 
            this.tbKren.Location = new System.Drawing.Point(326, 20);
            this.tbKren.Name = "tbKren";
            this.tbKren.ReadOnly = true;
            this.tbKren.Size = new System.Drawing.Size(100, 20);
            this.tbKren.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(241, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Угол тангажа:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(242, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Угол крена:";
            // 
            // tbHeight
            // 
            this.tbHeight.Location = new System.Drawing.Point(91, 72);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.ReadOnly = true;
            this.tbHeight.Size = new System.Drawing.Size(100, 20);
            this.tbHeight.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Высота:";
            // 
            // tbSpeed
            // 
            this.tbSpeed.Location = new System.Drawing.Point(91, 46);
            this.tbSpeed.Name = "tbSpeed";
            this.tbSpeed.ReadOnly = true;
            this.tbSpeed.Size = new System.Drawing.Size(100, 20);
            this.tbSpeed.TabIndex = 3;
            // 
            // tbDirection
            // 
            this.tbDirection.Location = new System.Drawing.Point(91, 20);
            this.tbDirection.Name = "tbDirection";
            this.tbDirection.ReadOnly = true;
            this.tbDirection.Size = new System.Drawing.Size(100, 20);
            this.tbDirection.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Скорость:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Направление:";
            // 
            // timerUpdate
            // 
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numAzimuth);
            this.groupBox1.Controls.Add(this.numAngleSpeed);
            this.groupBox1.Controls.Add(this.btnSetAzimuth);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.numTangag);
            this.groupBox1.Controls.Add(this.numKren);
            this.groupBox1.Controls.Add(this.numHeight);
            this.groupBox1.Controls.Add(this.numHeightSpeed);
            this.groupBox1.Controls.Add(this.btnMoveAngle);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.btnMoveHeight);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(12, 148);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(432, 97);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Управление";
            // 
            // numAzimuth
            // 
            this.numAzimuth.Location = new System.Drawing.Point(248, 70);
            this.numAzimuth.Maximum = new decimal(new int[] {
            110,
            0,
            0,
            0});
            this.numAzimuth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numAzimuth.Name = "numAzimuth";
            this.numAzimuth.Size = new System.Drawing.Size(66, 20);
            this.numAzimuth.TabIndex = 67;
            this.numAzimuth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numAngleSpeed
            // 
            this.numAngleSpeed.Location = new System.Drawing.Point(248, 44);
            this.numAngleSpeed.Maximum = new decimal(new int[] {
            110,
            0,
            0,
            0});
            this.numAngleSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numAngleSpeed.Name = "numAngleSpeed";
            this.numAngleSpeed.Size = new System.Drawing.Size(66, 20);
            this.numAngleSpeed.TabIndex = 16;
            this.numAngleSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnSetAzimuth
            // 
            this.btnSetAzimuth.Location = new System.Drawing.Point(338, 67);
            this.btnSetAzimuth.Name = "btnSetAzimuth";
            this.btnSetAzimuth.Size = new System.Drawing.Size(88, 23);
            this.btnSetAzimuth.TabIndex = 66;
            this.btnSetAzimuth.Text = "Задать";
            this.btnSetAzimuth.UseVisualStyleBackColor = true;
            this.btnSetAzimuth.Click += new System.EventHandler(this.btnSetAzimuth_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(184, 72);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(47, 13);
            this.label41.TabIndex = 65;
            this.label41.Text = "Азимут:";
            // 
            // numTangag
            // 
            this.numTangag.Location = new System.Drawing.Point(91, 70);
            this.numTangag.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numTangag.Name = "numTangag";
            this.numTangag.Size = new System.Drawing.Size(66, 20);
            this.numTangag.TabIndex = 15;
            // 
            // numKren
            // 
            this.numKren.Location = new System.Drawing.Point(91, 44);
            this.numKren.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numKren.Name = "numKren";
            this.numKren.Size = new System.Drawing.Size(66, 20);
            this.numKren.TabIndex = 14;
            // 
            // numHeight
            // 
            this.numHeight.Location = new System.Drawing.Point(91, 18);
            this.numHeight.Maximum = new decimal(new int[] {
            185,
            0,
            0,
            0});
            this.numHeight.Minimum = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numHeight.Name = "numHeight";
            this.numHeight.Size = new System.Drawing.Size(66, 20);
            this.numHeight.TabIndex = 13;
            this.numHeight.Value = new decimal(new int[] {
            95,
            0,
            0,
            0});
            // 
            // numHeightSpeed
            // 
            this.numHeightSpeed.Location = new System.Drawing.Point(248, 18);
            this.numHeightSpeed.Maximum = new decimal(new int[] {
            110,
            0,
            0,
            0});
            this.numHeightSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numHeightSpeed.Name = "numHeightSpeed";
            this.numHeightSpeed.Size = new System.Drawing.Size(66, 20);
            this.numHeightSpeed.TabIndex = 12;
            this.numHeightSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnMoveAngle
            // 
            this.btnMoveAngle.Location = new System.Drawing.Point(338, 41);
            this.btnMoveAngle.Name = "btnMoveAngle";
            this.btnMoveAngle.Size = new System.Drawing.Size(88, 23);
            this.btnMoveAngle.TabIndex = 11;
            this.btnMoveAngle.Text = "Задать";
            this.btnMoveAngle.UseVisualStyleBackColor = true;
            this.btnMoveAngle.Click += new System.EventHandler(this.btnMoveAngle_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(168, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Скорость уг.:";
            // 
            // btnMoveHeight
            // 
            this.btnMoveHeight.Location = new System.Drawing.Point(338, 15);
            this.btnMoveHeight.Name = "btnMoveHeight";
            this.btnMoveHeight.Size = new System.Drawing.Size(88, 23);
            this.btnMoveHeight.TabIndex = 8;
            this.btnMoveHeight.Text = "Задать";
            this.btnMoveHeight.UseVisualStyleBackColor = true;
            this.btnMoveHeight.Click += new System.EventHandler(this.btnMoveHeight_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(184, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Скорость:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Тангаж:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Крен:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Высота:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numRotation);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.numNewSpeed);
            this.groupBox2.Controls.Add(this.btnJump);
            this.groupBox2.Controls.Add(this.btnRight);
            this.groupBox2.Controls.Add(this.btnLeft);
            this.groupBox2.Controls.Add(this.btnBackword);
            this.groupBox2.Controls.Add(this.cbAutoMotion);
            this.groupBox2.Controls.Add(this.btnForword);
            this.groupBox2.Controls.Add(this.numJump);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(12, 251);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(432, 144);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Макет";
            // 
            // numRotation
            // 
            this.numRotation.Location = new System.Drawing.Point(360, 72);
            this.numRotation.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numRotation.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numRotation.Name = "numRotation";
            this.numRotation.Size = new System.Drawing.Size(66, 20);
            this.numRotation.TabIndex = 18;
            this.numRotation.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(296, 74);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 13);
            this.label17.TabIndex = 17;
            this.label17.Text = "Поворот:";
            // 
            // numNewSpeed
            // 
            this.numNewSpeed.Location = new System.Drawing.Point(360, 43);
            this.numNewSpeed.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.numNewSpeed.Minimum = new decimal(new int[] {
            3000,
            0,
            0,
            -2147483648});
            this.numNewSpeed.Name = "numNewSpeed";
            this.numNewSpeed.Size = new System.Drawing.Size(66, 20);
            this.numNewSpeed.TabIndex = 16;
            this.numNewSpeed.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // btnJump
            // 
            this.btnJump.Location = new System.Drawing.Point(76, 112);
            this.btnJump.Name = "btnJump";
            this.btnJump.Size = new System.Drawing.Size(146, 23);
            this.btnJump.TabIndex = 12;
            this.btnJump.Text = "Высота";
            this.btnJump.UseVisualStyleBackColor = true;
            this.btnJump.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnJump_MouseDown);
            this.btnJump.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnJump_MouseUp);
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(197, 72);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(88, 23);
            this.btnRight.TabIndex = 11;
            this.btnRight.Text = "Вправо";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnRight_MouseDown);
            this.btnRight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnRight_MouseUp);
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(9, 72);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(88, 23);
            this.btnLeft.TabIndex = 10;
            this.btnLeft.Text = "Влево";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnLeft_MouseDown);
            this.btnLeft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnLeft_MouseUp);
            // 
            // btnBackword
            // 
            this.btnBackword.Location = new System.Drawing.Point(103, 72);
            this.btnBackword.Name = "btnBackword";
            this.btnBackword.Size = new System.Drawing.Size(88, 23);
            this.btnBackword.TabIndex = 9;
            this.btnBackword.Text = "Назад";
            this.btnBackword.UseVisualStyleBackColor = true;
            this.btnBackword.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnBackword_MouseDown);
            this.btnBackword.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnBackword_MouseUp);
            // 
            // cbAutoMotion
            // 
            this.cbAutoMotion.AutoSize = true;
            this.cbAutoMotion.Checked = true;
            this.cbAutoMotion.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoMotion.Location = new System.Drawing.Point(7, 20);
            this.cbAutoMotion.Name = "cbAutoMotion";
            this.cbAutoMotion.Size = new System.Drawing.Size(163, 17);
            this.cbAutoMotion.TabIndex = 0;
            this.cbAutoMotion.Text = "Автоматическое движение";
            this.cbAutoMotion.UseVisualStyleBackColor = true;
            this.cbAutoMotion.CheckedChanged += new System.EventHandler(this.cbAutoMotion_CheckedChanged);
            // 
            // btnForword
            // 
            this.btnForword.Location = new System.Drawing.Point(103, 43);
            this.btnForword.Name = "btnForword";
            this.btnForword.Size = new System.Drawing.Size(88, 23);
            this.btnForword.TabIndex = 8;
            this.btnForword.Text = "Вперед";
            this.btnForword.UseVisualStyleBackColor = true;
            this.btnForword.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnForword_MouseDown);
            this.btnForword.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnForword_MouseUp);
            // 
            // numJump
            // 
            this.numJump.Location = new System.Drawing.Point(360, 112);
            this.numJump.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
            this.numJump.Minimum = new decimal(new int[] {
            4095,
            0,
            0,
            -2147483648});
            this.numJump.Name = "numJump";
            this.numJump.Size = new System.Drawing.Size(66, 20);
            this.numJump.TabIndex = 13;
            this.numJump.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(296, 114);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Прыгнуть:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(296, 45);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Скорость:";
            // 
            // btnSetKren
            // 
            this.btnSetKren.Location = new System.Drawing.Point(278, 121);
            this.btnSetKren.Name = "btnSetKren";
            this.btnSetKren.Size = new System.Drawing.Size(88, 23);
            this.btnSetKren.TabIndex = 11;
            this.btnSetKren.Text = "Задать";
            this.btnSetKren.UseVisualStyleBackColor = true;
            this.btnSetKren.Click += new System.EventHandler(this.btnSetKren_Click);
            // 
            // tbHost
            // 
            this.tbHost.Location = new System.Drawing.Point(22, 12);
            this.tbHost.Name = "tbHost";
            this.tbHost.ShortcutsEnabled = false;
            this.tbHost.Size = new System.Drawing.Size(232, 20);
            this.tbHost.TabIndex = 18;
            this.tbHost.Text = "127.0.0.1";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(6, 20);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(176, 23);
            this.btnStart.TabIndex = 19;
            this.btnStart.Text = "Старт платформы";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbGetLegParameters);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.tbScale);
            this.groupBox3.Controls.Add(this.tbLegSpeed);
            this.groupBox3.Controls.Add(this.textBoxRightLeg);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.textBoxLeftLeg);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.tbTangagMax);
            this.groupBox3.Controls.Add(this.tbTangagMin);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.btnSetTangag);
            this.groupBox3.Controls.Add(this.tbKrenMax);
            this.groupBox3.Controls.Add(this.tbKrenMin);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.btnTestMode);
            this.groupBox3.Controls.Add(this.btnWorkMode);
            this.groupBox3.Controls.Add(this.btnUnblock);
            this.groupBox3.Controls.Add(this.btnBlock);
            this.groupBox3.Controls.Add(this.btnStop);
            this.groupBox3.Controls.Add(this.btnStart);
            this.groupBox3.Controls.Add(this.btnSetKren);
            this.groupBox3.Location = new System.Drawing.Point(451, 41);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(372, 354);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Управление";
            // 
            // cbGetLegParameters
            // 
            this.cbGetLegParameters.AutoSize = true;
            this.cbGetLegParameters.Location = new System.Drawing.Point(197, 294);
            this.cbGetLegParameters.Name = "cbGetLegParameters";
            this.cbGetLegParameters.Size = new System.Drawing.Size(133, 17);
            this.cbGetLegParameters.TabIndex = 49;
            this.cbGetLegParameters.Text = "Получать параметры";
            this.cbGetLegParameters.UseVisualStyleBackColor = true;
            this.cbGetLegParameters.CheckedChanged += new System.EventHandler(this.cbGetLegParameters_CheckedChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(277, 327);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(13, 13);
            this.label23.TabIndex = 48;
            this.label23.Text = "=";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(166, 327);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 13);
            this.label22.TabIndex = 47;
            this.label22.Text = "Масштаб:";
            // 
            // tbScale
            // 
            this.tbScale.Location = new System.Drawing.Point(225, 324);
            this.tbScale.Name = "tbScale";
            this.tbScale.Size = new System.Drawing.Size(47, 20);
            this.tbScale.TabIndex = 46;
            this.tbScale.Text = "1";
            this.tbScale.TextChanged += new System.EventHandler(this.tbScale_TextChanged);
            // 
            // tbLegSpeed
            // 
            this.tbLegSpeed.Location = new System.Drawing.Point(295, 324);
            this.tbLegSpeed.Name = "tbLegSpeed";
            this.tbLegSpeed.Size = new System.Drawing.Size(47, 20);
            this.tbLegSpeed.TabIndex = 44;
            // 
            // textBoxRightLeg
            // 
            this.textBoxRightLeg.Location = new System.Drawing.Point(77, 324);
            this.textBoxRightLeg.Name = "textBoxRightLeg";
            this.textBoxRightLeg.Size = new System.Drawing.Size(76, 20);
            this.textBoxRightLeg.TabIndex = 43;
            this.textBoxRightLeg.TextChanged += new System.EventHandler(this.textBoxLeg_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 325);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 13);
            this.label20.TabIndex = 42;
            this.label20.Text = "Правая нога:";
            // 
            // textBoxLeftLeg
            // 
            this.textBoxLeftLeg.Location = new System.Drawing.Point(77, 291);
            this.textBoxLeftLeg.Name = "textBoxLeftLeg";
            this.textBoxLeftLeg.Size = new System.Drawing.Size(76, 20);
            this.textBoxLeftLeg.TabIndex = 41;
            this.textBoxLeftLeg.TextChanged += new System.EventHandler(this.textBoxLeg_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 294);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 13);
            this.label21.TabIndex = 40;
            this.label21.Text = "Левая нога:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(196, 245);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(76, 20);
            this.textBox3.TabIndex = 38;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 248);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(138, 13);
            this.label19.TabIndex = 37;
            this.label19.Text = "Скорость центрирования:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(278, 243);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(88, 23);
            this.button3.TabIndex = 36;
            this.button3.Text = "Задать";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(196, 217);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(76, 20);
            this.textBox2.TabIndex = 35;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 220);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(164, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "Макс. скорость перемещения:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(278, 215);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(88, 23);
            this.button2.TabIndex = 33;
            this.button2.Text = "Задать";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(196, 189);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(76, 20);
            this.textBox1.TabIndex = 32;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 192);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(152, 13);
            this.label15.TabIndex = 31;
            this.label15.Text = "Макс. скорость отклонения:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(278, 187);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 23);
            this.button1.TabIndex = 30;
            this.button1.Text = "Задать";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // tbTangagMax
            // 
            this.tbTangagMax.Location = new System.Drawing.Point(197, 154);
            this.tbTangagMax.Name = "tbTangagMax";
            this.tbTangagMax.Size = new System.Drawing.Size(76, 20);
            this.tbTangagMax.TabIndex = 29;
            // 
            // tbTangagMin
            // 
            this.tbTangagMin.Location = new System.Drawing.Point(115, 154);
            this.tbTangagMin.Name = "tbTangagMin";
            this.tbTangagMin.Size = new System.Drawing.Size(76, 20);
            this.tbTangagMin.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 157);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(113, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Изменения тангажа:";
            // 
            // btnSetTangag
            // 
            this.btnSetTangag.Location = new System.Drawing.Point(278, 152);
            this.btnSetTangag.Name = "btnSetTangag";
            this.btnSetTangag.Size = new System.Drawing.Size(88, 23);
            this.btnSetTangag.TabIndex = 26;
            this.btnSetTangag.Text = "Задать";
            this.btnSetTangag.UseVisualStyleBackColor = true;
            this.btnSetTangag.Click += new System.EventHandler(this.btnSetTangag_Click);
            // 
            // tbKrenMax
            // 
            this.tbKrenMax.Location = new System.Drawing.Point(197, 123);
            this.tbKrenMax.Name = "tbKrenMax";
            this.tbKrenMax.Size = new System.Drawing.Size(76, 20);
            this.tbKrenMax.TabIndex = 25;
            // 
            // tbKrenMin
            // 
            this.tbKrenMin.Location = new System.Drawing.Point(115, 123);
            this.tbKrenMin.Name = "tbKrenMin";
            this.tbKrenMin.Size = new System.Drawing.Size(76, 20);
            this.tbKrenMin.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 126);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Изменения крена:";
            // 
            // btnTestMode
            // 
            this.btnTestMode.Location = new System.Drawing.Point(190, 84);
            this.btnTestMode.Name = "btnTestMode";
            this.btnTestMode.Size = new System.Drawing.Size(176, 23);
            this.btnTestMode.TabIndex = 24;
            this.btnTestMode.Text = "Тестовый режим";
            this.btnTestMode.UseVisualStyleBackColor = true;
            this.btnTestMode.Click += new System.EventHandler(this.btnTestMode_Click);
            // 
            // btnWorkMode
            // 
            this.btnWorkMode.Location = new System.Drawing.Point(6, 84);
            this.btnWorkMode.Name = "btnWorkMode";
            this.btnWorkMode.Size = new System.Drawing.Size(176, 23);
            this.btnWorkMode.TabIndex = 23;
            this.btnWorkMode.Text = "Рабочий режим";
            this.btnWorkMode.UseVisualStyleBackColor = true;
            this.btnWorkMode.Click += new System.EventHandler(this.btnWorkMode_Click);
            // 
            // btnUnblock
            // 
            this.btnUnblock.Location = new System.Drawing.Point(190, 52);
            this.btnUnblock.Name = "btnUnblock";
            this.btnUnblock.Size = new System.Drawing.Size(176, 23);
            this.btnUnblock.TabIndex = 22;
            this.btnUnblock.Text = "Разблокировать платформу";
            this.btnUnblock.UseVisualStyleBackColor = true;
            this.btnUnblock.Click += new System.EventHandler(this.btnUnblock_Click);
            // 
            // btnBlock
            // 
            this.btnBlock.Location = new System.Drawing.Point(6, 52);
            this.btnBlock.Name = "btnBlock";
            this.btnBlock.Size = new System.Drawing.Size(176, 23);
            this.btnBlock.TabIndex = 21;
            this.btnBlock.Text = "Заблокировать платформу";
            this.btnBlock.UseVisualStyleBackColor = true;
            this.btnBlock.Click += new System.EventHandler(this.btnBlock_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(190, 20);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(176, 23);
            this.btnStop.TabIndex = 20;
            this.btnStop.Text = "Cтоп платформы";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // cbDirectConnection
            // 
            this.cbDirectConnection.AutoSize = true;
            this.cbDirectConnection.Checked = true;
            this.cbDirectConnection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDirectConnection.Location = new System.Drawing.Point(451, 15);
            this.cbDirectConnection.Name = "cbDirectConnection";
            this.cbDirectConnection.Size = new System.Drawing.Size(129, 17);
            this.cbDirectConnection.TabIndex = 19;
            this.cbDirectConnection.Text = "Прямое соединение";
            this.cbDirectConnection.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbLog);
            this.groupBox4.Location = new System.Drawing.Point(12, 632);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(811, 92);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Лог";
            // 
            // lbLog
            // 
            this.lbLog.FormattingEnabled = true;
            this.lbLog.Location = new System.Drawing.Point(7, 16);
            this.lbLog.Name = "lbLog";
            this.lbLog.Size = new System.Drawing.Size(798, 69);
            this.lbLog.TabIndex = 0;
            // 
            // timerUpdateLegs
            // 
            this.timerUpdateLegs.Tick += new System.EventHandler(this.timerUpdateLegs_Tick);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnSupplyComposition);
            this.groupBox5.Controls.Add(this.btnSupplyWater);
            this.groupBox5.Controls.Add(this.btnSupplyOff);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.tbСomposition);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.tbDispenser);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.tbValve);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.tbWaterP);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Location = new System.Drawing.Point(12, 401);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(297, 114);
            this.groupBox5.TabIndex = 22;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Пожарный рукав VR";
            // 
            // btnSupplyComposition
            // 
            this.btnSupplyComposition.Location = new System.Drawing.Point(218, 87);
            this.btnSupplyComposition.Name = "btnSupplyComposition";
            this.btnSupplyComposition.Size = new System.Drawing.Size(75, 23);
            this.btnSupplyComposition.TabIndex = 62;
            this.btnSupplyComposition.Text = "Пена";
            this.btnSupplyComposition.UseVisualStyleBackColor = true;
            this.btnSupplyComposition.Click += new System.EventHandler(this.btnSupplyComposition_Click);
            // 
            // btnSupplyWater
            // 
            this.btnSupplyWater.Location = new System.Drawing.Point(218, 63);
            this.btnSupplyWater.Name = "btnSupplyWater";
            this.btnSupplyWater.Size = new System.Drawing.Size(75, 23);
            this.btnSupplyWater.TabIndex = 61;
            this.btnSupplyWater.Text = "Вода";
            this.btnSupplyWater.UseVisualStyleBackColor = true;
            this.btnSupplyWater.Click += new System.EventHandler(this.btnSupplyWater_Click);
            // 
            // btnSupplyOff
            // 
            this.btnSupplyOff.Location = new System.Drawing.Point(218, 39);
            this.btnSupplyOff.Name = "btnSupplyOff";
            this.btnSupplyOff.Size = new System.Drawing.Size(75, 23);
            this.btnSupplyOff.TabIndex = 60;
            this.btnSupplyOff.Text = "Отключить";
            this.btnSupplyOff.UseVisualStyleBackColor = true;
            this.btnSupplyOff.Click += new System.EventHandler(this.btnSupplyOff_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(210, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(83, 13);
            this.label28.TabIndex = 59;
            this.label28.Text = "Задать режим:";
            // 
            // tbСomposition
            // 
            this.tbСomposition.Location = new System.Drawing.Point(116, 88);
            this.tbСomposition.Name = "tbСomposition";
            this.tbСomposition.Size = new System.Drawing.Size(76, 20);
            this.tbСomposition.TabIndex = 57;
            this.tbСomposition.TextChanged += new System.EventHandler(this.tbСomposition_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 91);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(81, 13);
            this.label27.TabIndex = 56;
            this.label27.Text = "Состав смеси:";
            // 
            // tbDispenser
            // 
            this.tbDispenser.Location = new System.Drawing.Point(116, 65);
            this.tbDispenser.Name = "tbDispenser";
            this.tbDispenser.Size = new System.Drawing.Size(76, 20);
            this.tbDispenser.TabIndex = 55;
            this.tbDispenser.TextChanged += new System.EventHandler(this.tbDispenser_TextChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 68);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(78, 13);
            this.label26.TabIndex = 54;
            this.label26.Text = "Распылитель:";
            // 
            // tbValve
            // 
            this.tbValve.Location = new System.Drawing.Point(116, 42);
            this.tbValve.Name = "tbValve";
            this.tbValve.Size = new System.Drawing.Size(76, 20);
            this.tbValve.TabIndex = 53;
            this.tbValve.TextChanged += new System.EventHandler(this.tbValve_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 45);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(111, 13);
            this.label24.TabIndex = 52;
            this.label24.Text = "Открытие заслонки:";
            // 
            // tbWaterP
            // 
            this.tbWaterP.Location = new System.Drawing.Point(116, 19);
            this.tbWaterP.Name = "tbWaterP";
            this.tbWaterP.Size = new System.Drawing.Size(76, 20);
            this.tbWaterP.TabIndex = 51;
            this.tbWaterP.TextChanged += new System.EventHandler(this.tbWaterP_TextChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(90, 13);
            this.label25.TabIndex = 50;
            this.label25.Text = "Давление воды:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tbAirP1);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.tbPulse1);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Location = new System.Drawing.Point(553, 440);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(210, 62);
            this.groupBox6.TabIndex = 60;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Пожарный 1";
            // 
            // tbAirP1
            // 
            this.tbAirP1.Location = new System.Drawing.Point(128, 39);
            this.tbAirP1.Name = "tbAirP1";
            this.tbAirP1.Size = new System.Drawing.Size(76, 20);
            this.tbAirP1.TabIndex = 53;
            this.tbAirP1.TextChanged += new System.EventHandler(this.tbAirP1_TextChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(7, 42);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(115, 13);
            this.label32.TabIndex = 52;
            this.label32.Text = "Давление в баллоне:";
            // 
            // tbPulse1
            // 
            this.tbPulse1.Location = new System.Drawing.Point(128, 16);
            this.tbPulse1.Name = "tbPulse1";
            this.tbPulse1.Size = new System.Drawing.Size(76, 20);
            this.tbPulse1.TabIndex = 51;
            this.tbPulse1.TextChanged += new System.EventHandler(this.tbPulse1_TextChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(7, 19);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 13);
            this.label33.TabIndex = 50;
            this.label33.Text = "Пульс:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tbAirP2);
            this.groupBox7.Controls.Add(this.label29);
            this.groupBox7.Controls.Add(this.tbPulse2);
            this.groupBox7.Controls.Add(this.label30);
            this.groupBox7.Location = new System.Drawing.Point(553, 504);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(210, 62);
            this.groupBox7.TabIndex = 61;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Пожарный 2";
            // 
            // tbAirP2
            // 
            this.tbAirP2.Location = new System.Drawing.Point(128, 39);
            this.tbAirP2.Name = "tbAirP2";
            this.tbAirP2.Size = new System.Drawing.Size(76, 20);
            this.tbAirP2.TabIndex = 53;
            this.tbAirP2.TextChanged += new System.EventHandler(this.tbAirP2_TextChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 42);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(115, 13);
            this.label29.TabIndex = 52;
            this.label29.Text = "Давление в баллоне:";
            // 
            // tbPulse2
            // 
            this.tbPulse2.Location = new System.Drawing.Point(128, 13);
            this.tbPulse2.Name = "tbPulse2";
            this.tbPulse2.Size = new System.Drawing.Size(76, 20);
            this.tbPulse2.TabIndex = 51;
            this.tbPulse2.TextChanged += new System.EventHandler(this.tbPulse2_TextChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 19);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 13);
            this.label30.TabIndex = 50;
            this.label30.Text = "Пульс:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.tbWaterPDevice);
            this.groupBox8.Controls.Add(this.label40);
            this.groupBox8.Controls.Add(this.tbValveDevice);
            this.groupBox8.Controls.Add(this.label39);
            this.groupBox8.Controls.Add(this.tbPump);
            this.groupBox8.Controls.Add(this.label38);
            this.groupBox8.Controls.Add(this.textBox4);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Location = new System.Drawing.Point(321, 401);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(210, 114);
            this.groupBox8.TabIndex = 62;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "ПРО";
            // 
            // tbWaterPDevice
            // 
            this.tbWaterPDevice.Location = new System.Drawing.Point(127, 89);
            this.tbWaterPDevice.Name = "tbWaterPDevice";
            this.tbWaterPDevice.Size = new System.Drawing.Size(76, 20);
            this.tbWaterPDevice.TabIndex = 59;
            this.tbWaterPDevice.TextChanged += new System.EventHandler(this.tbWaterPDevice_TextChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 92);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(118, 13);
            this.label40.TabIndex = 58;
            this.label40.Text = "Производительность:";
            // 
            // tbValveDevice
            // 
            this.tbValveDevice.Location = new System.Drawing.Point(127, 65);
            this.tbValveDevice.Name = "tbValveDevice";
            this.tbValveDevice.Size = new System.Drawing.Size(76, 20);
            this.tbValveDevice.TabIndex = 57;
            this.tbValveDevice.TextChanged += new System.EventHandler(this.tbValveDevice_TextChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 68);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(119, 13);
            this.label39.TabIndex = 56;
            this.label39.Text = "Положение заслонки:";
            // 
            // tbPump
            // 
            this.tbPump.Location = new System.Drawing.Point(127, 42);
            this.tbPump.Name = "tbPump";
            this.tbPump.Size = new System.Drawing.Size(76, 20);
            this.tbPump.TabIndex = 55;
            this.tbPump.TextChanged += new System.EventHandler(this.tbPump_TextChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 45);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(100, 13);
            this.label38.TabIndex = 54;
            this.label38.Text = "Давление насоса:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(127, 19);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(76, 20);
            this.textBox4.TabIndex = 53;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 22);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(115, 13);
            this.label31.TabIndex = 52;
            this.label31.Text = "Давление в баллоне:";
            // 
            // tbWaterV
            // 
            this.tbWaterV.Location = new System.Drawing.Point(681, 413);
            this.tbWaterV.Name = "tbWaterV";
            this.tbWaterV.Size = new System.Drawing.Size(76, 20);
            this.tbWaterV.TabIndex = 51;
            this.tbWaterV.TextChanged += new System.EventHandler(this.tbWaterV_TextChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(560, 414);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(97, 13);
            this.label34.TabIndex = 50;
            this.label34.Text = "Объем цистерны:";
            // 
            // btnDevice
            // 
            this.btnDevice.Location = new System.Drawing.Point(553, 562);
            this.btnDevice.Name = "btnDevice";
            this.btnDevice.Size = new System.Drawing.Size(210, 67);
            this.btnDevice.TabIndex = 63;
            this.btnDevice.Text = "Устройства";
            this.btnDevice.UseVisualStyleBackColor = true;
            this.btnDevice.Click += new System.EventHandler(this.btnDevice_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label35);
            this.groupBox9.Controls.Add(this.tbSenderID);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Controls.Add(this.tbMsgID);
            this.groupBox9.Controls.Add(this.tbMsgParameter);
            this.groupBox9.Controls.Add(this.label37);
            this.groupBox9.Controls.Add(this.cbRecieveMsg);
            this.groupBox9.Controls.Add(this.btSendMsg);
            this.groupBox9.Location = new System.Drawing.Point(12, 529);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(355, 97);
            this.groupBox9.TabIndex = 64;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Сообщения";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(7, 26);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(55, 13);
            this.label35.TabIndex = 58;
            this.label35.Text = "SenderID:";
            // 
            // tbSenderID
            // 
            this.tbSenderID.Location = new System.Drawing.Point(116, 23);
            this.tbSenderID.Name = "tbSenderID";
            this.tbSenderID.Size = new System.Drawing.Size(76, 20);
            this.tbSenderID.TabIndex = 59;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 49);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 13);
            this.label36.TabIndex = 60;
            this.label36.Text = "MsgID:";
            // 
            // tbMsgID
            // 
            this.tbMsgID.Location = new System.Drawing.Point(116, 46);
            this.tbMsgID.Name = "tbMsgID";
            this.tbMsgID.Size = new System.Drawing.Size(76, 20);
            this.tbMsgID.TabIndex = 61;
            // 
            // tbMsgParameter
            // 
            this.tbMsgParameter.Location = new System.Drawing.Point(116, 69);
            this.tbMsgParameter.Name = "tbMsgParameter";
            this.tbMsgParameter.Size = new System.Drawing.Size(76, 20);
            this.tbMsgParameter.TabIndex = 63;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(7, 72);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(61, 13);
            this.label37.TabIndex = 62;
            this.label37.Text = "Параметр:";
            // 
            // cbRecieveMsg
            // 
            this.cbRecieveMsg.AutoSize = true;
            this.cbRecieveMsg.Location = new System.Drawing.Point(209, 68);
            this.cbRecieveMsg.Name = "cbRecieveMsg";
            this.cbRecieveMsg.Size = new System.Drawing.Size(133, 17);
            this.cbRecieveMsg.TabIndex = 20;
            this.cbRecieveMsg.Text = "Получать сообщения";
            this.cbRecieveMsg.UseVisualStyleBackColor = true;
            // 
            // btSendMsg
            // 
            this.btSendMsg.Location = new System.Drawing.Point(209, 23);
            this.btSendMsg.Name = "btSendMsg";
            this.btSendMsg.Size = new System.Drawing.Size(88, 23);
            this.btSendMsg.TabIndex = 19;
            this.btSendMsg.Text = "Отправить";
            this.btSendMsg.UseVisualStyleBackColor = true;
            this.btSendMsg.Click += new System.EventHandler(this.btSendMsg_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 730);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.btnDevice);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.tbWaterV);
            this.Controls.Add(this.tbHost);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxParams);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.cbDirectConnection);
            this.Name = "MainForm";
            this.Text = "Диагностика";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBoxParams.ResumeLayout(false);
            this.groupBoxParams.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAzimuth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAngleSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTangag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numKren)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeightSpeed)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRotation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNewSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numJump)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.GroupBox groupBoxParams;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbSpeed;
        private System.Windows.Forms.TextBox tbDirection;
        private System.Windows.Forms.TextBox tbHeight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer timerUpdate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnMoveAngle;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnMoveHeight;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbHeightPosition;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbTangag;
        private System.Windows.Forms.TextBox tbKren;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numAngleSpeed;
        private System.Windows.Forms.NumericUpDown numTangag;
        private System.Windows.Forms.NumericUpDown numKren;
        private System.Windows.Forms.NumericUpDown numHeight;
        private System.Windows.Forms.NumericUpDown numHeightSpeed;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnJump;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnBackword;
        private System.Windows.Forms.CheckBox cbAutoMotion;
        private System.Windows.Forms.Button btnForword;
        private System.Windows.Forms.NumericUpDown numNewSpeed;
        private System.Windows.Forms.NumericUpDown numJump;
        private System.Windows.Forms.Button btnSetKren;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numRotation;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbHost;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox lbLog;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnBlock;
        private System.Windows.Forms.Button btnUnblock;
        private System.Windows.Forms.CheckBox cbDirectConnection;
        private System.Windows.Forms.Button btnTestMode;
        private System.Windows.Forms.Button btnWorkMode;
        private System.Windows.Forms.TextBox tbKrenMax;
        private System.Windows.Forms.TextBox tbKrenMin;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbTangagMax;
        private System.Windows.Forms.TextBox tbTangagMin;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnSetTangag;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxRightLeg;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxLeftLeg;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Timer timerUpdateLegs;
        private System.Windows.Forms.TextBox tbLegSpeed;
        private System.Windows.Forms.TextBox tbScale;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox cbGetLegParameters;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tbСomposition;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbDispenser;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbValve;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbWaterP;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tbAirP1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tbPulse1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox tbAirP2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbPulse2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnSupplyComposition;
        private System.Windows.Forms.Button btnSupplyWater;
        private System.Windows.Forms.Button btnSupplyOff;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tbWaterV;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btnDevice;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox cbRecieveMsg;
        private System.Windows.Forms.Button btSendMsg;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tbSenderID;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbMsgID;
        private System.Windows.Forms.TextBox tbMsgParameter;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbPump;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbValveDevice;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbWaterPDevice;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.NumericUpDown numAzimuth;
        private System.Windows.Forms.Button btnSetAzimuth;
        private System.Windows.Forms.Label label41;
    }
}

