﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace PlatformDiag
{
    public interface ISetDeviceInfo
    {
        void SetFirehoseOutput(int value);
        void SetFirehoseValve(int value);
        void SetFirehoseDispenser(int value);
        void SetPulse(int person, int value);
    }

    public partial class MainForm : Form, ISetDeviceInfo
    {
        public static ISetDeviceInfo DeviceInfo;

        private IntPtr _task = IntPtr.Zero;

        double direction;
        double azimuth;
        double height;
        double height_position;
        double angle_kren;
        double angle_tangag;

        const int HdwPrecision = 1;

        DllFunc.FuncForManager _funcManager;

        double _old_height_position;

        bool DirectCommand { get { return cbDirectConnection.Checked; } }

        public MainForm()
        {
            InitializeComponent();
            UpdateAutoMode(cbAutoMotion.Checked);

            tbKrenMin.Text = _minKren.ToString();
            tbKrenMax.Text = _maxKren.ToString();
            tbTangagMin.Text = _minTangag.ToString();
            tbTangagMax.Text = _maxTangag.ToString();

            _funcManager = new DllFunc.FuncForManager();

            numHeightSpeed.Value = Properties.Settings.Default.HeightSpeed;
            numAngleSpeed.Value = Properties.Settings.Default.AngleSpeed;
        }

        //override On

        private void btnTest_Click(object sender, EventArgs e)
        {
            double version = TDSClientAPI.Version();

            if (_task == IntPtr.Zero)
            {
                _task = TDSClientAPI.StartTask(tbHost.Text);
                //if (_task == IntPtr.Zero)
                //{
                //    MessageBox.Show("Ошибка подклюбченгия к серверу");
                //    return;
                //}
                if (_task != IntPtr.Zero)
                    TDSServiceAPI.SetAutoMove(_task, false);
            }

            //TDSClientAPI.StopTask(task);

            timerUpdate.Start();
            UpdateParams();
        }

        int GetLegSpeed(string text)
        {
            if (string.IsNullOrEmpty(text) || text == "0")
                return 0;

            int result = 0;
            if (!int.TryParse(text.Substring(1), out result))
            {
                Log("Parse leg error:" + text);
            }

            return result;
        }

        bool _updateMode = false;

        private void UpdateParams()
        {
            _updateMode = true;

            direction = TDSClientAPI.Read_direction(_task);
            double speed = TDSClientAPI.Read_speed(_task);

            double height_old = height;
            height = TDSClientAPI.Read_height(_task);
            tbHeight.Text = height.ToString();

            height_position = TDSClientAPI.Read_height_position(_task);
            tbHeightPosition.Text = height_position.ToString();

            //Определяется через Азимут
            //double angle_kren_old = angle_kren;
            //angle_kren = TDSClientAPI.Read_angle_kren(_task);
            //tbKren.Text = angle_kren.ToString();
            //double angle_tangag_old = angle_tangag;
            //angle_tangag = TDSClientAPI.Read_angle_tangag(_task);
            //tbTangag.Text = angle_tangag.ToString();

            tbWaterP.Text = TDSClientAPI.Read_WaterP(_task).ToString();
            tbWaterV.Text = TDSClientAPI.Read_WaterV(_task).ToString();
            tbValve.Text = TDSClientAPI.Read_Valve(_task).ToString();
            tbDispenser.Text = TDSClientAPI.Read_Dispenser(_task).ToString();
            tbСomposition.Text = TDSClientAPI.Read_Composition(_task).ToString();

            tbPump.Text = TDSClientAPI.Read_PumpP(_task).ToString();

            tbPulse1.Text = TDSClientAPI.Read_Pulse(_task, 0).ToString();
            tbPulse2.Text = TDSClientAPI.Read_Pulse(_task, 1).ToString();
            tbAirP1.Text = TDSClientAPI.Read_AirP(_task, 0).ToString();
            tbAirP2.Text = TDSClientAPI.Read_AirP(_task, 1).ToString();

            if (Math.Abs(height_old - height) > 0.9)
            {
                double newAzimuth = 0;
                if (height >= 1)
                    newAzimuth = 9;
                else if (height <= -1)
                    newAzimuth = -9;

                UpdateAzimuth(newAzimuth);
            }

            if (DirectCommand)
            {
                speed = _legNewSpeed;
                if (_platformStarted)
                {
                    direction = _funcManager.PL_Read_direction();
                    speed = _funcManager.PL_Read_speed();
                }


                if (Connected /*&& cbAutoMotion.Checked*/)
                {
                    TDSServiceAPI.SetSpeed(_task, speed);
                    TDSServiceAPI.SetDirection(_task, direction);

                    TDSServiceAPI.PlatformParameters parameters = new TDSServiceAPI.PlatformParameters();
                    TDSServiceAPI.GetParameters2(_task, ref parameters);
                }
            }

            if (cbRecieveMsg.Checked)
                ReadEvents();

            tbDirection.Text = direction.ToString();
            tbSpeed.Text = speed.ToString();

            _updateMode = false;
        }

        private double _CR(double angle)
        {
            if (double.IsNaN(angle))
                return 0;
            if (angle > 9)
                return 9;
            if (angle < -9)
                return -9;
            return angle;
        }

        private void UpdateAzimuth(double newValue)
        {
            azimuth = newValue;

            double new_angle_kren = _CR(TDSServiceAPI.CalculateRoll(azimuth, direction));
            double new_angle_tangag = _CR(TDSServiceAPI.CalculatePitch(azimuth, direction));

            Log("Update azimuth: " + azimuth + " kren " + new_angle_kren + " tangag " + new_angle_tangag);

            if (Math.Abs(new_angle_kren - angle_kren) > 1)
            {
                angle_kren = new_angle_kren;
                numKren.Value = (int)new_angle_kren;
                if (_platformStarted)
                {
                    bool result = _funcManager.PL_Move_kren(angle_kren, AngleSpeed);
                    Log("PL_Move_kren: " + angle_kren + " " + AngleSpeed + " " + result);
                }
            }

            if (Math.Abs(new_angle_tangag - angle_tangag) > 1)
            {
                angle_tangag = new_angle_tangag;
                numTangag.Value = (int)new_angle_tangag;
                if (_platformStarted)
                {
                    bool result = _funcManager.PL_Move_tangag(angle_tangag, AngleSpeed);
                    Log("PL_Move_tangag: " + angle_tangag + " " + AngleSpeed + " " + result);
                }
            }
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            UpdateParams();
        }

        private void btnDevice_Click(object sender, EventArgs e)
        {
            DevicesForm deviceForm = new DevicesForm();

            deviceForm.Show();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            DeviceInfo = this;

            if (Properties.Settings.Default.AutoStart)
                Start();
        }

        private void Log(string text)
        {
            lbLog.Items.Insert(0, text);
        }

        private bool Connected { get { return _task != IntPtr.Zero; } }

        #region Platform

        double HeightSpeed { get { return (double)numHeightSpeed.Value; } }
        private void btnMoveHeight_Click(object sender, EventArgs e)
        {
            if (Connected)
            {
                TDSClientAPI.Move_height(_task, (double)numHeight.Value, (double)numHeightSpeed.Value);
            }

            if (DirectCommand)
            {
                bool result = _funcManager.PL_Move_height((double)numHeight.Value, HeightSpeed);
                Log("PL_Move_height " + numHeight.Value + " " + numHeightSpeed.Value + ": " + result);
            }
        }

        double AngleSpeed { get { return (double)numAngleSpeed.Value; } }
        private void btnMoveAngle_Click(object sender, EventArgs e)
        {
            if (Connected)
            {
                uint newTangag = (uint)numTangag.Value;
                uint newKren = (uint)numKren.Value;

                if (angle_tangag != newTangag && angle_kren != newKren)
                    TDSClientAPI.Move(_task, newKren, newTangag, AngleSpeed);
                else if (angle_tangag != newTangag)
                    TDSClientAPI.Move_tangag(_task, newTangag, AngleSpeed);
                else if (angle_kren != newKren)
                    TDSClientAPI.Move_kren(_task, newKren, AngleSpeed);
            }

            if (DirectCommand)
            {
                bool result = false;
                double speed = (double)numAngleSpeed.Value;
                double newTangag = (double)numTangag.Value;
                double newKren = (double)numKren.Value;

                if (angle_tangag != newTangag && angle_kren != newKren)
                    result = _funcManager.PL_Move(newKren, newTangag, speed);
                else if (angle_tangag != newTangag)
                    result = _funcManager.PL_Move_tangag(newTangag, speed);
                else if (angle_kren != newKren)
                    result = _funcManager.PL_Move_kren(newKren, speed);

                Log("_funcManager.PL_Move...: " + result);
            }
        }

        private void cbAutoMotion_CheckedChanged(object sender, EventArgs e)
        {
            UpdateAutoMode(cbAutoMotion.Checked);
        }

        private void UpdateAutoMode(bool autoMode)
        {
            btnForword.Enabled = !autoMode;
            btnBackword.Enabled = !autoMode;
            btnLeft.Enabled = !autoMode;
            btnRight.Enabled = !autoMode;
            btnJump.Enabled = !autoMode;
        }

        private void btnForword_MouseDown(object sender, MouseEventArgs e)
        {
            TDSServiceAPI.SetSpeed(_task, (int)numNewSpeed.Value);
        }
        private void btnForword_MouseUp(object sender, MouseEventArgs e)
        {
            TDSServiceAPI.SetSpeed(_task, 0);
        }

        private void btnBackword_MouseDown(object sender, MouseEventArgs e)
        {
            TDSServiceAPI.SetSpeed(_task, -(int)numNewSpeed.Value);
        }

        private void btnBackword_MouseUp(object sender, MouseEventArgs e)
        {
            TDSServiceAPI.SetSpeed(_task, 0);
        }

        private void btnLeft_MouseDown(object sender, MouseEventArgs e)
        {
            TDSServiceAPI.Rotate(_task, -(int)numRotation.Value);
        }

        private void btnLeft_MouseUp(object sender, MouseEventArgs e)
        {
            TDSServiceAPI.Rotate(_task, 0);
        }

        private void btnRight_MouseDown(object sender, MouseEventArgs e)
        {
            TDSServiceAPI.Rotate(_task, (int)numRotation.Value);

        }

        private void btnRight_MouseUp(object sender, MouseEventArgs e)
        {
            TDSServiceAPI.Rotate(_task, 0);
        }

        private void btnJump_MouseDown(object sender, MouseEventArgs e)
        {
            _old_height_position = height_position;
            int newValue = (int)_old_height_position + (int)numJump.Value;

            if (newValue < 0) newValue = 0;
            else if (newValue > 4095) newValue = 4095;

            TDSServiceAPI.Jump(_task, (uint)newValue, 4095);
        }

        private void btnJump_MouseUp(object sender, MouseEventArgs e)
        {
            TDSServiceAPI.Jump(_task, _old_height_position, 4095);
        }

        private bool _platformStarted = false;

        private void Start()
        {
            byte wrong_result = Connected ? TDSClientAPI.StartPlatform(_task) : (byte)100;
            Log("TDSClientAPI.StartPlatform: " + wrong_result);

            int result = _funcManager.PL_Start(); //PL_move.FuncForManager.PL _funcManager.PL_StartPlatform();
            Log("_funcManager.PL_StartPlatform: " + result);

            if (result == 1)
                _platformStarted = true;

            Log("Platform started: " + _platformStarted);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (Connected)
            {
                TDSClientAPI.StopTask(_task);
            }

            if (_platformStarted)
            {
                int result = _funcManager.PL_Stop();
                Log("_funcManager.PL_StopPlatform: " + result);
                _platformStarted = false;
            }

        }

        private void btnBlock_Click(object sender, EventArgs e)
        {
            //if (Connected) TDSClientAPI.Al(_task);

            if (DirectCommand)
            {
                bool result = _funcManager.PL_Alarm((char)1);
                Log("_funcManager.PL_Alarm 1: " + result);
            }
        }

        private void btnUnblock_Click(object sender, EventArgs e)
        {
            //if (Connected) TDSClientAPI.Al(_task);

            if (DirectCommand)
            {
                bool result = _funcManager.PL_Alarm((char)0);
                Log("_funcManager.PL_Alarm 0: " + result);
            }
        }

        private void btnWorkMode_Click(object sender, EventArgs e)
        {
            //if (Connected) TDSClientAPI.Al(_task);

            if (DirectCommand)
            {
                int result = _funcManager.PL_Set_mode((char)1);
                Log("_funcManager.PL_Set_mode 1: " + result);
            }
        }

        private void btnTestMode_Click(object sender, EventArgs e)
        {
            //if (Connected) TDSClientAPI.Al(_task);

            if (DirectCommand)
            {
                int result = _funcManager.PL_Set_mode((char)2);
                Log("_funcManager.PL_Set_mode 2: " + result);
            }
        }

        private double _minKren = -9.31;
        private double _maxKren = 9.31;
        private void btnSetKren_Click(object sender, EventArgs e)
        {
            _minKren = double.Parse(tbKrenMin.Text);
            _maxKren = double.Parse(tbKrenMax.Text);

            if (DirectCommand)
            {
                bool result = _funcManager.PL_Set_angle_kren(_minKren, _maxKren);
                Log("_funcManager.PL_Set_angle_kren: " + result);
            }
        }

        private double _minTangag = -10.72;
        private double _maxTangag = 10.72;
        private void btnSetTangag_Click(object sender, EventArgs e)
        {
            _minTangag = double.Parse(tbTangagMin.Text);
            _maxTangag = double.Parse(tbTangagMax.Text);

            if (DirectCommand)
            {
                bool result = _funcManager.PL_Set_angle_tangag(_minTangag, _maxTangag);
                Log("_funcManager.PL_Set_angle_Tangag: " + result);
            }
        }

        private void timerUpdateLegs_Tick(object sender, EventArgs e)
        {
            try
            {
                //string speedRight = _funcManager.PL_RE.PL_Read_RightLeg();
                //string speedLeft = _funcManager.PL_Read_LeftLeg();
                //textBoxRightLeg.Text = speedRight;
                //textBoxLeftLeg.Text = speedLeft;
                textBoxRightLeg.Text = "";
                textBoxLeftLeg.Text = "";

            }
            catch (Exception ex)
            {
                Log("Get parameters error: " + ex.Message);
            }
        }

        int _legNewSpeed;
        private void textBoxLeg_TextChanged(object sender, EventArgs e)
        {
            UpdateSpeed();
        }

        private void UpdateSpeed()
        {
            int right = GetLegSpeed(textBoxRightLeg.Text);
            int left = GetLegSpeed(textBoxLeftLeg.Text);
            _legNewSpeed = (int)((right + left) * _scale);

            tbLegSpeed.Text = _legNewSpeed.ToString();
        }

        float _scale = 1;

        private void tbScale_TextChanged(object sender, EventArgs e)
        {
            if (!float.TryParse(tbScale.Text, out _scale))
                Log("Scale error:" + tbScale.Text);

            UpdateSpeed();
        }

        private void cbGetLegParameters_CheckedChanged(object sender, EventArgs e)
        {
            timerUpdateLegs.Enabled = cbGetLegParameters.Checked;
        }

        private void btnSetAzimuth_Click(object sender, EventArgs e)
        {
            UpdateAzimuth((double)numAzimuth.Value);
        }

        #endregion

        #region Send data
        private void tbWaterP_TextChanged(object sender, EventArgs e)
        {
            UpdateWater(_updateMode);
        }

        private void UpdateWater(bool updateMode)
        {
            try
            {
                double waterP = double.Parse(tbWaterP.Text);
                if (Connected && !updateMode)
                {
                    TDSClientAPI.Write_WaterP(_task, waterP);
                }
            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }

        private void tbValve_TextChanged(object sender, EventArgs e)
        {
            UpdateValve(_updateMode);
        }

        private void UpdateValve(bool updateMode)
        {
            try
            {
                double value = double.Parse(tbValve.Text);
                if (Connected && !updateMode)
                {
                    TDSClientAPI.Write_Valve(_task, value);
                }
            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }


        private void tbDispenser_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double value = double.Parse(tbDispenser.Text);
                if (Connected && !_updateMode)
                {
                    TDSClientAPI.Write_Dispenser(_task, value);
                }
            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }

        private void tbСomposition_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double value = double.Parse(tbСomposition.Text);
                if (Connected && !_updateMode)
                {
                    TDSClientAPI.Write_Composition(_task, value);
                }
            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }

        private void tbPulse1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double value = double.Parse(tbPulse1.Text);
                if (Connected && !_updateMode)
                {
                    TDSClientAPI.Write_Pulse(_task, 0, value);
                }
            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }

        private void tbAirP1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double value = double.Parse(tbAirP1.Text);
                if (Connected && !_updateMode)
                {
                    TDSClientAPI.Write_AirP(_task, 0, value);
                }
            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }

        private void tbPulse2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double value = double.Parse(tbPulse2.Text);
                if (Connected && !_updateMode)
                {
                    TDSClientAPI.Write_Pulse(_task, 1, value);
                }
            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }

        private void tbAirP2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double value = double.Parse(tbAirP2.Text);
                if (Connected && !_updateMode)
                {
                    TDSClientAPI.Write_AirP(_task, 1, value);
                }
            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }

        private void tbWaterV_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double value = double.Parse(tbWaterV.Text);
                if (Connected && !_updateMode)
                {
                    TDSClientAPI.Write_WaterV(_task, value);
                }
            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }

        private void btnSupplyOff_Click(object sender, EventArgs e)
        {
            tbWaterP.Text = "0";
            tbValve.Text = "0";
            tbDispenser.Text = "2000";
            tbСomposition.Text = "0";
        }

        private void btnSupplyWater_Click(object sender, EventArgs e)
        {
            tbWaterP.Text = "7";
            tbValve.Text = "4095";
            tbDispenser.Text = "2000";
            tbСomposition.Text = "0";
        }

        private void btnSupplyComposition_Click(object sender, EventArgs e)
        {
            tbWaterP.Text = "7";
            tbValve.Text = "4095";
            tbDispenser.Text = "2000";
            tbСomposition.Text = "1";
        }

        double _waterPDevice;
        double _valve;
        double _pumpP = 1;

        private void tbPump_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double newPump = double.Parse(tbPump.Text);

                if (newPump == _pumpP)
                    return;

                _pumpP = double.Parse(tbPump.Text);
                UpdateWaterP();

                if (Connected && !_updateMode)
                {
                    TDSClientAPI.Write_PumpP(_task, _pumpP);
                }

                UpdateWater(false);
                UpdateValve(false);

            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }

        private void tbWaterPDevice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _waterPDevice = string.IsNullOrEmpty(tbWaterPDevice.Text) ? 0 : double.Parse(tbWaterPDevice.Text);
                UpdateWaterP();
            }
            catch (Exception ex)
            {
                Log("Set error : " + ex.Message);
            }
        }

        private void tbValveDevice_TextChanged(object sender, EventArgs e)
        {
            _valve = string.IsNullOrEmpty(tbValveDevice.Text) ? 0 : double.Parse(tbValveDevice.Text);
            UpdateWaterP();
        }


        public void SetFirehoseOutput(int value)
        {
            tbWaterPDevice.Text = value.ToString();
        }

        private void UpdateWaterP()
        {
            double waterP;
            double valveVR;
            if (_pumpP == 0)
            {
                waterP = 0;
                valveVR = 0;
            }
            else
            {
                double valve = 8.0 * _valve / 4095;
                valveVR = _valve;
                waterP = Math.Round(Math.Min(valve, _waterPDevice));
            }

            tbWaterP.Text = waterP.ToString();
            tbValve.Text = valveVR.ToString();
        }

        public void SetFirehoseValve(int value)
        {
            tbValveDevice.Text = value.ToString();
        }

        public void SetFirehoseDispenser(int value)
        {
            tbDispenser.Text = value.ToString();
        }

        public void SetPulse(int person, int value)
        {
            if (person == 0)
                tbPulse1.Text = value.ToString();
            else
                tbPulse2.Text = value.ToString();
        }

        private void btSendMsg_Click(object sender, EventArgs e)
        {
            ReadEvents();
            if (Connected)
            {
                TDSClientAPI.Event msg = new TDSClientAPI.Event();
                msg.senderID = int.Parse(tbSenderID.Text);
                msg.msgID = int.Parse(tbMsgID.Text);
                msg.parameter = int.Parse(tbMsgParameter.Text);
                TDSClientAPI.RaiseEvent2(_task, msg);
            }
        }

        private void ReadEvents()
        {
            if (Connected)
            {
                TDSClientAPI.Event msg = new TDSClientAPI.Event();
                while (TDSClientAPI.PopEvent(_task, ref msg) > 0)
                    Log("Events. Sender: " + msg.senderID + " msgID: " + msg.msgID + " parameter: " + msg.parameter);
            }
        }

        #endregion


    }
}
