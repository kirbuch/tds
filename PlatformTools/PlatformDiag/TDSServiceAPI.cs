﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using PlatformDiag.Properties;

namespace PlatformDiag
{
    class TDSServiceAPI
    {
        public struct PlatformParameters
        {
            public bool Started;
            public char Alarm; // 1 - тревога(аварийный останов), 0 - отмена аварии
            public char Mode; // 1 - рабочий режим, 2 - режим настройки и тестирования

            public double angle_kren_min;
            public double angle_kren_max;
            public double angle_kren;
            public double angle_kren_target;
            public double angle_kren_speed;

            public double angle_tangag_min;
            public double angle_tangag_max;
            public double angle_tangag;
            public double angle_tangag_target;
            public double angle_tangag_speed;

            public double maxSpeed_deviation;
            public double maxSpeed_move;
            public double Speed_centering;

            public double Height; //текущее значение высоты подвижности, значения от -34мм до 45мм
            public double Height_target;
            public double Height_speed;

            public double Direction; //Моделируется 12-ти битное значение от 0 до 4095	(4095 = 360)
            public double Speed; //Моделируется  мм*с, отрицательное значение будет означать «пятиться назад»
            public double Height_Position; //Моделируется 12-ти битное значение от 0 до 4095};

            public bool _autoMove;
        };

        internal static void StartListenPort(Settings @default)
        {
            throw new NotImplementedException();
        }

        [DllImport("TDSClient.dll")]
        public static extern void GetParameters2(IntPtr task, ref PlatformParameters parameters);

        //public enum EParameters
        //{
        //    PARAM_AutoMove = -100,
        //    PARAM_Jump,
        //    PARAM_Speed,
        //    PARAM_Rotate,
        //    PARAM_Direction,
        //    PARAM_NO_CHANGES = 0,
        //    PARAM_Height = 1,
        //    PARAM_Kren,
        //    PARAM_Tangag,
        //    PARAM_Angle
        //};

        //public struct ParametersCmd
        //{
        //    public EParameters Parameter;
        //    public double x;
        //    public double y;
        //    public double s;
        //};
        //[DllImport("TDSClient.dll")]
        //public static extern ParametersCmd PopParameterChanges(IntPtr task);


        [DllImport("TDSClient.dll")]
        public static extern bool SetAutoMove(IntPtr task, bool autoMove);

        [DllImport("TDSClient.dll")]
        public static extern bool Jump(IntPtr task, double height, double speed);

        [DllImport("TDSClient.dll")]
        public static extern bool SetSpeed(IntPtr task, double speed);

        [DllImport("TDSClient.dll")]
        public static extern bool SetDirection(IntPtr task, double direction);

        [DllImport("TDSClient.dll")]
        public static extern bool Rotate(IntPtr task, double rotateSpeed);


        [DllImport("TDSClient.dll")]
        public static extern IntPtr CreateDevice(string server, int port, byte typeId);

        [DllImport("TDSClient.dll")]
        public static extern int GetDeviceCount();

        [DllImport("TDSClient.dll")]
        public static extern char GetDeviceCode(IntPtr device);

        [DllImport("TDSClient.dll")]
        
        public static extern void GetDeviceParameters(IntPtr device, /*[MarshalAsAttribute(UnmanagedType.LPArray, SizeConst = 8)]*/ int[] parameters);

        [DllImport("TDSClient.dll")]
        public static extern void SetDeviceOn(IntPtr device, bool isOn);

        [DllImport("TDSClient.dll")]
        public static extern void SetDeviceParam(IntPtr device, int parameter);

        [DllImport("TDSClient.dll")]
        public static extern long GetLastUpdateTime(IntPtr device);

        [DllImport("TDSClient.dll")]
        public static extern void DestroyDevice(IntPtr device);

        [DllImport("TDSClient.dll")]
        public static extern void StartListenPort(int port);

        [DllImport("TDSClient.dll")]
        public static extern void StopListenPort();

        [DllImport("TDSClient.dll")]
        public static extern double CalculateRoll(double alpha, double beta);

        [DllImport("TDSClient.dll")]
        public static extern double CalculatePitch(double alpha, double beta);


    }
}
