﻿namespace PlatformDiag
{
    partial class DevicesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucDevice4 = new PlatformDiag.UCDevice();
            this.ucDevice3 = new PlatformDiag.UCDevice();
            this.ucDevice2 = new PlatformDiag.UCDevice();
            this.ucDevice1 = new PlatformDiag.UCDevice();
            this.tbHost = new System.Windows.Forms.TextBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.cbDirectConnection = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // ucDevice4
            // 
            this.ucDevice4.DeviceIP = "192.168.13.42";
            this.ucDevice4.DeviceType = 0;
            this.ucDevice4.Location = new System.Drawing.Point(418, 270);
            this.ucDevice4.Name = "ucDevice4";
            this.ucDevice4.Size = new System.Drawing.Size(410, 228);
            this.ucDevice4.TabIndex = 3;
            // 
            // ucDevice3
            // 
            this.ucDevice3.DeviceIP = "192.168.13.42";
            this.ucDevice3.DeviceType = 0;
            this.ucDevice3.Location = new System.Drawing.Point(2, 270);
            this.ucDevice3.Name = "ucDevice3";
            this.ucDevice3.Size = new System.Drawing.Size(410, 228);
            this.ucDevice3.TabIndex = 2;
            // 
            // ucDevice2
            // 
            this.ucDevice2.DeviceIP = "192.168.13.42";
            this.ucDevice2.DeviceType = 0;
            this.ucDevice2.Location = new System.Drawing.Point(418, 36);
            this.ucDevice2.Name = "ucDevice2";
            this.ucDevice2.Size = new System.Drawing.Size(410, 228);
            this.ucDevice2.TabIndex = 1;
            // 
            // ucDevice1
            // 
            this.ucDevice1.DeviceIP = "192.168.13.42";
            this.ucDevice1.DeviceType = 0;
            this.ucDevice1.Location = new System.Drawing.Point(2, 36);
            this.ucDevice1.Name = "ucDevice1";
            this.ucDevice1.Size = new System.Drawing.Size(410, 228);
            this.ucDevice1.TabIndex = 0;
            // 
            // tbHost
            // 
            this.tbHost.Enabled = false;
            this.tbHost.Location = new System.Drawing.Point(7, 9);
            this.tbHost.Name = "tbHost";
            this.tbHost.ShortcutsEnabled = false;
            this.tbHost.Size = new System.Drawing.Size(232, 20);
            this.tbHost.TabIndex = 21;
            this.tbHost.Text = "127.0.0.1";
            // 
            // btnTest
            // 
            this.btnTest.Enabled = false;
            this.btnTest.Location = new System.Drawing.Point(245, 8);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(163, 23);
            this.btnTest.TabIndex = 20;
            this.btnTest.Text = "Подключиться";
            this.btnTest.UseVisualStyleBackColor = true;
            // 
            // cbDirectConnection
            // 
            this.cbDirectConnection.AutoSize = true;
            this.cbDirectConnection.Checked = true;
            this.cbDirectConnection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDirectConnection.Enabled = false;
            this.cbDirectConnection.Location = new System.Drawing.Point(414, 11);
            this.cbDirectConnection.Name = "cbDirectConnection";
            this.cbDirectConnection.Size = new System.Drawing.Size(129, 17);
            this.cbDirectConnection.TabIndex = 22;
            this.cbDirectConnection.Text = "Прямое соединение";
            this.cbDirectConnection.UseVisualStyleBackColor = true;
            // 
            // DevicesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 509);
            this.Controls.Add(this.tbHost);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.cbDirectConnection);
            this.Controls.Add(this.ucDevice4);
            this.Controls.Add(this.ucDevice3);
            this.Controls.Add(this.ucDevice2);
            this.Controls.Add(this.ucDevice1);
            this.Name = "DevicesForm";
            this.Text = "DevicesForm";
            this.Load += new System.EventHandler(this.DevicesForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UCDevice ucDevice1;
        private UCDevice ucDevice2;
        private UCDevice ucDevice3;
        private UCDevice ucDevice4;
        private System.Windows.Forms.TextBox tbHost;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.CheckBox cbDirectConnection;
    }
}