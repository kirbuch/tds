﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformDiag
{
    public class MTools
    {
        public static double GetLineValue(double x, double y0, double y1, double x0, double x1)
        {
            return (y1 - y0) * (x - x0) / (x1 - x0) + y0;
        }
        public static double GetLineValueS(double x, double y0, double y1, double x0, double x1)
        {
            if (Math.Sign(x - x0) != Math.Sign(x - x1))
                return GetLineValue(x, y0, y1, x0, x1);

            if (x0 < x1)
                return (x < x0) ? y0 : y1;
            else
                return (x > x0) ? y0 : y1;
        }

    }
}
