﻿using System;
using System.Runtime.InteropServices;

namespace PlatformDiag
{
    class PL_API
    {
        /// <summary>
        /// производит включение изделия, начальную инициализацию устройств,//ЗАМЕНА ВОЗВРАЩАЕМОГО ТИПА(был char)
        ///платформа не выходит из парковочного состояния.
        /// </summary>
        /// <returns>возвращает 1(единицу) в случае успешной инициализации устройств или код ошибки</returns>
        [DllImport("PL_move.dll")]
        public static extern int PL_StartPlatform();
        /// <summary>
        /// переводит платформу в парковочное состояние и производит корректное завершение
        /// работы устройств
        /// </summary>
        /// <returns>возвращает 1(единицу) в случае успешного завершения работы устройств или код ошибки</returns>
        [DllImport("PL_move.dll")]
        public static extern char PL_StopPlatform();

		/// <summary>
		/// блокирует платформу в текущем положении до поступления сигнала о снятии тревоги//ФУНКЦИЯ НЕ РЕАЛИЗОВАНА
		/// </summary>
		/// <param name="data">1 - тревога (аварийный останов), 0 - отмена аварии.</param>
		/// <returns>Возвращает 0 в случае удачи и 1 в случае не удачи</returns>
		[DllImport("PL_move.dll")]
        public static extern bool PL_Alarm(char data);

		/// <summary>
		/// задаёт режим работы.
		/// различие режимов состоит в том, что в рабочем режиме нельзя менять настройки
		/// функции настроек (значение записываются в файл и используются при работе платформы во всех режимах)
		/// </summary>
		/// <param name="data">1 - рабочий режим, 2 - режим настройки и тестирования</param>
		/// <returns>возвращает 0 в случае успешного включения режима или код ошибки</returns>
		[DllImport("PL_move.dll")] 
		public static extern char PL_Set_mode(char data);

		[DllImport("PL_move.dll")] 
		public static extern bool PL_Set_angle_kren(double xmin, double xmax);//ЗАМЕНА ТИПА ПАРАМЕТРОВ. Принимаемые значения -9,31 до 9,31
		[DllImport("PL_move.dll")] 
		public static extern bool PL_Set_angle_tangag(double ymin, double ymax);//ЗАМЕНА ТИПА ПАРАМЕТРОВ. Принимаемые значения -10,72 до 10,72
																				//0 в обоих случаях будет означать среднее положение
		[DllImport("PL_move.dll")] 
		public static extern bool PL_Set_maxSpeed_deviation(double s);//установить макс. скорость отклонения (наклона) мм*с.//ЗАМЕНА ТИПА ПАРАМЕТРОВ. Минимальная скорость 9.127 мм*с, макс. 91.27 мм*с
		
		[DllImport("PL_move.dll")]
		public static extern bool PL_Set_maxSpeed_move(double s);//установить макс. скорость перемещения (изменение положения по вертикали) мм*с.//ЗАМЕНА ТИПА ПАРАМЕТРОВ. Минимальная скорость 9.127 мм*с, макс. 91.27 мм*с
		[DllImport("PL_move.dll")]
		public static extern bool PL_Set_Speed_centering(double s);//установить скорость центрирования//ЗАМЕНА ТИПА ПАРАМЕТРОВ. Минимальная скорость 9.127 мм*с, макс. 91.27 мм*с
																   //функции информирования о состоянии
																   //TODO: Выбрать формат строк
																   //string Read_status();//возвращает текущее состояние 0 - норма или код аварии

		[DllImport("PL_move.dll")]
		public static extern uint PL_Read_direction();//возвращает текущее направление движения игрока (12-ти битное значение от 0 до 4095	(4095 = 360))//ФУНКЦИЯ НЕ РЕАЛИЗОВАНА
		[DllImport("PL_move.dll")]
		public static extern int PL_Read_speed();//возвращает текущее значение скорости перемещения игрока (шага) мм*с, отрицательное значение//ФУНКЦИЯ НЕ РЕАЛИЗОВАНА
												 //будет означать «пятиться назад»

		[DllImport("PL_move.dll")]
		public static extern uint PL_Read_height_position();//возвращает текущее значение высоты пояса игрока (12-ти битное значение от 0 до 4095) //Высота пояса//ФУНКЦИЯ НЕ РЕАЛИЗОВАНА
															//калибровку предлагаю проводить под каждого игрока, т.е. перед запуском программы
															//считываем текущее положение высоты, отклонение от этого значения более чем на 100 мм
															//вниз считаем присядом, выше прыжком (100мм для примера)
		[DllImport("PL_move.dll")]
		public static extern int PL_Read_height();//возвращает текущее значение высоты подвижности, значения от -34мм до 45мм. Высота платформы//ФУНКЦИЯ НЕ РЕАЛИЗОВАНА
		
		[DllImport("PL_move.dll")]
		public static extern uint PL_Read_angle_kren();//возвращает текущее значение угла крена, от 0 до 360.//ФУНКЦИЯ НЕ РЕАЛИЗОВАНА
		[DllImport("PL_move.dll")]
		public static extern uint PL_Read_angle_tangag();//возвращает текущее значение угла тангажа, от 0 до 360.//ФУНКЦИЯ НЕ РЕАЛИЗОВАНА
														 //функции команды
		/// <summary>
		/// задаёт изменение крена на указанную величину и с заданной скоростью//ЗАМЕНА ТИПА ПАРАМЕТРОВ.
		/// </summary>
		/// <param name="x">величина крена</param>
		/// <param name="s">скорость изменения</param>
		/// <returns></returns>
		[DllImport("PL_move.dll")]
		public static extern bool PL_Move_kren(double x, double s);
		[DllImport("PL_move.dll")]
		public static extern bool PL_Move_tangag(double y, double s);//задаёт изменение крена на указанную величину и с заданной скоростью//ЗАМЕНА ТИПА ПАРАМЕТРОВ.
		[DllImport("PL_move.dll")]
		public static extern bool PL_Move_height(double x, double s);//задаёт изменение высоты на указанную величину и с заданной скоростью//ЗАМЕНА ТИПА ПАРАМЕТРОВ.
		[DllImport("PL_move.dll")]
		public static extern bool PL_Move(double x, double y, double s);//задаёт изменение по двум координатам на указанную величину и с заданной скоростью//ЗАМЕНА ТИПА ПАРАМЕТРОВ.
																		//автоматические функции
		[DllImport("PL_move.dll")]
		public static extern bool PL_Auto_centr();//автоматического центрирования по вертикали//ФУНКЦИЯ НЕ РЕАЛИЗОВАНА
		[DllImport("PL_move.dll")]
		public static extern bool PL_Auto_horizon();//автоматического приведения в горизонтальное состояние//ФУНКЦИЯ НЕ РЕАЛИЗОВАНА
	}
}
