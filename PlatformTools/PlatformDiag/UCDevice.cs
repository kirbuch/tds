﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using PlatformDiag.Properties;

namespace PlatformDiag
{
    public sealed class Device
    {
        public const int Firehose = 0;
        public const int Fireman0 = 1;
        public const int Fireman1 = 2;
    }

    public partial class UCDevice : UserControl
    {
        Label[] _lbls;
        TextBox[] _tbParams;

        string[] _names1;
        string[] _names2;
        string[] _names3;
        string[] _names4;
        string[][] _devices;

        private IntPtr _device;

        int _paramValue = 0;

        public string DeviceIP
        {
            set { tbAdress.Text = value; }
            get { return tbAdress.Text; }
        }

        public int DeviceType
        {
            set { cbDeviceType.SelectedIndex = value; }
            get { return cbDeviceType.SelectedIndex; }
        }

        public UCDevice()
        {
            InitializeComponent();

            btnOff.Visible = false;

            _lbls = new Label[] { lblP1, lblP2, label4, label5, label9, label8, label7, label6 };
            _tbParams = new TextBox[] { tbP1, tbP2, tbP3, tbP4, tbP5, tbP6, tbP7, tbP8 };

            _names1 = new string[] { "Готовность", "Запущено", "Весовое измерение", "Сила", "Заслонка", "Производительность", "Распылитель", "Ошибка", "Нагрузка" };
            _names2 = new string[] { "Нагрев", "Температура", "Резерв", "Пульс", "Резерв", "Резерв", "Резерв", "Ошибка", "Целевая темп." };
            _names3 = _names2;
            //_names3 = new string[] { "выкл. Нагрев", "вкл. нагрев", "Температура", "Целевая темп.", "Пульс", "Вентель", "Резерв", "Ошибка", "Целевая темп." };
            _names4 = new string[] { "Выключен", "Запущен", "Режим", "Резерв", "Резерв", "Резерв", "Резерв", "Ошибка", "Режим" };

            _devices = new string[][] { _names1, _names2, _names3, _names4 };

            cbDeviceType.SelectedIndex = 0;
            _device = IntPtr.Zero;

            tbP4.TextChanged += TbP4_TextChanged;
            tbP5.TextChanged += TbP5_TextChanged;
            tbP6.TextChanged += outputChanged;
            tbP7.TextChanged += dispencerChanged;
        }
        private void TbP4_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbP4.Text) && (DeviceType == Device.Fireman0 || DeviceType == Device.Fireman1))
            {
                int value = int.Parse(tbP4.Text);
                MainForm.DeviceInfo.SetPulse(DeviceType == Device.Fireman0 ? 0 : 1, value);
            }
        }

        private void TbP5_TextChanged(object sender, EventArgs e)
        {
            if (cbDeviceType.SelectedIndex == 0 && !string.IsNullOrEmpty(tbP5.Text))
            {
                int value = int.Parse(tbP5.Text);
                int newValue = (int)MTools.GetLineValueS(value, 0, 4095, Settings.Default.FirehoseValveLeft, Settings.Default.FirehoseValveRight);
                MainForm.DeviceInfo.SetFirehoseValve(newValue);
            }
        }

        private void outputChanged(object sender, EventArgs e)
        {
            if (cbDeviceType.SelectedIndex == 0 && !string.IsNullOrEmpty(tbP6.Text))
            {
                int value = int.Parse(tbP6.Text);
                int newValue = (int)MTools.GetLineValueS(value, 2, 8, Settings.Default.FirehoseOutputLeft, Settings.Default.FirehoseOutputRight);
                MainForm.DeviceInfo.SetFirehoseOutput(newValue);
            }
        }

        private void dispencerChanged(object sender, EventArgs e)
        {
            if (cbDeviceType.SelectedIndex == 0 && !string.IsNullOrEmpty(tbP7.Text))
                MainForm.DeviceInfo.SetFirehoseDispenser(int.Parse(tbP7.Text));
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < 8; i++)
            {
                _lbls[i].Text = _devices[cbDeviceType.SelectedIndex][i];
            }

            lblParameterName.Text = _devices[cbDeviceType.SelectedIndex][8];
        }

        private void btnAttach_Click(object sender, EventArgs e)
        {
            try
            {
                byte deviceType = (byte)(cbDeviceType.SelectedIndex + 1);
                if (deviceType >= 3) deviceType--; // Убираем лишний нагреватель
                _device = TDSServiceAPI.CreateDevice(tbAdress.Text, Settings.Default.DevicePort, deviceType);
                TDSServiceAPI.SetDeviceOn(_device, cbOn.Checked);
                TDSServiceAPI.SetDeviceParam(_device, _paramValue);

                btnAttach.Visible = false;
                btnOff.Visible = true;

                lbLog.Items.Add("Connect " + tbAdress.Text + ":" + Settings.Default.DevicePort);
            }
            catch (Exception ex)
            {
                lbLog.Items.Add("Error:" + ex.Message);
            }
        }

        private void btnOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (_device != IntPtr.Zero)
                    TDSServiceAPI.DestroyDevice(_device);

                _device = IntPtr.Zero;

                btnAttach.Visible = true;
                btnOff.Visible = false;

                lbLog.Items.Add("Disconnect");
            }
            catch (Exception ex)
            {
                lbLog.Items.Add("Error:" + ex.Message);
            }

        }

        private void cbOn_CheckedChanged(object sender, EventArgs e)
        {
            if (_device != IntPtr.Zero)
                TDSServiceAPI.SetDeviceOn(_device, cbOn.Checked);
        }

        private void tbParameters_TextChanged(object sender, EventArgs e)
        {
            _paramValue = int.Parse(tbParameter.Text);

            if (_device != IntPtr.Zero)
            {
                TDSServiceAPI.SetDeviceParam(_device, _paramValue);
            }
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            if (_device != IntPtr.Zero)
            {
                int[] parameters = new int[9];

                TDSServiceAPI.GetDeviceParameters(_device, parameters);

                for (int i = 0; i < _tbParams.Length; i++)
                    _tbParams[i].Text = parameters[i].ToString();

            }
        }
    }
}
