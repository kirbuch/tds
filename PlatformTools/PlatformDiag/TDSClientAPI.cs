﻿using System;
using System.Runtime.InteropServices;

namespace PlatformDiag
{
    public class TDSClientAPI
    {
        public struct Event
        {
            public int senderID;    //Идентификатор инициатора события (номер персоны)
            public int msgID;       //Идентификатор сообщения
            public int parameter;   //Параметр
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
            public byte[] data; //Буффер с дополнительными данными
        };

        [DllImport("TDSClient.dll")]
        public static extern double Version();

        [DllImport("TDSClient.dll", CharSet = CharSet.Ansi)]
        public static extern IntPtr StartTask(string host);

        [DllImport("TDSClient.dll")]
        public static extern void StopTask(IntPtr task);


        //TODO: заменить возвращаемый тип
        [DllImport("TDSClient.dll")]
        public static extern byte StartPlatform(IntPtr task);

        [DllImport("TDSClient.dll")]
        public static extern double Read_direction(IntPtr task);

        [DllImport("TDSClient.dll")]
        public static extern double Read_speed(IntPtr task);

        /// <summary>
        /// Возвращает текущее значение высоты подвижности, значения от -34мм до 45мм
        /// </summary>
        /// <param name="task">Платформа</param>
        /// <returns>Высота платформы</returns>
        [DllImport("TDSClient.dll")]
        public static extern double Read_height(IntPtr task);

        /// <summary>
        /// возвращает текущее значение высоты пояса игрока
        /// </summary>
        /// <param name="task">Платформа</param>
        /// <returns>ВЫсота пояса, (12-ти битное значение от 0 до 4095)</returns>
        [DllImport("TDSClient.dll")]
        public static extern double Read_height_position(IntPtr task);

        /// <summary>
        /// Возвращает текущее значение угла крена, от 0 до 360
        /// </summary>
        /// <param name="task">Платформа</param>
        /// <returns>текущее значение угла крена, от 0 до 360</returns>
        [DllImport("TDSClient.dll")]
        public static extern double Read_angle_kren(IntPtr task);

        /// <summary>
        /// Возвращает текущее значение угла тангажа, от 0 до 360.
        /// </summary>
        /// <param name="task">Платформа</param>
        /// <returns>текущее значение угла тангажа, от 0 до 360</returns>
        [DllImport("TDSClient.dll")]
        public static extern double Read_angle_tangag(IntPtr task);

        /// <summary>
        /// задаёт изменение высоты на указанную величину и с заданной скоростью
        /// </summary>
        /// <param name="task">Платформа</param>
        /// <param name="x">Новое значение, значения от -34мм до 45мм</param>
        /// <param name="s">Скорость</param>
        /// <returns></returns>
        [DllImport("TDSClient.dll")]
        public static extern bool Move_height(IntPtr task, double x, double s);

        /// <summary>
        /// задаёт изменение крена на указанную величину и с заданной скоростью
        /// </summary>
        /// <param name="task">Платформа</param>
        /// <param name="x">Угол крена, от 0 до 360</param>
        /// <param name="s">Скорость</param>
        /// <returns></returns>
        [DllImport("TDSClient.dll")]
        public static extern bool Move_kren(IntPtr task, double x, double s);

        /// <summary>
        /// задаёт изменение тангажа на указанную величину и с заданной скоростью
        /// </summary>
        /// <param name="task">Платформа</param>
        /// <param name="y">угол тангажа, от 0 до 360</param>
        /// <param name="s">Скорость</param>
        /// <returns></returns>
        [DllImport("TDSClient.dll")]
        public static extern bool Move_tangag(IntPtr task, double y, double s);

        /// <summary>
        /// задаёт изменение по двум координатам на указанную величину и с заданной скоростью
        /// </summary>
        /// <param name="task">Платформа</param>
        /// <param name="x">Угол крена, от 0 до 360</param>
        /// <param name="y">угол тангажа, от 0 до 360</param>
        /// <param name="s">Скорость</param>
        /// <returns></returns>
        [DllImport("TDSClient.dll")]
        public static extern bool Move(IntPtr task, double x, double y, double s);

        [DllImport("TDSClient.dll")]
        public static extern bool Write_WaterP(IntPtr task, double p);

        [DllImport("TDSClient.dll")]
        public static extern bool Write_Valve(IntPtr task, double value);

        [DllImport("TDSClient.dll")]
        public static extern bool Write_Dispenser(IntPtr task, double value);

        [DllImport("TDSClient.dll")]
        public static extern bool Write_Composition(IntPtr task, double value);

        [DllImport("TDSClient.dll")]
        public static extern bool Write_Pulse(IntPtr task, int id, double value);

        [DllImport("TDSClient.dll")]
        public static extern bool Write_AirP(IntPtr task, int id, double value);

        [DllImport("TDSClient.dll")]
        public static extern double Read_WaterP(IntPtr task);

        [DllImport("TDSClient.dll")]
        public static extern double Read_Valve(IntPtr task);

        [DllImport("TDSClient.dll")]
        public static extern double Read_Dispenser(IntPtr task);

        [DllImport("TDSClient.dll")]
        public static extern double Read_Composition(IntPtr task);

        [DllImport("TDSClient.dll")]
        public static extern double Read_Pulse(IntPtr task, int id);

        [DllImport("TDSClient.dll")]
        public static extern double Read_AirP(IntPtr task, int id);

        [DllImport("TDSClient.dll")]
        public static extern bool Write_WaterV(IntPtr task, double value);

        [DllImport("TDSClient.dll")]
        public static extern double Read_WaterV(IntPtr task);

        [DllImport("TDSClient.dll")]
        public static extern bool Write_PumpP(IntPtr task, double value);

        [DllImport("TDSClient.dll")]
        public static extern double Read_PumpP(IntPtr task);

        [DllImport("TDSClient.dll")]
        public static extern void RaiseEvent(IntPtr task, int senderID, int msgID, int parameter);

        [DllImport("TDSClient.dll")]
        public static extern void RaiseEvent2(IntPtr task, Event msg);

        [DllImport("TDSClient.dll")]
        public static extern int PopEvent(IntPtr task, ref Event msg);
    }
}
