﻿namespace PlatformDiag
{
    partial class UCDevice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.tbAdress = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnOff = new System.Windows.Forms.Button();
            this.cbDeviceType = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbP8 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbP7 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbP6 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbP5 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbP4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbP3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbP2 = new System.Windows.Forms.TextBox();
            this.lblP2 = new System.Windows.Forms.Label();
            this.tbP1 = new System.Windows.Forms.TextBox();
            this.lblP1 = new System.Windows.Forms.Label();
            this.lbLog = new System.Windows.Forms.ListBox();
            this.tbParameter = new System.Windows.Forms.TextBox();
            this.lblParameterName = new System.Windows.Forms.Label();
            this.cbOn = new System.Windows.Forms.CheckBox();
            this.btnAttach = new System.Windows.Forms.Button();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Адрес устройства:";
            // 
            // tbAdress
            // 
            this.tbAdress.Location = new System.Drawing.Point(113, 19);
            this.tbAdress.Name = "tbAdress";
            this.tbAdress.Size = new System.Drawing.Size(99, 20);
            this.tbAdress.TabIndex = 1;
            this.tbAdress.Text = "192.168.13.42";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnOff);
            this.groupBox1.Controls.Add(this.cbDeviceType);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbP8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbP7);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tbP6);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.tbP5);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbP4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbP3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbP2);
            this.groupBox1.Controls.Add(this.lblP2);
            this.groupBox1.Controls.Add(this.tbP1);
            this.groupBox1.Controls.Add(this.lblP1);
            this.groupBox1.Controls.Add(this.lbLog);
            this.groupBox1.Controls.Add(this.tbParameter);
            this.groupBox1.Controls.Add(this.lblParameterName);
            this.groupBox1.Controls.Add(this.cbOn);
            this.groupBox1.Controls.Add(this.btnAttach);
            this.groupBox1.Controls.Add(this.tbAdress);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(440, 250);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Устройство";
            // 
            // btnOff
            // 
            this.btnOff.Location = new System.Drawing.Point(223, 19);
            this.btnOff.Name = "btnOff";
            this.btnOff.Size = new System.Drawing.Size(95, 23);
            this.btnOff.TabIndex = 25;
            this.btnOff.Text = "Отключиться";
            this.btnOff.UseVisualStyleBackColor = true;
            this.btnOff.Click += new System.EventHandler(this.btnOff_Click);
            // 
            // cbDeviceType
            // 
            this.cbDeviceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeviceType.FormattingEnabled = true;
            this.cbDeviceType.Items.AddRange(new object[] {
            "Рукавная линия (01)",
            "Система нагрева (02)",
            "Система нагрева (02)",
            "Вентилятор (03)"});
            this.cbDeviceType.Location = new System.Drawing.Point(113, 45);
            this.cbDeviceType.Name = "cbDeviceType";
            this.cbDeviceType.Size = new System.Drawing.Size(99, 21);
            this.cbDeviceType.TabIndex = 24;
            this.cbDeviceType.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Тип устройства:";
            // 
            // textBox6
            // 
            this.tbP8.Location = new System.Drawing.Point(328, 149);
            this.tbP8.Name = "textBox6";
            this.tbP8.Size = new System.Drawing.Size(99, 20);
            this.tbP8.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(221, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Адрес устройства:";
            // 
            // textBox7
            // 
            this.tbP7.Location = new System.Drawing.Point(328, 123);
            this.tbP7.Name = "textBox7";
            this.tbP7.Size = new System.Drawing.Size(99, 20);
            this.tbP7.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(221, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Адрес устройства:";
            // 
            // textBox8
            // 
            this.tbP6.Location = new System.Drawing.Point(328, 97);
            this.tbP6.Name = "textBox8";
            this.tbP6.Size = new System.Drawing.Size(99, 20);
            this.tbP6.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(221, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Адрес устройства:";
            // 
            // textBox9
            // 
            this.tbP5.Location = new System.Drawing.Point(328, 71);
            this.tbP5.Name = "textBox9";
            this.tbP5.Size = new System.Drawing.Size(99, 20);
            this.tbP5.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(221, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Адрес устройства:";
            // 
            // textBox5
            // 
            this.tbP4.Location = new System.Drawing.Point(113, 149);
            this.tbP4.Name = "textBox5";
            this.tbP4.Size = new System.Drawing.Size(99, 20);
            this.tbP4.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Адрес устройства:";
            // 
            // textBox4
            // 
            this.tbP3.Location = new System.Drawing.Point(113, 123);
            this.tbP3.Name = "textBox4";
            this.tbP3.Size = new System.Drawing.Size(99, 20);
            this.tbP3.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Адрес устройства:";
            // 
            // textBox3
            // 
            this.tbP2.Location = new System.Drawing.Point(113, 97);
            this.tbP2.Name = "textBox3";
            this.tbP2.Size = new System.Drawing.Size(99, 20);
            this.tbP2.TabIndex = 10;
            // 
            // lblP2
            // 
            this.lblP2.AutoSize = true;
            this.lblP2.Location = new System.Drawing.Point(6, 100);
            this.lblP2.Name = "lblP2";
            this.lblP2.Size = new System.Drawing.Size(101, 13);
            this.lblP2.TabIndex = 9;
            this.lblP2.Text = "Адрес устройства:";
            // 
            // tbP1
            // 
            this.tbP1.Location = new System.Drawing.Point(113, 71);
            this.tbP1.Name = "tbP1";
            this.tbP1.Size = new System.Drawing.Size(99, 20);
            this.tbP1.TabIndex = 8;
            // 
            // lblP1
            // 
            this.lblP1.AutoSize = true;
            this.lblP1.Location = new System.Drawing.Point(6, 74);
            this.lblP1.Name = "lblP1";
            this.lblP1.Size = new System.Drawing.Size(70, 13);
            this.lblP1.TabIndex = 7;
            this.lblP1.Text = "Параметр 1:";
            // 
            // lbLog
            // 
            this.lbLog.ColumnWidth = 85;
            this.lbLog.FormattingEnabled = true;
            this.lbLog.Location = new System.Drawing.Point(9, 175);
            this.lbLog.MultiColumn = true;
            this.lbLog.Name = "lbLog";
            this.lbLog.Size = new System.Drawing.Size(418, 69);
            this.lbLog.TabIndex = 6;
            // 
            // tbParameter
            // 
            this.tbParameter.Location = new System.Drawing.Point(328, 45);
            this.tbParameter.Name = "tbParameter";
            this.tbParameter.Size = new System.Drawing.Size(99, 20);
            this.tbParameter.TabIndex = 5;
            this.tbParameter.Text = "0";
            this.tbParameter.TextChanged += new System.EventHandler(this.tbParameters_TextChanged);
            // 
            // lblParameterName
            // 
            this.lblParameterName.AutoSize = true;
            this.lblParameterName.Location = new System.Drawing.Point(221, 48);
            this.lblParameterName.Name = "lblParameterName";
            this.lblParameterName.Size = new System.Drawing.Size(61, 13);
            this.lblParameterName.TabIndex = 4;
            this.lblParameterName.Text = "Параметр:";
            // 
            // cbOn
            // 
            this.cbOn.AutoSize = true;
            this.cbOn.Location = new System.Drawing.Point(328, 23);
            this.cbOn.Name = "cbOn";
            this.cbOn.Size = new System.Drawing.Size(75, 17);
            this.cbOn.TabIndex = 3;
            this.cbOn.Text = "Включить";
            this.cbOn.UseVisualStyleBackColor = true;
            this.cbOn.CheckedChanged += new System.EventHandler(this.cbOn_CheckedChanged);
            // 
            // btnAttach
            // 
            this.btnAttach.Location = new System.Drawing.Point(223, 17);
            this.btnAttach.Name = "btnAttach";
            this.btnAttach.Size = new System.Drawing.Size(95, 23);
            this.btnAttach.TabIndex = 2;
            this.btnAttach.Text = "Подключиться";
            this.btnAttach.UseVisualStyleBackColor = true;
            this.btnAttach.Click += new System.EventHandler(this.btnAttach_Click);
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // UCDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCDevice";
            this.Size = new System.Drawing.Size(447, 258);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbAdress;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAttach;
        private System.Windows.Forms.CheckBox cbOn;
        private System.Windows.Forms.ListBox lbLog;
        private System.Windows.Forms.TextBox tbParameter;
        private System.Windows.Forms.Label lblParameterName;
        private System.Windows.Forms.TextBox tbP8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbP7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbP6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbP5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbP4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbP3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbP2;
        private System.Windows.Forms.Label lblP2;
        private System.Windows.Forms.TextBox tbP1;
        private System.Windows.Forms.Label lblP1;
        private System.Windows.Forms.ComboBox cbDeviceType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Timer timerUpdate;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnOff;
    }
}
