﻿#include "TDSServer.h"


//----------------------------------------------------------------------
// tds_room

void tds_section::join(tds_participant_ptr participant)
{
	participants_.insert(participant);

	//HACK: отсылка сообщений из буфера не нужна???
	//for (auto msg : recent_msgs_)
	//	participant->deliver(msg);
}

void tds_section::leave(tds_participant_ptr participant)
{
	participants_.erase(participant);
}

void tds_section::deliver(const tds_message& msg)
{
	_sent_msgs.push_back(msg);
	while (_sent_msgs.size() > max_sent_msgs)
		_sent_msgs.pop_front();

	for (auto participant : participants_)
		participant->deliver(msg);
}

void tds_section::handle(const tds_message& msg)
{
	std::cout << "tds_section::handle" << std::endl;

	recent_msgs_.push_back(msg);
	while (recent_msgs_.size() > max_recent_msgs)
		recent_msgs_.pop_front();

	if (_handler)
		_handler->handle(msg);
}

//----------------------------------------------------------------------
// tds_session
void tds_session::start()
{
	_section.join(shared_from_this());
	do_read_header();
}

void tds_session::deliver(const tds_message& msg)
{
	bool write_in_progress = !write_msgs_.empty();
	write_msgs_.push_back(msg);
	if (!write_in_progress)
	{	
		do_write();
	}
}

// private:
void tds_session::do_read_header()
{
	auto self(shared_from_this());
	boost::asio::async_read(socket_,
		boost::asio::buffer(read_msg_.data(), tds_message::header_length),
		[this, self](boost::system::error_code ec, std::size_t /*length*/)
		{
			if (!ec)
			{
				read_msg_.decode_header();
				do_read_body();
			}
			else
			{
				std::cout << "error: " << ec.message() << std::endl;
				_section.leave(shared_from_this());
			}
		});
}

void tds_session::do_read_body()
{
	auto self(shared_from_this());
	boost::asio::async_read(socket_,
		boost::asio::buffer(read_msg_.body(), read_msg_.body_length()),
		[this, self](boost::system::error_code ec, std::size_t /*length*/)
		{
			if (!ec)
			{
				_section.handle(read_msg_);
				do_read_header();
			}
			else
			{
				_section.leave(shared_from_this());
			}
		});
}

void tds_session::do_write()
{
	auto self(shared_from_this());
	boost::asio::async_write(socket_,
		boost::asio::buffer(write_msgs_.front().data(),
			write_msgs_.front().length()),
		[this, self](boost::system::error_code ec, std::size_t /*length*/)
		{
			if (!ec)
			{
				write_msgs_.pop_front();
				if (!write_msgs_.empty())
				{
					do_write();
				}
			}
			else
			{
				_section.leave(shared_from_this());
			}
		});
}
//----------------------------------------------------------------------
// tds_server

void tds_server::do_accept()
{
	acceptor_.async_accept(
		[this](boost::system::error_code ec, tcp::socket socket)
		{
			if (!ec)
			{
				std::make_shared<tds_session>(std::move(socket), _section)->start();
			}

			do_accept();
		});
}
