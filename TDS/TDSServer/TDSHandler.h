﻿#ifndef TDS_HANDLER_HPP
#define TDS_HANDLER_HPP

#include "../API/message.hpp"

class tds_transport
{
public:
	virtual ~tds_transport() {}
	virtual void deliver(const tds_message& msg) = 0;
};
typedef std::shared_ptr<tds_transport> tds_transport_ptr;

class tds_handler
{
public:
	virtual ~tds_handler() {}
	virtual void attach(tds_transport* transport) = 0;
	virtual void handle(const tds_message& msg) = 0;
};
typedef std::shared_ptr<tds_handler> tds_handler_ptr;

#endif
