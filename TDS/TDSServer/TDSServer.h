﻿// TestConsole.h : Include file for standard system include files,
// or project specific include files.

#ifndef TDS_SERVER_HPP
#define TDS_SERVER_HPP

#include <cstdlib>
#include <deque>
#include <iostream>
#include <list>
#include <memory>
#include <set>
#include <utility>
#include <boost/asio.hpp>

#include "../API/message.hpp"
#include "TDSHandler.h"

using boost::asio::ip::tcp;

//----------------------------------------------------------------------

typedef std::deque<tds_message> tds_message_queue;

//----------------------------------------------------------------------

class tds_participant
{
public:
	virtual ~tds_participant() {}
	virtual void deliver(const tds_message& msg) = 0;
};

typedef std::shared_ptr<tds_participant> tds_participant_ptr;

//----------------------------------------------------------------------
class tds_section : tds_transport
{
public:
	void join(tds_participant_ptr participant);
	void leave(tds_participant_ptr participant);
	void handle(const tds_message& msg);

	void set_handle(tds_handler* handler) { _handler = handler; _handler->attach(this); }
	virtual void deliver(const tds_message& msg);

private:
	std::set<tds_participant_ptr> participants_;
	enum { 
		max_recent_msgs = 100,
		max_sent_msgs = 100
	};
	tds_message_queue recent_msgs_;
	tds_message_queue _sent_msgs;

	tds_handler* _handler = 0;
};

//----------------------------------------------------------------------

class tds_session
	: public tds_participant,
	public std::enable_shared_from_this<tds_participant> // std::enable_shared_from_this<tds_session> HACK: разобраться почему ошибка при компиляции

{
public:
	tds_session(tcp::socket socket, tds_section& section)
		: socket_(std::move(socket)),
		_section(section)
	{
	}

	void start();

	void deliver(const tds_message& msg);

private:
	void do_read_header();
	void do_read_body();
	void do_write();

	tcp::socket socket_;
	tds_section& _section;
	tds_message read_msg_;
	tds_message_queue write_msgs_;
};

//----------------------------------------------------------------------

class tds_server
{
public:
	tds_server(boost::asio::io_context& io_context,
		const tcp::endpoint& endpoint)
		: acceptor_(io_context, endpoint)
	{
		do_accept();
	}

	tds_section& section() { return _section; }
private:
	void do_accept();

	tcp::acceptor acceptor_;
	tds_section _section;
};

#endif
