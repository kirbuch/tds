#include <cstdlib>
#include <deque>
#include <iostream>
#include <list>
#include <memory>
#include <set>
#include <utility>
#include <thread>
#include <boost/asio.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "TDSServer.h"
#include "../API/TDSClientAPI.h"
#include "../API/TDSServiceAPI.h"
#include "../API/PlatformParameters.h"
#include "../API/message.hpp"

using boost::asio::ip::tcp;

//----------------------------------------------------------------------

using namespace std;

//TODO: ����������� � ������� � ���� �����
//boost::uuids::uuid TDS_MSG_POSITION = boost::uuids::string_generator()("{C94C1CE5-5B4A-4A0D-8A4F-DAC7C709255B}");
//boost::uuids::uuid TDS_MSG_MOVE_PARAMETER = boost::uuids::string_generator()("{0B926887-EEBD-4F6E-A340-D44E665C2D98}");

struct Work
{
	tds_server* pServer;
	TDS_TASK task;

	Work(tds_server* pServer, TDS_TASK task)
	{
		this->pServer = pServer;
		this->task = task;
	}
};

class TestWorker : public tds_handler
{
public:
	TestWorker() :
		_transport(0),
		_task(0),
		_running(false)
	{}

	virtual ~TestWorker() { Stop(); }

	void Start()
	{
		if (_running) //TODO: Log this
			return;

		_task = StartTask();
		StartPlatform(_task);
		_running = true;

		cout << "Create test platform" << endl;

		_thread = shared_ptr<std::thread>(new std::thread(run, this));
	}

	void Stop()
	{
		_running = true;
	}


	// Inherited via tds_handler
	virtual void attach(tds_transport* transport) override
	{
		_transport = transport;
	}

	virtual void handle(const tds_message& msg) override
	{
		msg.code();
		if (msg.code() == TDS_MSG_POSITION)
		{
			std::cout << "TDS_MSG_POSITION ";
			PlatformParameters param;
			memcpy(&param, msg.body(), msg.body_length());
			std::cout << "speed: " << param.Speed << " direction:" << param.Direction;
		}
		else if (msg.code() == TDS_MSG_EVENT)
		{
			std::cout << "TDS_MSG_POSITION ";
			Event event;
			memcpy(&event, msg.body(), msg.body_length());
			std::cout << "msgID: " << event.msgID << " senderID:" << event.senderID << " parameter:" << event.parameter;
			RaiseEvent2(_task, event);
		}
		else if (msg.code() == TDS_MSG_MOVE_PARAMETER)
		{
			ChangeParameters msgChange;
			memcpy(&msgChange, msg.body(), msg.body_length());
			std::cout << "ChangeParameters: " << msgChange.Parameter << " x= " << msgChange.x << " y= " << msgChange.y << " s= " << msgChange.s << std::endl;

			switch (msgChange.Parameter)
			{
			case EParameters::PARAM_Height:
				Move_height(_task, msgChange.x, msgChange.s);
				break;
			case EParameters::PARAM_Azimuth:
				Move_azimuth(_task, msgChange.x);
				break;
			case EParameters::PARAM_Kren:
				Move_kren(_task, msgChange.x, msgChange.s);
				break;
			case EParameters::PARAM_Tangag:
				Move_tangag(_task, msgChange.y, msgChange.s);
				break;
			case EParameters::PARAM_Angle:
				Move(_task, msgChange.x, msgChange.y, msgChange.s);
				break;
			case EParameters::PARAM_AutoMove:
				SetAutoMove(_task, msgChange.x != 0);
				break;
			case EParameters::PARAM_Jump:
				Jump(_task, msgChange.y, msgChange.s);
				break;
			case EParameters::PARAM_Speed:
				SetSpeed(_task, msgChange.s);
				break;
			case EParameters::PARAM_Rotate:
				Rotate(_task, msgChange.s);
				break;
			case EParameters::PARAM_Direction:
				SetDirection(_task, msgChange.x);
				break;
			case EParameters::PARAM_Firehose_waterP:
				Write_WaterP(_task, msgChange.x);
				break;
			case EParameters::PARAM_Firehose_waterV:
				Write_WaterV(_task, msgChange.x);
				break;
			case EParameters::PARAM_Firehose_pumpP:
				Write_PumpP(_task, msgChange.x);
				break;
			case EParameters::PARAM_Firehose_valve:
				Write_Valve(_task, msgChange.x);
				break;
			case EParameters::PARAM_Firehose_dispenser:
				Write_Dispenser(_task, msgChange.x);
				break;
			case EParameters::PARAM_Firehose_composition:
				Write_Composition(_task, msgChange.x);
				break;
			case EParameters::PARAM_Firehose_pulse0:
				Write_Pulse(_task, 0, msgChange.x);
				break;
			case EParameters::PARAM_Firehose_pulse1:
				Write_Pulse(_task, 1, msgChange.x);
				break;
			case EParameters::PARAM_Firehose_airP0:
				Write_AirP(_task, 0, msgChange.x);
				break;
			case EParameters::PARAM_Firehose_airP1:
				Write_AirP(_task, 1, msgChange.x);
				break;
			}

			//_transport->deliver(msg);
		}
		else
		{
			std::cout << "Unknown msg. Code: " << msg.code() << ": ";
			std::cout.write(msg.body(), msg.body_length());
		}
		std::cout << "\n";
	}

private:
	bool _running;
	tds_transport* _transport;
	TDS_TASK _task;
	shared_ptr<std::thread> _thread;

	void do_run()
	{
		cout << "run started" << endl;
		while (_running)
		{
			if (_transport != 0)
			{
				const PlatformParameters& param = GetParameters(_task);

				tds_message msg(TDS_MSG_POSITION, sizeof(PlatformParameters), &param);

				_transport->deliver(msg);

				Event event;
				while (PopEvent(_task, event) > 0)
				{
					tds_message eventMsg(TDS_MSG_EVENT, sizeof(Event), &event);

					_transport->deliver(eventMsg);
				}
			}

			std::this_thread::sleep_for(100ms);
		}
	}

	void static run(TestWorker* worker)
	{
		cout << "start run" << endl;
		worker->do_run();
	}
};

int main(int argc, char* argv[])
{
	try
	{
		cout << "TDSClient version " << Version() << endl;

		const char* port = DEFAULT_PORT;
		if (argc > 1)
			port = argv[1];

		boost::asio::io_context io_context;
		int nport = atoi(port);
		cerr << "Using port: " << nport << ",  for set port: TDSServer <port>\n";
		
		tcp::endpoint endpoint(tcp::v4(), nport);
		tds_server server(io_context, endpoint);

		TestWorker worker;

		server.section().set_handle(&worker);

		worker.Start();

		cout << "run context" << endl;
		io_context.run();

		cout << "stop" << endl;
	}
	catch (std::exception& e)
	{
		cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}
