﻿// TDSClientAPI.h : Include file for TDS API functions,
#ifndef TDS_SERVICE_API_HPP
#define TDS_SERVICE_API_HPP

#include "PlatformParameters.h"
#include "TDSClientAPI.h"

#define TDS_DEVICE void*

extern "C"
{
	const PlatformParameters& GetParameters(TDS_TASK task);
	void GetParameters2(TDS_TASK task, PlatformParameters& params);

	bool SetParameters(TDS_TASK task, PlatformParameters parameters);

	bool SetAutoMove(TDS_TASK task, bool autoMove);
	bool Jump(TDS_TASK task, double height, double speed);
	bool SetSpeed(TDS_TASK task, double speed);
	bool SetDirection(TDS_TASK task, double direction);
	bool Rotate(TDS_TASK task, double rotateSpeed);

	ParametersCmd PopParameterChanges(TDS_TASK task);

	//////////////////////////////////////////////////////////////////////////////////////////////
	//Device

	TDS_DEVICE CreateDevice(const char* server, int port, char typeId);
	int GetDeviceCount();
	char GetDeviceCode(TDS_DEVICE device);
	void GetDeviceParameters(TDS_DEVICE device, int* buffer);
	void SetDeviceOn(TDS_DEVICE device, bool isOn);
	void SetDeviceParam(TDS_DEVICE device, int parameter);
	time_t GetLastUpdateTime(TDS_DEVICE device);
	void DestroyDevice(TDS_DEVICE device);

	void StartListenPort(int port);
	void StopListenPort();

	//////////////////////////////////////////////////////////////////////////////////////////////
	//Math
	double CalculateRoll(double alpha, double beta);
	double CalculatePitch(double alpha, double beta);
}

#endif
