#ifndef TDS_MESSAGE_H
#define TDS_MESSAGE_H

extern "C"
{
	const int TDS_MSG_SenderID_unknown = -1; //�����������
	const int TDS_MSG_SenderID_firefighter1 = 0; //�������� 1
	const int TDS_MSG_SenderID_firefighter2 = 1; //�������� 2
	const int TDS_MSG_SenderID_boss = 2; //��������� �������
	const int TDS_MSG_SenderID_bot = 3; //���
	const int TDS_MSG_SenderID_firepoint = 4; //����� ����

	const int TDS_MSG_fitehose_take = 1; //����� - �����
	const int TDS_MSG_fitehose_drop = 2; //����� - ��������
	const int TDS_MSG_fitehose_unpack = 3; //����� - ���������
	const int TDS_MSG_fitehose_pack = 4; //����� - ��������
	const int TDS_MSG_fitehose_connect = 5; //����� - ���������
	const int TDS_MSG_fitehose_disconnect = 6; //����� - �����������
	const int TDS_MSG_tankvehicle_back_connect = 7; //������������ ������ ������ - ���������
	const int TDS_MSG_tankvehicle_back_disconnect = 8; //������������ ������ ������ - �����������
	const int TDS_MSG_tankvehicle_right_connect = 9; //������������ ������ ������ - ���������
	const int TDS_MSG_tankvehicle_right_disconnect = 10; //������������ ������ ������ - �����������
	const int TDS_MSG_tankvehicle_left_connect = 11; //������������ ����� ������ - ���������
	const int TDS_MSG_tankvehicle_left_disconnect = 12; //������������ ����� ������ - �����������
	const int TDS_MSG_tankvehicle_water_on = 13; //������������ ���� - ���
	const int TDS_MSG_tankvehicle_water_off = 14; //������������ ���� - ����
	const int TDS_MSG_tankvehicle_foam_on = 15; //������������ ���� - ���
	const int TDS_MSG_tankvehicle_foam_off = 16; //������������ ���� - ����
	const int TDS_MSG_tankvehicle_in_on = 17; //������������ ���� ������� - ���
	const int TDS_MSG_tankvehicle_in_off = 18; //������������ ���� ������� - ����
	const int TDS_MSG_tankvehicle_cistern_on = 19; //������������ ���� ��� - ����
	const int TDS_MSG_tankvehicle_cistern_off = 20; //������������ ���� ��� - ���
	const int TDS_MSG_teehose_in_connect = 21; //������������ ������� ���� - ���������
	const int TDS_MSG_teehose_in_disconnect = 22; //������������ ������� ���� - �����������
	const int TDS_MSG_teehose_out1_connect = 23; //������������ ������� �����1 - ���������
	const int TDS_MSG_teehose_out1_disconnect = 24; //������������ ������� �����1 - �����������
	const int TDS_MSG_teehose_valve1_on = 25; //������������ ������� ����1 - ���
	const int TDS_MSG_teehose_valve1_off = 26; //������������ ������� ����1 - ����
	const int TDS_MSG_teehose_out2_connect = 27; //������������ ������� �����2 - ���������
	const int TDS_MSG_teehose_out2_disconnect = 28; //������������ ������� �����2 - �����������
	const int TDS_MSG_teehose_valve2_on = 29; //������������ ������� ����2 - ���
	const int TDS_MSG_teehose_valve2_off = 30; //������������ ������� ����2 - ����
	const int TDS_MSG_teehose_out3_connect = 31; //������������ ������� �����3 - ���������
	const int TDS_MSG_teehose_out3_disconnect = 32; //������������ ������� �����3 - �����������
	const int TDS_MSG_teehose_valve3_on = 33; //������������ ������� ����3 - ���
	const int TDS_MSG_teehose_valve3_off = 34; //������������ ������� ����3 - ����
	const int TDS_MSG_firemonitor_take = 35; //����� �������� - �����
	const int TDS_MSG_firemonitor_drop = 36; //����� �������� - ��������
	const int TDS_MSG_firemonitor_on = 37; //����� �������� - ���
	const int TDS_MSG_firemonitor_off = 38; //����� �������� - ����
	const int TDS_MSG_firemonitor_compact = 39; //����� �������� ����� - ����������
	const int TDS_MSG_firemonitor_spread = 40; //����� �������� ����� - �����������
	const int TDS_MSG_firemonitor_shield = 41; //����� �������� ����� - ����������-�����
	const int TDS_MSG_grouptorch_take = 42; //������ ��������� - �����
	const int TDS_MSG_grouptorch_drop = 43; //������ ��������� - ��������
	const int TDS_MSG_grouptorch_on = 44; //������ ��������� - ���
	const int TDS_MSG_grouptorch_off = 45; //������ ��������� - ����
	const int TDS_MSG_door_close = 46; //����� - �������
	const int TDS_MSG_door_open = 47; //����� - �������
	const int TDS_MSG_botfireman_follow = 48; //���-�������� - ������� ��
	const int TDS_MSG_botfireman_cancel = 49; //���-�������� - ������
	const int TDS_MSG_botfireman_shutdown_electrocity = 50; //���-�������� - ���� �������������
	const int TDS_MSG_fire_burn = 51; //����� - �����
	const int TDS_MSG_fire_none = 52; //����� - ��  �����
	const int TDS_MSG_radio_take = 53; //����� - �����
	const int TDS_MSG_radio_drop = 54; //����� - ��������
	const int TDS_MSG_radio_attach = 55; //����� - �����
	const int TDS_MSG_radio_detach = 56; //����� - �����
	const int TDS_MSG_imager_take = 57; //���������� - �����
	const int TDS_MSG_imager_drop = 58; //���������� - ��������
	const int TDS_MSG_imager_on = 59; //���������� - ���
	const int TDS_MSG_imager_off = 60; //���������� - ����
	const int TDS_MSG_rebreathingmask_take = 61; //����� ���� - �����
	const int TDS_MSG_rebreathingmask_drop = 62; //����� ���� - ��������
	const int TDS_MSG_rebreathingmask_attach = 63; //����� ���� - �����
	const int TDS_MSG_rebreathingmask_detach = 64; //����� ���� - �����
	const int TDS_MSG_hooligantools_take = 65; //�������-���� - �����
	const int TDS_MSG_hooligantools_drop = 66; //�������-���� - ��������
	const int TDS_MSG_antirollback_take = 67; //����� ��������������� - �����
	const int TDS_MSG_antirollback_drop = 68; //����� ��������������� - ��������
	const int TDS_MSG_antirollback_install = 69; //����� ��������������� - ����������
	const int TDS_MSG_Hosefixing_take = 70; //�������� �������� - �����
	const int TDS_MSG_Hosefixing_drop = 71; //�������� �������� - ��������
	const int TDS_MSG_Hosefixing_install = 72; //�������� �������� - ����������
	const int TDS_MSG_subject_not_irrigated = 73; //������� - �� ���������
	const int TDS_MSG_subject_irrigated = 74; //������� - ���������
	const int TDS_MSG_subject_fire = 75; //������� - �����
	const int TDS_MSG_subject_stewed = 76; //������� - �������
	const int TDS_MSG_subject_burned = 77; //������� - ������
	const int TDS_MSG_room_enter = 78; //��������� - ����� 
	const int TDS_MSG_room_exit = 79; //��������� - �����
	const int TDS_MSG_room_fire = 80; //��������� - �����
	const int TDS_MSG_room_no_fire = 81; //��������� - �� �����
	const int TDS_MSG__num = 81; // - ����� ���������� ���������

}
#endif //TDS_MESSAGE_H