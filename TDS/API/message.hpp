#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <boost/uuid/uuid.hpp>

// {AD4929A3-68AE-4EA2-B5ED-F32BCF4EBCE8}
static const boost::uuids::uuid TDS_MSG_POSITION = { 0xad, 0x49, 0x29, 0xa3, 0x68, 0xae, 0x4e, 0xa2, 0xb5, 0xed, 0xf3, 0x2b, 0xcf, 0x4e, 0xbc, 0xe8 };

// {AE8DB03A-411D-4A04-B09B-A40DF477AC19}
static const boost::uuids::uuid TDS_MSG_MOVE_PARAMETER = { 0xae, 0x8d, 0xb0, 0x3a, 0x41, 0x1d, 0x4a, 0x04, 0xb0, 0x9b, 0xa4, 0xd, 0xf4, 0x77, 0xac, 0x19 };

// {6A2D0734-39F4-4E6C-AE78-8E41230F80C6}
static const boost::uuids::uuid TDS_MSG_EVENT = { 0x6a, 0x2d, 0x07, 0x34, 0x39, 0xf4, 0x4e, 0x6c, 0xae, 0x78, 0x8e, 0x41, 0x23, 0x0f, 0x80, 0xc6 };

class tds_message
{
public:
    struct header
    {
        boost::uuids::uuid cmd_code;
        uint64_t body_length = 0;
    };

    enum { header_length = sizeof(header) };
    enum { max_body_length = 2048 };

    tds_message()
    {
    }

    tds_message(boost::uuids::uuid cmd_code, uint64_t body_length, const void* data)
    {
        _header.cmd_code = cmd_code;
        _header.body_length = body_length;
        
        encode_header();
        //TODO: ������������ ������������ ����� ��� ��������� �� ������������ �����
        memcpy(body(), data, body_length);
    }

    const boost::uuids::uuid& code() const
    {
        return _header.cmd_code;
    }

    const char* data() const
    {
        return data_;
    }

    char* data()
    {
        return data_;
    }

    std::size_t length() const
    {
        return header_length + body_length();
    }

    const char* body() const
    {
        return data_ + header_length;
    }

    char* body()
    {
        return data_ + header_length;
    }

    std::size_t body_length() const
    {
        return _header.body_length;
    }

    void body_length(std::size_t new_length)
    {
        _header.body_length = new_length;
        if (_header.body_length > max_body_length)
            _header.body_length = max_body_length;
    }

    bool decode_header()
    {
        std::memcpy(&_header, data_, header_length);
        if (_header.body_length > max_body_length)
        {
            _header.body_length = 0;
            return false;
        }
        return true;
    }

    void encode_header()
    {
        std::memcpy(data_, &_header, header_length);
    }


private:
    //TODO: ������������ ������������ �����
    header _header;
    char data_[header_length + max_body_length];
};

#endif // MESSAGE_HPP
