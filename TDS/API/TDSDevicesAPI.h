// TDSDevicesAPI.h : Include file for devices API functions,

#pragma once

#define DEVICE void*

extern "C"
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//�������� �����

	//��������� �������� �����
	struct FirehoseInfo
	{
		bool  Ready;    // ����������
		bool  IsWorking;// ��������
		float Weight;   // ������� ���������
		float Force;    // ������� ���������� ��������(����)
		float Valve;    // ��������� ����� �������� �������� (0 - 4095)
		float Output;   // ��������� ���������� �������������������(0 - 4095)
		float Dispenser;// ��������� ���������� ������������(0 - 4095)
		int   Error;    // ������, ��� ������
	};

	/// <summary>
	/// ������ �������� �����
	/// </summary>
	void SetFirehoseOn(bool isOn);

	/// <summary>
	/// ��������� ������������ �������� �������� �����
	/// </summary>
	void SetFirehoseLoad(float kLoad);

	/// <summary>
	/// ��������� ������ � �������� �����
	/// </summary>
	/// <returns>��� ������, 0 - ������� ���</returns>
	int GetFirehoseInfo(FirehoseInfo* pInfo);


	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//������� ������� ���������

	//��������� ������� 
	struct HeaterInfo
	{
		char  Person;      // ������������� ��������� 0, 1
		bool  IsHeating;   // ������ �������
		char  CurrentTemp; // ������� �����������
		char  DestTemp;    // ������� �����������
		float Pulse;       // ������� �����
		float Air;         // ��������� ������� ������ �������
		int   Error;       // ������, ��� ������
	};

	/// <summary>
	/// ������ �������� �����
	/// </summary>
	void SetHeaterOn(char person,  bool isOn);

	/// <summary>
	/// ������� ������� �����������
	/// </summary>
	void SetHeaterDestTemp(char person, char newTemp);

	/// <summary>
	/// ��������� ������ ��������� �������
	/// </summary>
	/// <returns>��� ������, 0 - ������� ���</returns>
	int GetHeaterInfo(char person, HeaterInfo* pInfo);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //����������

	//��������� �����������
	struct VentilatorInfo
	{
		bool  Ready;    // ����������
		bool  IsWorking;// ��������
		char  Mode;     // ������� ����� (0/1/2/3)
		int   Error;    // ������, ��� ������
	};

	/// <summary>
	/// ������ �����������
	/// </summary>
	void SetVentilatorOn(bool isOn);

	/// <summary>
	/// ������� ������ ������ ����������� (0/1/2/3)
	/// </summary>
	void SetVentilatorMode(char newMode);

	/// <summary>
	/// ��������� ������ ��������� �����������
	/// </summary>
	/// <returns>��� ������, 0 - ������� ���</returns>
	int GetVentilatorInfo(VentilatorInfo* pInfo);


}


