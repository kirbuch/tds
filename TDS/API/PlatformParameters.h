#ifndef PLATFORM_PARAMETERS_HPP
#define PLATFORM_PARAMETERS_HPP

//#include <boost/uuid/string_generator.hpp>
//
////TODO: ����������� � ������� � ���� �����
//extern boost::uuids::uuid TDS_MSG_POSITION; 

#include "TDSClientAPI.h"

enum EParameters
{
	PARAM_AutoMove = -100,
	PARAM_Jump,
	PARAM_Speed,
	PARAM_Rotate,
	PARAM_Direction,
	PARAM_NO_CHANGES = 0,
	PARAM_Height = 1,
	PARAM_Azimuth,
	PARAM_Kren,
	PARAM_Tangag,
	PARAM_Angle,
	PARAM_Firehose_waterP,
	PARAM_Firehose_valve,
	PARAM_Firehose_dispenser,
	PARAM_Firehose_composition,
	PARAM_Firehose_pulse0,
	PARAM_Firehose_pulse1,
	PARAM_Firehose_airP0,
	PARAM_Firehose_airP1,
	PARAM_Firehose_waterV,
	PARAM_Firehose_pumpP,
	PARAM_Msg,
};

#pragma pack(push, 1)
struct ParametersCmd
{
	EParameters Parameter;
	double x;
	double y;
	double s;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct ChangeParameters : public ParametersCmd
{
	ChangeParameters() { Parameter = PARAM_NO_CHANGES; }
	ChangeParameters(EParameters Parameter, double x, double y, double s) { this->Parameter = Parameter; this->x = x; this->y = y; this->s = s; }
};
#pragma pack(pop)

#pragma pack(push, 1)
struct PlatformParameters
{
	bool Started = false;
	char Alarm = 0; // 1 - �������(��������� �������), 0 - ������ ������
	char Mode = 1; // 1 - ������� �����, 2 - ����� ��������� � ������������

	double angle_azimuth = 0;

	double angle_kren_min = 0;
	double angle_kren_max = 360;
	double angle_kren = 0;
	double angle_kren_target = 0;
	double angle_kren_speed = 0;

	double angle_tangag_min = 0;
	double angle_tangag_max = 360;
	double angle_tangag = 0;
	double angle_tangag_target = 0;
	double angle_tangag_speed = 0;

	double maxSpeed_deviation = 1;
	double maxSpeed_move = 100;
	double Speed_centering = 1;

	double Height = 0; //������� �������� ������ �����������, �������� �� -34�� �� 45��
	double Height_target = 0;
	double Height_speed = 0;

	double Direction = 0; //������������ 12-�� ������ �������� �� 0 �� 4095	(4095 = 360)
	double Speed = 10; //������������  ��*�, ������������� �������� ����� �������� ��������� �����
	double Height_Position = 1000; //������������ 12-�� ������ �������� �� 0 �� 4095};

	bool _autoMove = true;

	double Firehose_waterP = 0;
	double Firehose_waterV = 0;
	double Firehose_valve = 0;
	double Firehose_dispenser = 0;
	double Firehose_composition = 0;
	double Firehose_pulse0 = 0;
	double Firehose_pulse1 = 0;
	double Firehose_airP0 = 0;
	double Firehose_airP1 = 0;
	double Firehose_pumpP = 1;

	PositionInfo positions[5];
};
#pragma pack(pop)

#endif
