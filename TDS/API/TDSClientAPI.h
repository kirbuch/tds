﻿// TDSClientAPI.h : Include file for TDS API functions,

#pragma once

#define TDS_TASK void*
#define TDS_SERVER void*

extern "C"
{
	static const unsigned DIRECTION_360 = 4095;
	static const char* DEFAULT_PORT = "2121";

	/// <summary>
	/// Версия библиотеки
	/// </summary>
	/// <returns>Номер версии. Мажорная.Минорная </returns>
	double Version();

	///// <summary>
	///// Статическое соединение с сервером
	///// </summary>
	///// <returns>1 - при удачном соединении</returns>
	//int ConnectToServer(const char* host = 0);

	///// <summary>
	///// Отключение Статического соединения с сервером
	///// </summary>
	///// <returns>1 - при удачном соединении</returns>
	//void Disconnect();

	/// <summary>
	/// Создается имитатор соединения с ДП
	/// </summary>
	/// <returns>Идентификатор задачи, 0 - при неудачном соединении</returns>
	TDS_TASK StartTask(const char* host = 0);

	/// <summary>
	/// Остановить выполнение задачи
	/// </summary>
	/// <param name="task">Идентификатор задачи</param>
	void StopTask(TDS_TASK task);

	int GetTestValue(TDS_TASK task);

	char StartPlatform(TDS_TASK task);//производит включение изделия, начальную инициализацию устройств,
	//платформа не выходит из парковочного состояния.
	//возвращает 1(единицу) в случае успешной инициализации устройств или код ошибки
	char StopPlatform(TDS_TASK task);//переводит платформу в парковочное состояние и производит корректное завершение
	//работы устройств
	//возвращает 1(единицу) в случае успешного завершения работы устройств или код ошибки
	char Kalibration(TDS_TASK task);//калибровка датчиков. При первом запуске выводим платформу в среднее
	//положение при помощи спец утилиты, поворачиваем пояс в положение прямо
	// в соответствии с меткой (вертикаль калибруем отдельно, см. ниже) далее
	//вызываем функцию, она читает датчики,
	// записывает показания в файл и в дальнейшем пляшем от этих показаний

	bool Alarm(TDS_TASK task, char data);//блокирует платформу в текущем положении до поступления сигнала о снятии тревоги
	//1 - тревога (аварийный останов), 0 - отмена аварии. Возвращает 0 в случае удачи
	//и 1 в случае не удачи

	char Set_mode(TDS_TASK task, char data);//задаёт режим работы. 1 - рабочий режим, 2 - режим настройки и тестирования
	//возвращает 0 в случае успешного включения режима или код ошибки
	//различие режимов состоит в том, что в рабочем режиме нельзя менять настройки
	//функции настроек (значение записываются в файл и используются при работе платформы во всех режимах)

	bool Set_angle_kren(TDS_TASK task, double xmin, double xmax);//установить угол крена, принимает значения от 0 до 360.
	bool Set_angle_tangag(TDS_TASK task, double ymin, double ymax);//установить угол тангажа, принимает значения от 0 до 360.
	//0 в обоих случаях будет означать среднее положение
	bool Set_maxSpeed_deviation(TDS_TASK task, double s);//установить макс. скорость отклонения (наклона) мм*с.
	bool Set_maxSpeed_move(TDS_TASK task, double s);//установить макс. скорость перемещения (изменение положения по вертикали) мм*с.
	bool Set_Speed_centering(TDS_TASK task, double s);//установить скорость центрирования
	//функции информирования о состоянии
	//TODO: Выбрать формат строк
	//string Read_status();//возвращает текущее состояние 0 - норма или код аварии

	double Read_direction(TDS_TASK task);//возвращает текущее направление движения игрока (12-ти битное значение от 0 до 4095	(4095 = 360))
	double Read_speed(TDS_TASK task);//возвращает текущее значение скорости перемещения игрока (шага) мм*с, отрицательное значение
	//будет означать «пятиться назад»

	double Read_height_position(TDS_TASK task);//возвращает текущее значение высоты пояса игрока (12-ти битное значение от 0 до 4095) //Высота пояса
	//калибровку предлагаю проводить под каждого игрока, т.е. перед запуском программы
	//считываем текущее положение высоты, отклонение от этого значения более чем на 100 мм
	//вниз считаем присядом, выше прыжком (100мм для примера)
	double Read_height(TDS_TASK task);//возвращает текущее значение высоты подвижности, значения от -34мм до 45мм. Высота платформы
	double Read_angle_kren(TDS_TASK task);//возвращает текущее значение угла крена, от 0 до 360.
	double Read_angle_tangag(TDS_TASK task);//возвращает текущее значение угла тангажа, от 0 до 360.
	//функции команды
	
	bool Move_azimuth(TDS_TASK task, double angle); //задаёт изменение курсового угла
	bool Move_kren(TDS_TASK task, double x, double s);//задаёт изменение крена на указанную величину и с заданной скоростью
	bool Move_tangag(TDS_TASK task, double y, double s);//задаёт изменение тангажа на указанную величину и с заданной скоростью
	bool Move_height(TDS_TASK task, double x, double s);//задаёт изменение высоты на указанную величину и с заданной скоростью
	bool Move(TDS_TASK task, double x, double y, double s);//задаёт изменение по двум координатам на указанную величину и с заданной скоростью
		//автоматические функции
	bool Auto_centr(TDS_TASK task);//автоматического центрирования по вертикали
	bool Auto_horizon(TDS_TASK task);//автоматического приведения в горизонтальное состояние

	bool Write_WaterP(TDS_TASK task, double p); //Установить давление в прожарном рукове 
	bool Write_Valve(TDS_TASK task, double value); //Установить ручки открытия заслонки (0 - 4095)
	bool Write_Dispenser(TDS_TASK task, double value); //Установить состояние распылителя (0 - 4095)
	bool Write_Composition(TDS_TASK task, double value);  //Установить состав смеси (0 - вода, 1 - пена)
	bool Write_Pulse(TDS_TASK task, int id, double value);  //Установить состояние пульса id - идентификатор пожарного (0, 1)
	bool Write_AirP(TDS_TASK task, int id, double value);  //Установить давление в баллоне
	double Read_WaterP(TDS_TASK task);  //Получить давление в пожарном рукове 
	double Read_Valve(TDS_TASK task); //Получить ручки открытия заслонки (0 - 4095)
	double Read_Dispenser(TDS_TASK task); //Получить состояние распылителя (0 - 4095)
	double Read_Composition(TDS_TASK task); //Получить состав смеси (0 - вода, 1 - пена)
	double Read_Pulse(TDS_TASK task, int id); //Получить состояние пульса id - идентификатор пожарного (0, 1)
	double Read_AirP(TDS_TASK task, int id); //Получить давление в баллоне

	bool Write_WaterV(TDS_TASK task, double p); //Установить объем в пожарной цистерне (л)
	double Read_WaterV(TDS_TASK task); //Получить объем в пожарной цистерне (л)

	bool Write_PumpP(TDS_TASK task, double p); //Установить объем в пожарной цистерне (л)
	double Read_PumpP(TDS_TASK task); //Получить объем в пожарной цистерне (л)

	//Координаты объекта
	struct PositionInfo
	{
		float  X; // Координата X
		float  Y; // Координата Y
	};

	/// <summary>
	/// Передача координат
	/// </summary>
	/// <param name="task"></param>
	/// <param name="objectID">0 - пожарный 1, 1 - пожарный 2, 2 - начальник караула, 3 бот, 4 - точка огня</param>
	/// <param name="firefighter"></param>
	void SetPosition(TDS_TASK task, int objectID, const PositionInfo& position);
	void GetPosition(TDS_TASK task, int objectID, PositionInfo& position);

	/// <summary>
	/// Генерация события
	/// </summary>
	/// <param name="senderID">Идентификатор инициатора события (номер персоны)</param>
	/// <param name="msgID">Идентификатор сообщения</param>
	/// <param name="parameter">Параметр</param>
	void RaiseEvent(TDS_TASK task, int senderID, int msgID, int parameter);

#pragma pack(push, 1)
	struct Event
	{
		int senderID;    //Идентификатор инициатора события (номер персоны)
		int msgID;       //Идентификатор сообщения
		int parameter;   //Параметр
		char data[1024]; //Буффер с дополнительными данными
	};
#pragma pack(pop)

	void RaiseEvent2(TDS_TASK task, const Event& event);

	/// <summary>
	/// Получение события, из очереди событий FIFO, полученное событие удаляется из очереди
	/// </summary>
	/// <param name="event">event структура для записи события</param>
	/// <returns>Количестов полученных событий, включая записанное в event</returns>
	int PopEvent(TDS_TASK task, Event& event);
}


