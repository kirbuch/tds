
#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

struct Command
{
    int Code;
    int Value;
};

int main(int argc, char* argv[])
{
    try
    {
        if (argc != 2)
        {
            std::cerr << "Usage: client <host>" << std::endl;
            return 1;
        }

        boost::asio::io_context io_context;

        tcp::resolver resolver(io_context);
        tcp::resolver::results_type endpoints =
            resolver.resolve(argv[1], "daytime");

        tcp::socket socket(io_context);
        boost::asio::connect(socket, endpoints);

        std::cout << "Connect to server 13" << std::endl;

        for (;;)
        {
            //boost::array<char, 128> buf;
            Command cmd;
            boost::system::error_code error;

            size_t len = socket.read_some(boost::asio::buffer(&cmd, sizeof(cmd)), error);

            if (error == boost::asio::error::eof)
                break; // Connection closed cleanly by peer.
            else if (error)
                throw boost::system::system_error(error); // Some other error.

            std::cout << cmd.Code << " " << cmd.Value << std::endl;

            cmd.Code = 3;
            cmd.Value += 1;

            boost::asio::write(socket, boost::asio::buffer(&cmd, sizeof(cmd)), error);
            if (error == boost::asio::error::eof)
                break; // Connection closed cleanly by peer.
            else if (error)
                throw boost::system::system_error(error); // Some other error.
        }
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


std::string make_daytime_string()
{
    using namespace std; // For time_t, time and ctime;
    time_t now = time(0);
    return ctime(&now);
}

int main()
{
    try
    {
        std::cout << "TDSServer start..." << std::endl;

        boost::asio::io_context io_context;

        tcp::acceptor acceptor(io_context, tcp::endpoint(tcp::v4(), 13));

        Command cmd;
        cmd.Code = 2;
        cmd.Value = 3;

        for (;;)
        {

            tcp::socket socket(io_context);
            acceptor.accept(socket);

            std::string message = make_daytime_string();

            boost::system::error_code ignored_error;
            //            boost::asio::write(socket, boost::asio::buffer(message), ignored_error);
            cmd.Code = 2;

            boost::asio::write(socket, boost::asio::buffer(&cmd, sizeof(cmd)), ignored_error);

            boost::asio::read(socket, boost::asio::buffer(&cmd, sizeof(cmd)), ignored_error);

            std::cout << cmd.Code << " " << cmd.Value << std::endl;
        }
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}


//int main()
//{
//	cout << "result";
//
//    //using namespace boost::lambda;
//    //typedef std::istream_iterator<int> in;
//
//    //std::for_each(
//    //    in(std::cin), in(), std::cout << (_1 * 3) << " ");
//
//
//
//
//}