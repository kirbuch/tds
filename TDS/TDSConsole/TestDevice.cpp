#include <cstdlib>
#include <iostream>

#include "TDSConsole.h"
#include "../API/TDSServiceAPI.h"

//#include <deque>
//#include <iostream>

int TestDevice(int argc, char* argv[])
{
	std::cout << "test device" << std::endl;
	std::cout << sizeof(int) << std::endl;

	StartListenPort(4444);

	int count = GetDeviceCount();
	std::cout << "Device count: " << count << std::endl;
		
	TDS_DEVICE device = CreateDevice("127.0.0.1", 4210, 2);
	std::cout << "ConnectToDevice: " << device << std::endl;

	count = GetDeviceCount();
	std::cout << "Device count: " << count << std::endl;

	char code = GetDeviceCode(device);
	std::cout << "Device code:	" << code << std::endl;

	std::cout << "Press any key" << std::endl;
	std::cin.get();

	SetDeviceOn(device, true);
	std::cout << "SetDeviceOn" << std::endl;

	SetDeviceParam(device, 123);
	std::cout << "SetDeviceParam 123" << std::endl;

	int parameteres[9];
	GetDeviceParameters(device, parameteres);
	for (int i = 0; i < 8; i++)
		std::cout << parameteres[i] << " ";
	std::cout << std::endl;

	std::cout << "Press any key" << std::endl;
	std::cin.get();

	DestroyDevice(device);
	std::cout << "Disconnect: " << std::endl;

	count = GetDeviceCount();
	std::cout << "Device count: " << count << std::endl;

	std::cout << "Press any key" << std::endl;
	std::cin.get();

	return 0;
}

