﻿// TestConsole.cpp : Defines the entry point for the application.
//

#include "TDSConsole.h"

#include <cstdlib>
#include <deque>
#include <iostream>
#include <thread>
#include <boost/asio.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "../API/message.hpp"
#include "../API/TDSClientAPI.h"
#include "../API/PlatformParameters.h"

int TestDevice(int argc, char* argv[]);

using boost::asio::ip::tcp;

typedef std::deque<tds_message> tds_message_queue;

//boost::uuids::uuid TDS_MSG_POSITION = boost::uuids::string_generator()("{C94C1CE5-5B4A-4A0D-8A4F-DAC7C709255B}");

class tds_client
{
public:
	tds_client(boost::asio::io_context& io_context,
		const tcp::resolver::results_type& endpoints)
		: io_context_(io_context),
		socket_(io_context)
	{
		do_connect(endpoints);
	}

	void write(const tds_message& msg)
	{
		boost::asio::post(io_context_,
			[this, msg]()
			{
				bool write_in_progress = !write_msgs_.empty();
				write_msgs_.push_back(msg);
				if (!write_in_progress)
				{
					do_write();
				}
			});
	}

	void close()
	{
		boost::asio::post(io_context_, [this]() { socket_.close(); });
	}

private:
	void do_connect(const tcp::resolver::results_type& endpoints)
	{
		boost::asio::async_connect(socket_, endpoints,
			[this](boost::system::error_code ec, tcp::endpoint)
			{
				if (!ec)
				{
					do_read_header();
				}
			});
	}

	void do_read_header()
	{
		boost::asio::async_read(socket_,
			boost::asio::buffer(read_msg_.data(), tds_message::header_length),
			[this](boost::system::error_code ec, std::size_t /*length*/)
			{
				if (!ec)
				{	
					do_read_body();
				}
				else
				{
					socket_.close();
				}
			});
	}

	void do_read_body()
	{
		boost::asio::async_read(socket_,
			boost::asio::buffer(read_msg_.body(), read_msg_.body_length()),
			[this](boost::system::error_code ec, std::size_t /*length*/)
			{
				if (!ec)
				{
					if (read_msg_.code() == TDS_MSG_POSITION)
					{
						//std::cout << "TDS_MSG_POSITION ";
						//PlatformParameters param;
						//memcpy(&param, read_msg_.body(), read_msg_.body_length());
						//std::cout << "speed: " << param.Speed << " direction:" << param.Direction;
					}
					else
					{
						std::cout << "msg code: " << read_msg_.code() << ": ";
						std::cout.write(read_msg_.body(), read_msg_.body_length());
						std::cout << "\n";
					}
					do_read_header();
				}
				else
				{
					socket_.close();
				}
			});
	}

	void do_write()
	{
		boost::asio::async_write(socket_,
			boost::asio::buffer(write_msgs_.front().data(),
				write_msgs_.front().length()),
			[this](boost::system::error_code ec, std::size_t /*length*/)
			{
				if (!ec)
				{
					write_msgs_.pop_front();
					if (!write_msgs_.empty())
					{
						do_write();
					}
				}
				else
				{
					socket_.close();
				}
			});
	}

private:
	boost::asio::io_context& io_context_;
	tcp::socket socket_;
	tds_message read_msg_;
	tds_message_queue write_msgs_;
};

const char* MakeMessage(char* buffer, char letter, char device, int param)
{
	char buf[14];
	buffer[0] = letter;
	buffer[2] = device;
	sprintf(buf, "%c0%c%010d", letter, device, param);

	//std::string str = std::format("{:+06d}", c);
	//itoa(param, buffer + 3, 10);
	//const char* result = 

	return (const char*)memcpy(buffer, buf, 13);;
}

static std::map<std::string, int> g_map;

int main(int argc, char* argv[])
{
	boost::uuids::string_generator gen;
	//boost::uuids::uuid u2 = gen("{A1CAD5D2-919B-4412-B288-346752D680D7}");

	std::cout << TDS_MSG_MOVE_PARAMETER << std::endl;
	std::cout << TDS_MSG_POSITION << std::endl;
	std::cout << TDS_MSG_EVENT << std::endl;

	std::cout << Version() << std::endl;


	//return TestDevice(argc, argv);

	//if (argc >= 2 && strcmp(argv[1], "test_device") == 0)
	//{
	//	return TestDevice(argc, argv);
	//}

	try
	{
		if (argc != 3)
		{
			std::cerr << "Usage: tds_client <host> <port>\n";
			return 1;
		}

		boost::asio::io_context io_context;

		tcp::resolver resolver(io_context);
		auto endpoints = resolver.resolve(argv[1], argv[2]);
		tds_client c(io_context, endpoints);

		std::thread t([&io_context]() { io_context.run(); });

		char line[tds_message::max_body_length + 1];
		while (std::cin.getline(line, tds_message::max_body_length + 1))
		{
			if (strcmp(line, "pos") == 0)
			{
				PlatformParameters param;
				tds_message msg(TDS_MSG_POSITION, sizeof(PlatformParameters), &param);
				c.write(msg);
			}
			else
			{
				tds_message msg(TDS_MSG_MOVE_PARAMETER, std::strlen(line), line);
				c.write(msg);
			}
		}

		c.close();
		t.join();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}
