﻿// TestConsole.cpp : Defines the entry point for the application.
//

#include <thread>
#include <iostream>
#include <boost/uuid/string_generator.hpp>

#include "../API/PlatformParameters.h"
#include "../API/TDSServiceAPI.h"

#include "TestPlatform.h"
#include "NetPlatform.h"
#include "TDSClient.h"

#define _USE_MATH_DEFINES // для C++
#include <cmath>

using namespace std;

int g_CurrentTask = 0;

double Version()
{
	return 0.95;
}

TDS_TASK StartTask(const char* host /*= 0*/)
{
	Task* pTask = 0;
	if (host == 0)
	{
		TestPlatform* pTestTask = new TestPlatform();
		pTestTask->Start();
		pTask = pTestTask;
	}
	else
	{
		NetPlatform* pNetTask = new NetPlatform(host, "2121");
		pNetTask->Start();
		pTask = pNetTask;
	}

	return pTask;
}

void StopTask(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	pTask->Stop();
	//delete pTask;
}

int GetTestValue(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->GetTestValue();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Service API

const PlatformParameters& GetParameters(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->GetParameters();
}

void GetParameters2(TDS_TASK task, PlatformParameters& params)
{
	Task* pTask = (Task*)task;
	params = pTask->GetParameters();
}


bool SetParameters(TDS_TASK task, PlatformParameters parameters)
{
	Task* pTask = (Task*)task;
	return pTask->SetParameters(parameters);
}


bool SetAutoMove(TDS_TASK task, bool autoMove)
{
	Task* pTask = (Task*)task;
	return pTask->SetAutoMove(autoMove);
}

bool Jump(TDS_TASK task, double height, double speed)
{
	Task* pTask = (Task*)task;
	return pTask->Jump(height, speed);
}

bool SetSpeed(TDS_TASK task, double speed)
{
	Task* pTask = (Task*)task;
	return pTask->SetSpeed(speed);
}

bool SetDirection(TDS_TASK task, double speed)
{
	Task* pTask = (Task*)task;
	return pTask->SetDirection(speed);
}


bool Rotate(TDS_TASK task, double rotateSpeed)
{
	Task* pTask = (Task*)task;
	return pTask->Rotate(rotateSpeed);
}

ParametersCmd PopParameterChanges(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->PopParameterChanges();
}

//производит включение изделия, начальную инициализацию устройств,
//платформа не выходит из парковочного состояния.
//возвращает 1(единицу) в случае успешной инициализации устройств или код ошибки
char StartPlatform(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->StartPlatform();
}


//переводит платформу в парковочное состояние и производит корректное завершение
//работы устройств
//возвращает 1(единицу) в случае успешного завершения работы устройств или код ошибки
char StopPlatform(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->StopPlatform();
}


//калибровка датчиков. При первом запуске выводим платформу в среднее
//положение при помощи спец утилиты, поворачиваем пояс в положение прямо
// в соответствии с меткой (вертикаль калибруем отдельно, см. ниже) далее
//вызываем функцию, она читает датчики,
// записывает показания в файл и в дальнейшем пляшем от этих показаний
char Kalibration(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->Kalibration();
}

//блокирует платформу в текущем положении до поступления сигнала о снятии тревоги
//1 - тревога (аварийный останов), 0 - отмена аварии. Возвращает 0 в случае удачи
//и 1 в случае не удачи
bool Alarm(TDS_TASK task, char data)
{
	Task* pTask = (Task*)task;
	return pTask->alarm(data);
}

//задаёт режим работы. 1 - рабочий режим, 2 - режим настройки и тестирования
//возвращает 0 в случае успешного включения режима или код ошибки
//различие режимов состоит в том, что в рабочем режиме нельзя менять настройки
//функции настроек (значение записываются в файл и используются при работе платформы во всех режимах)
char Set_mode(TDS_TASK task, char data)
{
	Task* pTask = (Task*)task;
	return pTask->Set_mode(data);
}


//TODO: не понятныо почему два аргумента
//установить угол крена, принимает значения от 0 до 360.
bool Set_angle_kren(TDS_TASK task, double xmin, double xmax)
{
	Task* pTask = (Task*)task;
	return pTask->Set_angle_kren(xmin, xmax);
}

//TODO: не понятныо почему два аргумента
//установить угол тангажа, принимает значения от 0 до 360.
bool Set_angle_tangag(TDS_TASK task, double ymin, double ymax)
{
	Task* pTask = (Task*)task;
	return pTask->Set_angle_tangag(ymin, ymax);
}

//TODO: Как эта скорость ограничивает изменения автоматическое углов?
//0 в обоих случаях будет означать среднее положение //TODO: не ясно

//установить макс. скорость отклонения (наклона) мм*с.
bool Set_maxSpeed_deviation(TDS_TASK task, double s)
{
	Task* pTask = (Task*)task;
	return pTask->Set_maxSpeed_deviation(s);
}

//TODO: Как эта скорость ограничивает изменения автоматическое углов?
//установить макс. скорость перемещения (изменение положения по вертикали) мм*с.
bool Set_maxSpeed_move(TDS_TASK task, double s)
{
	Task* pTask = (Task*)task;
	return pTask->Set_maxSpeed_move(s);
}

//установить скорость центрирования
bool Set_Speed_centering(TDS_TASK task, double s)
{
	Task* pTask = (Task*)task;
	return pTask->Set_Speed_centering(s);
}

//функции информирования о состоянии
//TODO: Выбрать формат строк
//string Read_status();//возвращает текущее состояние 0 - норма или код аварии

//возвращает текущее направление движения игрока (12-ти битное значение от 0 до 4095	(4095 = 360))
double Read_direction(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->Read_direction();
}

//возвращает текущее значение скорости перемещения игрока (шага) мм*с, отрицательное значение
//будет означать «пятиться назад»
double Read_speed(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->Read_speed();
}

//возвращает текущее значение высоты пояса игрока (12-ти битное значение от 0 до 4095)
//калибровку предлагаю проводить под каждого игрока, т.е. перед запуском программы
//считываем текущее положение высоты, отклонение от этого значения более чем на 100 мм
//вниз считаем присядом, выше прыжком (100мм для примера)
double Read_height_position(TDS_TASK task)
{
	Task* pTask = (Task*)task;

	return pTask->Read_height_position();
}

//Высота платфорсы
//возвращает текущее значение высоты подвижности, значения от -34мм до 45мм.
double Read_height(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->Read_height();
}

//возвращает текущее значение угла крена, от 0 до 360.
double Read_angle_kren(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->Read_angle_kren();
}

//возвращает текущее значение угла тангажа, от 0 до 360.
double Read_angle_tangag(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->Read_angle_tangag();
}

///////////////////////////////////////////////////////////////////////////////////////////
//функции команды

//задаёт изменение крена на указанную величину и с заданной скоростью
bool Move_kren(TDS_TASK task, double x, double s)
{
	Task* pTask = (Task*)task;
	return pTask->Move_kren(x, s);
}

//задаёт изменение крена на указанную величину и с заданной скоростью
bool Move_tangag(TDS_TASK task, double y, double s)
{
	Task* pTask = (Task*)task;
	return pTask->Move_tangag(y, s);
}

//задаёт изменение курсового угла
bool Move_azimuth(TDS_TASK task, double angle)
{
	Task* pTask = (Task*)task;
	return pTask->Move_azimuth(angle);
}

//TODO: не ясно --- изменение высоты платформы
//задаёт изменение высоты на указанную величину и с заданной скоростью
bool Move_height(TDS_TASK task, double x, double s)
{
	Task* pTask = (Task*)task;
	return pTask->Move_height(x, s);
}

//TODO: не ясно - одновременное изменение тангажа и крена
//задаёт изменение по двум координатам на указанную величину и с заданной скоростью
//автоматические функции
bool Move(TDS_TASK task, double x, double y, double s)
{
	Task* pTask = (Task*)task;
	return pTask->Move(x, y, s);
}

//TODO: не ясно -- вызываем для авто центровки
//автоматического центрирования по вертикали
bool Auto_centr(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->Auto_centr();
}

//автоматического приведения в горизонтальное состояние
bool Auto_horizon(TDS_TASK task)
{
	Task* pTask = (Task*)task;
	return pTask->Auto_horizon();;
}


bool Write_WaterP(TDS_TASK task, double p) //Установить давление в прожарном рукове 
{
	Task* pTask = (Task*)task;
	return pTask->WriteWaterP(p);
}


bool Write_Valve(TDS_TASK task, double value) //Установить ручки открытия заслонки (0 - 4095)
{
	Task* pTask = (Task*)task;
	return pTask->Write_Valve(value);
}

bool Write_Dispenser(TDS_TASK task, double value) //Установить состояние распылителя (0 - 4095)
{
	Task* pTask = (Task*)task;
	return pTask->Write_Dispenser(value);
}

bool Write_Composition(TDS_TASK task, double value)  //Установить состояние распылителя (0 - вода, 1 - пена)
{
	Task* pTask = (Task*)task;
	return pTask->Write_Composition(value);
}

bool Write_Pulse(TDS_TASK task, int id, double value)  //Установить состояние пульса id - идентификатор пожарного (0, 1)
{
	Task* pTask = (Task*)task;
	return pTask->Write_Pulse(id, value);
}

bool Write_AirP(TDS_TASK task, int id, double value)
{
	Task* pTask = (Task*)task;
	return pTask->Write_AirP(id, value);
}

double Read_WaterP(TDS_TASK task)  //Получить давление в прожарном рукове 
{
	Task* pTask = (Task*)task;
	return pTask->ReadWaterP();
}

double Read_Valve(TDS_TASK task) //Получить ручки открытия заслонки (0 - 4095)
{
	Task* pTask = (Task*)task;
	return pTask->Read_Valve();
}

double Read_Dispenser(TDS_TASK task) //Получить состояние распылителя (0 - 4095)
{
	Task* pTask = (Task*)task;
	return pTask->Read_Dispenser();

}

double Read_Composition(TDS_TASK task) //Получить состояние распылителя (0 - вода, 1 - пена)
{
	Task* pTask = (Task*)task;
	return pTask->Read_Composition();

}

double Read_Pulse(TDS_TASK task, int id) //Получить состояние пульса id - идентификатор пожарного (0, 1)
{
	Task* pTask = (Task*)task;
	return pTask->Read_Pulse(id);

}

double Read_AirP(TDS_TASK task, int id)
{
	Task* pTask = (Task*)task;
	return pTask->Read_AirP(id);
}


bool Write_WaterV(TDS_TASK task, double value)
{
	Task* pTask = (Task*)task;
	return pTask->Write_WaterV(value);
}

double Read_WaterV(TDS_TASK task) //Получить объем в пожарной цистерне (л)
{
	Task* pTask = (Task*)task;
	return pTask->Read_WaterV();
}

bool Write_PumpP(TDS_TASK task, double value)
{
	Task* pTask = (Task*)task;
	return pTask->Write_PumpP(value);
}

double Read_PumpP(TDS_TASK task) //Получить объем в пожарной цистерне (л)
{
	Task* pTask = (Task*)task;
	return pTask->Read_PumpP();
}

void SetPosition(TDS_TASK task, int objectID, const PositionInfo& position)
{
}

void GetPosition(TDS_TASK task, int objectID, PositionInfo& position)
{
}

void RaiseEvent(TDS_TASK task, int senderID, int msgID, int parameter)
{
	Event event;

	event.senderID = senderID;
	event.msgID = msgID;
	event.parameter = parameter;

	RaiseEvent2(task, event);
}

void RaiseEvent2(TDS_TASK task, const Event& event)
{
	Task* pTask = (Task*)task;
	pTask->RaiseEvent(event);
}

int PopEvent(TDS_TASK task, Event& event)
{
	Task* pTask = (Task*)task;
	return pTask->PopEvent(event);
}

const double PI = 3.141592653589793;

double _A(double angle)
{
	angle = angle <= 180 ? angle : angle - 360;
	angle = angle >= -180 ? angle : angle + 360;

	return angle;
}


double CalculatePitch(double alpha, double beta)
{
	int sign = 1;
	if (beta > 90 && beta < 180) {
		beta = 180 - beta;
		sign = -1;
	}
	else if (beta > 270) {
		beta = -beta;
	}

	double A2 = alpha;
	double B2 = beta;

	double D2 = 45 * PI / 180;
	double E2 = 45;
	double G2 = 630;
	double M2 = G2 * sin(A2 * PI / 180);
	double J2 = sqrt(G2 * G2 * 2);
	double F2 = asin(M2 / J2);
	double H2 = cos(A2 * PI / 180) * G2;
	double I2 = H2 * sin(D2) / sin((180 - 45 - B2) * PI / 180);
	double K2 = H2 * sin(B2 * PI / 180) / sin((180 - 45 - B2) * PI / 180);
	double L2 = G2 * sin((90 - B2) * PI / 180) / sin((180 - 45 - 90 + B2) * PI / 180);
	double N2 = L2 * tan(F2);
	double O2 = K2 * tan(F2);
	double P2 = sqrt(N2 * N2 + I2 * I2);

	double gamma = atan(N2 / I2) / PI * 180;

	if (gamma < 0) gamma += 360;

	return _A(sign * gamma);
}

double CalculateRoll(double alpha, double beta)
{
	int sign = 1;
	if (beta > 90 && beta < 180) {
		beta = 180 - beta;
		//sign = -1;
	}
	else if (beta > 270) {
		beta = -beta;
		sign = -1;
	}

	double A8 = alpha;
	double B8 = 90 - beta;

	double D8 = 45 * PI / 180;

	double E8 = 45;
	double G8 = 630;
	double M8 = G8 * sin(A8 * PI / 180);
	double J8 = sqrt(G8 * G8 * 2);
	double F8 = asin(M8 / J8);
	double H8 = cos(A8 * PI / 180) * G8;
	double I8 = H8 * sin(D8) / sin((180 - 45 - B8) * PI / 180);
	double K8 = H8 * sin(B8 * PI / 180) / sin((180 - 45 - B8) * PI / 180);
	double L8 = G8 * sin((90 - B8) * PI / 180) / sin((180 - 45 - 90 + B8) * PI / 180);
	double N8 = L8 * tan(F8);
	double P8 = sqrt(N8 * N8 + I8 * I8);
	double tau = atan(N8 / I8) / PI * 180;

	return _A(sign * tau);
}
