#ifndef TEST_PLATFORM_HPP
#define TEST_PLATFORM_HPP

#include "Platform.h"

#include <queue>

const static double HMN_SPEED = 1500;

////TODO: Change to class
class TestPlatform : public Task
{
public:
	TestPlatform() {}

	virtual char StartPlatform() override;
	virtual char StopPlatform() override;
	virtual char Kalibration() override;
	virtual bool alarm(char code) override;
	virtual char Set_mode(char data) override;
	virtual bool Set_angle_kren(double xmin, double xmax) override;

	//TODO: �� �������� ������ ��� ���������
	//���������� ���� �������, ��������� �������� �� 0 �� 360.
	virtual bool Set_angle_tangag(double ymin, double ymax) override;
	virtual bool Set_maxSpeed_deviation(double s) override;
	virtual bool Set_maxSpeed_move(double s) override;

	//���������� �������� �������������
	virtual bool Set_Speed_centering(double s) override;

	//����� ��������� ����� �� ��������� �������� � � �������� ���������
	virtual bool Move_kren(double x, double s) override;

	//����� ��������� ����� �� ��������� �������� � � �������� ���������
	virtual bool Move_tangag(double y, double s) override;

	//����� ��������� ��������� ����
	virtual bool Move_azimuth(double angle) override;

	bool Move_height(double x, double s) override;

	virtual bool Move(double x, double y, double s) override;

		//TODO: �� ���� -- �������� ��� ���� ���������
	//��������������� ������������� �� ���������
	virtual bool Auto_centr() override;

	//��������������� ���������� � �������������� ���������
	virtual bool Auto_horizon() override;

	virtual bool SetAutoMove(bool autoMove) override;
	virtual bool Jump(double height, double speed) override;
	virtual bool SetSpeed(double speed)  override;
	virtual bool SetDirection(double direction) override;
	virtual bool Rotate(double rotateSpeed)  override;

	virtual bool WriteWaterP(double value) override;
	virtual double ReadWaterP() override;
	virtual bool Write_WaterV(double value) override;
	virtual double Read_WaterV() override;
	virtual bool Write_Valve(double value) override; //���������� ����� �������� �������� (0 - 4095)
	virtual double Read_Valve() override; //�������� ����� �������� �������� (0 - 4095)
	virtual bool Write_Dispenser(double value) override; //���������� ��������� ����������� (0 - 4095)
	virtual double Read_Dispenser() override; //�������� ��������� ����������� (0 - 4095)
	virtual bool Write_Composition(double value) override;  //���������� ��������� ����������� (0 - ����, 1 - ����)
	virtual double Read_Composition() override; //�������� ��������� ����������� (0 - ����, 1 - ����)
	virtual bool Write_Pulse(int id, double value) override;  //���������� ��������� ������ id - ������������� ��������� (0, 1)
	virtual double Read_Pulse(int id) override; //�������� ��������� ������ id - ������������� ��������� (0, 1)
	virtual bool Write_AirP(int id, double value) override;  //���������� �������� � �������
	virtual double Read_AirP(int id) override; //�������� �������� � �������

	virtual bool Write_PumpP(double value) override;
	virtual double Read_PumpP() override;

	virtual void RaiseEvent(const Event& event) override;

private:
	double startTime = 0;
	double lastRunTime = 0;
	double moveTime = 0;

	double height_position_target = _parameters.Height_Position;
	double height_position_speed = 0;

	double _rotate = 0;
	double _directionTarget = 0;

    void RunWorking(double delta);
protected:
	virtual void run() override;
};

#endif