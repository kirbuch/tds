#include <thread>
#include <iostream>
#include <cstdlib>
#include <deque>
#include <boost/asio.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "NetPlatform.h"

#include "../API/message.hpp"
#include "../API/TDSClientAPI.h"
#include "../API/PlatformParameters.h"

using boost::asio::ip::tcp;

typedef std::deque<tds_message> tds_message_queue;

class tds_client
{
public:
	tds_client(NetPlatform* platform, boost::asio::io_context& io_context,
		const tcp::resolver::results_type& endpoints)
		: _platform(platform),
		io_context_(io_context),
		socket_(io_context)
	{
		do_connect(endpoints);
	}

	void write(const tds_message& msg)
	{
		boost::asio::post(io_context_,
			[this, msg]()
			{
				bool write_in_progress = !write_msgs_.empty();
				write_msgs_.push_back(msg);
				if (!write_in_progress)
				{
					do_write();
				}
			});
	}

	void close()
	{
		boost::asio::post(io_context_, [this]() { socket_.close(); });
	}

private:
	void do_connect(const tcp::resolver::results_type& endpoints)
	{
		boost::asio::connect(socket_, endpoints);
		do_read_header();
	}

	void do_read_header()
	{
//		std::cout << "do_read_header";

		boost::asio::async_read(socket_,
			boost::asio::buffer(read_msg_.data(), tds_message::header_length),
			[this](boost::system::error_code ec, std::size_t /*length*/)
			{
				if (!ec)
				{
					read_msg_.decode_header();
					do_read_body();
				}
				else
				{
					socket_.close();
				}
			});
	}

	void do_read_body()
	{
		boost::asio::async_read(socket_,
			boost::asio::buffer(read_msg_.body(), read_msg_.body_length()),
			[this](boost::system::error_code ec, std::size_t /*length*/)
			{
				if (!ec)
				{
					if (read_msg_.code() == TDS_MSG_POSITION)
					{
						memcpy(&_platform->Parameters(), read_msg_.body(), read_msg_.body_length());
					}
					else if (read_msg_.code() == TDS_MSG_EVENT)
					{
						Event event;
						memcpy(&event, read_msg_.body(), read_msg_.body_length());
						//std::cout << "msgID: " << event.msgID << " senderID:" << event.senderID << " parameter:" << event.parameter;
						_platform->AddEvent(event);
						//RaiseEvent2(_task, event);
					}
					else if (read_msg_.code() == TDS_MSG_MOVE_PARAMETER)
					{
						ChangeParameters changes;
						memcpy(&changes, read_msg_.body(), read_msg_.body_length());
						_platform->SetParameterChanges(changes);
					}
					else
					{
						std::cout << "unknown msg code: " << read_msg_.code() << ": ";
						std::cout.write(read_msg_.body(), read_msg_.body_length()) << std::endl; 
					}
					do_read_header();
				}
				else
				{
					std::cerr << "error: " << ec.message() << std::endl;
					socket_.close();
				}
			});
	}

	void do_write()
	{
		boost::asio::async_write(socket_,
			boost::asio::buffer(write_msgs_.front().data(),
				write_msgs_.front().length()),
			[this](boost::system::error_code ec, std::size_t /*length*/)
			{
				if (!ec)
				{
					write_msgs_.pop_front();
					if (!write_msgs_.empty())
					{
						do_write();
					}
				}
				else
				{
					socket_.close();
				}
			});
	}

private:
	boost::asio::io_context& io_context_;
	tcp::socket socket_;
	tds_message read_msg_;
	tds_message_queue write_msgs_;

	NetPlatform* _platform = 0;
};

bool NetPlatform::Move_height(double x, double s)
{
	//HACK
	if (!IsWorking())
		return false;

	if (_pNetClient == 0)
		return false;

	ChangeParameters msgChange(EParameters::PARAM_Height, x, 0, s);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

//����� ��������� ��������� ����
bool NetPlatform::Move_azimuth(double angle)
{
	ChangeParameters msgChange(EParameters::PARAM_Azimuth, angle, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

//����� ��������� ����� �� ��������� �������� � � �������� ���������
bool NetPlatform::Move_kren(double x, double s)
{
	//TODO: ������, �������� ������ ������
	if (x < _parameters.angle_kren_min || x > _parameters.angle_kren_max)
		return false;
	if (!IsWorking())
		return false;
	if (_pNetClient == 0)
		return false;

	ChangeParameters msgChange(EParameters::PARAM_Kren, x, 0, s);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

bool NetPlatform::Move_tangag(double y, double s)
{
	//TODO: ������, �������� ������ ������
	if (y < _parameters.angle_tangag_min || y > _parameters.angle_tangag_max)
		return false;
	if (!IsWorking())
		return false;
	if (_pNetClient == 0)
		return false;

	ChangeParameters msgChange(EParameters::PARAM_Tangag, 0, y, s);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

bool NetPlatform::Move(double x, double y, double s)
{
	if (x < _parameters.angle_kren_min || x > _parameters.angle_kren_max)
		return false;
	if (y < _parameters.angle_tangag_min || y > _parameters.angle_tangag_max)
		return false;
	if (!IsWorking())
		return false;
	if (_pNetClient == 0)
		return false;

	ChangeParameters msgChange(EParameters::PARAM_Angle, x, y, s);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

bool NetPlatform::SetAutoMove(bool autoMove)
{
	ChangeParameters msgChange(EParameters::PARAM_AutoMove, autoMove ? 1 : 0, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

bool NetPlatform::Jump(double height, double speed)
{
	ChangeParameters msgChange(EParameters::PARAM_Jump, 0, height, speed);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

bool NetPlatform::SetSpeed(double speed)
{
	ChangeParameters msgChange(EParameters::PARAM_Speed, 0, 0, speed);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

bool NetPlatform::SetDirection(double direction)
{
	ChangeParameters msgChange(EParameters::PARAM_Direction, direction, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

bool NetPlatform::Rotate(double rotateSpeed)
{
	ChangeParameters msgChange(EParameters::PARAM_Rotate, 0, 0, rotateSpeed);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}


bool NetPlatform::WriteWaterP(double value)
{
	_parameters.Firehose_waterP = value;
	ChangeParameters msgChange(EParameters::PARAM_Firehose_waterP, value, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

double NetPlatform::ReadWaterP()
{
	return _parameters.Firehose_waterP;
}

bool NetPlatform::Write_WaterV(double value)
{
	_parameters.Firehose_waterV = value;
	ChangeParameters msgChange(EParameters::PARAM_Firehose_waterV, value, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

double NetPlatform::Read_WaterV()
{
	return _parameters.Firehose_waterV;
}

bool NetPlatform::Write_PumpP(double value)
{
	_parameters.Firehose_pumpP = value;
	ChangeParameters msgChange(EParameters::PARAM_Firehose_pumpP, value, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

double NetPlatform::Read_PumpP()
{
	return _parameters.Firehose_pumpP;
}

bool NetPlatform::Write_Valve(double value)
{
	_parameters.Firehose_valve = value;
	ChangeParameters msgChange(EParameters::PARAM_Firehose_valve, value, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);
	return true;
}

double NetPlatform::Read_Valve()
{
	return 	_parameters.Firehose_valve;
}

bool NetPlatform::Write_Dispenser(double value)
{
	_parameters.Firehose_dispenser = value;
	ChangeParameters msgChange(EParameters::PARAM_Firehose_dispenser, value, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);
	return true;
}

double NetPlatform::Read_Dispenser()
{
	return 	_parameters.Firehose_dispenser;
}

bool NetPlatform::Write_Composition(double value)
{
	_parameters.Firehose_composition = value;
	ChangeParameters msgChange(EParameters::PARAM_Firehose_composition, value, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

double NetPlatform::Read_Composition()
{
	return 	_parameters.Firehose_composition;
}

bool NetPlatform::Write_Pulse(int id, double value)
{
	EParameters param = id == 0 ? EParameters::PARAM_Firehose_pulse0 : EParameters::PARAM_Firehose_pulse1;

	ChangeParameters msgChange(param, value, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

double NetPlatform::Read_Pulse(int id)
{
	return id == 0 ? _parameters.Firehose_pulse0 : _parameters.Firehose_pulse1;
}

bool NetPlatform::Write_AirP(int id, double value)
{
	EParameters param = id == 0 ? EParameters::PARAM_Firehose_airP0 : EParameters::PARAM_Firehose_airP1;

	ChangeParameters msgChange(param, value, 0, 0);
	tds_message msg(TDS_MSG_MOVE_PARAMETER, sizeof(ChangeParameters), &msgChange);
	_pNetClient->write(msg);

	return true;
}

double NetPlatform::Read_AirP(int id)
{
	return id == 0 ? _parameters.Firehose_airP0 : _parameters.Firehose_airP1;
}

void NetPlatform::RaiseEvent(const Event& event)
{
	tds_message msg(TDS_MSG_EVENT, sizeof(Event), &event);
	_pNetClient->write(msg);
}

void NetPlatform::Start()
{
	if (_pNetClient != 0)
		return;

	tcp::resolver resolver(_io_context);
	auto endpoints = resolver.resolve(_host, _port);
	_pNetClient = new tds_client(this, _io_context, endpoints);

	Task::Start();
}

void NetPlatform::Stop()
{
	if (_pNetClient)
	{
		_pNetClient->close();

		Task::Stop();

		delete _pNetClient;
		_pNetClient = 0;
	}
}

void NetPlatform::run()
{
	_io_context.run();
}

bool NetPlatform::OnSetParameters()
{
	tds_message msg(TDS_MSG_POSITION, sizeof(PlatformParameters), &_parameters);
	_pNetClient->write(msg);

	return true;
}
