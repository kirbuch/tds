//
// DeviceUDPClient.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2010 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdlib>
#include <cstring>
#include <iostream>

#include <boost/asio.hpp>

#include "Device.h"

#include "../API/TDSDevicesAPI.h"
#include "../API/TDSServiceAPI.h"

//class Device
//{
//public:
//    //Device(const char* server, const char* port, int deviceID = -1) //:
//    //	//query(udp::v4(), server, port)
//    //{
//    //}
//
//private:
//    //udp::resolver::query query;
//    char isOn;
//    int parameter;
//};

using boost::asio::ip::udp;

enum { max_length = 1024 };


void SetFirehoseOn(bool isOn)
{
}

void SetFirehoseLoad(float kLoad)
{
}

int GetFirehoseInfo(FirehoseInfo* pInfo)
{
	return 0;
}

void SetHeaterOn(char person, bool isOn)
{
}

void SetHeaterDestTemp(char person, char newTemp) {}

int GetHeaterInfo(char person, HeaterInfo* pInfo) { return 0; }
void SetVentilatorOn(bool isOn) {}
void SetVentilatorMode(char newMode) {}
int GetVentilatorInfo(VentilatorInfo* pInfo) { return 0; }

TDS_DEVICE CreateDevice(const char* server, int port, char typeId)
{
	Device* pDevice = new Device(server, std::to_string(port), (EDeviceType)typeId);

	pDevice->Start();

	return pDevice;
}


int GetDeviceCount()
{
	return 0;
}

char GetDeviceCode(TDS_DEVICE device)
{
	Device* pDevice = (Device*)device;
	return pDevice->DeviceCode();
}

void GetDeviceParameters(TDS_DEVICE device, int* buffer)
{
	Device* pDevice = (Device*)device;
	memcpy(buffer, pDevice->Parameters, (Device::ParametersCount + 1) * sizeof(int));
}

void SetDeviceOn(TDS_DEVICE device, bool isOn)
{
	Device* pDevice = (Device*)device;
	return pDevice->SetOn(isOn);
}

void SetDeviceParam(TDS_DEVICE device, int parameter)
{
	Device* pDevice = (Device*)device;
	return pDevice->setParameter(parameter);
}

time_t GetLastUpdateTime(TDS_DEVICE device)
{
	return time(0);
}

void DestroyDevice(TDS_DEVICE device)
{
	Device* pDevice = (Device*)device;
	//pDevice->Stop();
	delete pDevice;

}


std::thread* pThreadServer = 0;
static std::map<std::string, Device*> g_map;

void server(boost::asio::io_service& io_service, short port)
{
	udp::socket sock(io_service, udp::endpoint(udp::v4(), port));
	for (;;)
	{
		char data[max_length];
		udp::endpoint sender_endpoint;
		size_t length = sock.receive_from(
			boost::asio::buffer(data, max_length), sender_endpoint);

		std::string adress = sender_endpoint.address().to_string();

		auto device = g_map.find(adress);
		if (device != g_map.end())
			device->second->HandleData(data, length);
	}
}

int server_blocking(int port)
{
	try
	{
		boost::asio::io_service io_service;

		using namespace std; // For atoi.
		server(io_service, port);
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}

void StartListenPort(int port)
{
	StopListenPort();

	pThreadServer = new std::thread(server_blocking, port);
}

//TODO: check
void StopListenPort()
{
	if (pThreadServer != 0)
	{
		//pThreadServer->join()
		delete pThreadServer;
	}

	pThreadServer = 0;
}


int device_connect(Device* pDevice)
{
	try
	{
		pDevice->Run();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}

void Device::Start()
{
	_running = true;
	_pThread = new std::thread(device_connect, this);
	g_map[_server] = this;
}

void Device::Stop()
{
	_running = false;
	g_map.erase(_server);
}

void Device::HandleData(char* data, size_t length)
{
	try
	{
		int idx = data[0] - 'A';

		if (idx < ParametersCount)
		{
			std::string value = std::string(data).substr(3, 10);
			Parameters[idx] = std::stoi(value);
		}
		else
			_error = -2;
	}
	catch (...)
	{
		_error = -1;
	}
}


	void Device::Run()
	{
		boost::asio::io_service io_service;

		udp::socket s(io_service, udp::endpoint(udp::v4(), 0));

		udp::resolver resolver(io_service);
		udp::resolver::query query(udp::v4(), _server, _service);
		udp::resolver::iterator iterator = resolver.resolve(query);

		//using namespace std; // For strlen.
		//std::cout << "Enter message: ";
		//char request[max_length];
		//std::cin.getline(request, max_length);

		while (_running)
		{
			size_t request_length = 13;
			char buffer[14];

			MakeMessage(buffer, 'A', DeviceCode(), _isOn ? 1 : 0);
			s.send_to(boost::asio::buffer(buffer, request_length), *iterator);
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));

			if (_isOn)
			{
				MakeMessage(buffer, 'B', DeviceCode(), 1100000000);
				s.send_to(boost::asio::buffer(buffer, request_length), *iterator);
				std::this_thread::sleep_for(std::chrono::milliseconds(1000));

				MakeMessage(buffer, 'C', DeviceCode(), getParameter());
				s.send_to(boost::asio::buffer(buffer, request_length), *iterator);
				std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			}
		}

		//char reply[max_length];
		//udp::endpoint sender_endpoint;
		//size_t reply_length = s.receive_from(
		//    boost::asio::buffer(reply, max_length), sender_endpoint);
		//std::cout << "Reply is: ";
		//std::cout.write(reply, reply_length);
		//std::cout << "\n";

	}

