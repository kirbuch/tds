#ifndef DEVICE_HPP
#define DEVICE_HPP

#include <memory.h>
#include <boost/asio.hpp>

using boost::asio::ip::udp;

enum EDeviceType
{
	Unknown = 0,
	Firehose,
	Heater,
	Ventilator
};

class Device
{
public:
	static const int ParametersCount = 8;

public:
	Device(std::string server, std::string service, EDeviceType deviceType) :
		_server(server), _service(service), DeviceType(deviceType)
	{
		_running = false;
		_isOn = false;
		_pThread = 0;
		_error = 0;

		for (int i = 0; i < ParametersCount + 1; i++)
			Parameters[i] = 0;
	}

	virtual ~Device()
	{
		Stop();
		_pThread->join();
		delete _pThread;
	}

	char DeviceCode()
	{
		switch (DeviceType)
		{
		case Unknown:
			return '0';
		case Firehose:
			return '1';
		case Heater:
			return '2';
		case Ventilator:
			return '3';
		default:
			return 'E';
		}
	}

	void SetOn(bool isOn) { _isOn = isOn; }

	bool IsRunning() { return _running; }

	void setParameter(int newValue)
	{
		Parameters[ParametersCount] = newValue;
	}

	int getParameter()
	{
		return Parameters[ParametersCount];
	}

	void Start();
	void Stop();
	void Run();

	void HandleData(char* data, size_t length);

public:
	static const char* MakeMessage(char* buffer, char letter, char device, int param)
	{
		char buf[14];
		buffer[0] = letter;
		buffer[2] = device;
		sprintf(buf, "%c0%c%010d", letter, device, param);

		//std::string str = std::format("{:+06d}", c);
		//itoa(param, buffer + 3, 10);
		//const char* result = 

		return (const char*)memcpy(buffer, buf, 13);;
	}


public:
	int Parameters[ParametersCount + 1];
	EDeviceType DeviceType;

private:
	bool _isOn;
	std::string _server;
	std::string _service;
	bool _running;
	std::thread* _pThread;
	unsigned _error;
};

#endif