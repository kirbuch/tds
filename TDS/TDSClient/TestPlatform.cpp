﻿#include <thread>
#include <chrono>

#include "TestPlatform.h"
#include "../API/TDSServiceAPI.h"

using namespace std;
using namespace std::chrono;

char TestPlatform::StartPlatform()
{
	_parameters.Started = true;
	_parameters.Alarm = 0;

	return 1;
}

char TestPlatform::StopPlatform()
{
	_parameters.Started = false;
	return 1;
}

char TestPlatform::Kalibration()
{
	_parameters.Started = false;
	return 1;
}

bool TestPlatform::alarm(char code)
{
	_parameters.Alarm = code;
	return true;
}

char TestPlatform::Set_mode(char data)
{
	_parameters.Mode = data;
	return 1;
}

bool TestPlatform::Set_angle_kren(double xmin, double xmax)
{
	if (xmin > 360 || xmax > 360 || xmin > xmax)
		return false;

	//TODO: не понятныо почему два аргумента
	_parameters.angle_kren_min = xmin;
	_parameters.angle_kren_max = xmax;

	return true;
}

//TODO: не понятныо почему два аргумента
//установить угол тангажа, принимает значения от 0 до 360.
bool TestPlatform::Set_angle_tangag(double ymin, double ymax)
{
	if (ymin > 360 || ymax > 360 || ymin > ymax)
		return false;

	_parameters.angle_tangag_min = ymin;
	_parameters.angle_tangag_max = ymax;

	return true;
}

bool TestPlatform::Set_maxSpeed_deviation(double s)
{
	_parameters.maxSpeed_deviation = s;
	return true;
}

bool TestPlatform::Set_maxSpeed_move(double s)
{
	_parameters.maxSpeed_move = s;
	return true;
}

//установить скорость центрировани¤
bool TestPlatform::Set_Speed_centering(double s)
{
	_parameters.Speed_centering = s;
	return true;
}

bool TestPlatform::Move_azimuth(double angle)
{
	_parameters.angle_azimuth = angle;
	
	double tangag = CalculatePitch(_parameters.angle_azimuth, _parameters.Direction);
	Move_tangag(tangag, _parameters.angle_tangag_speed);

	double kren = CalculateRoll(_parameters.angle_azimuth, _parameters.Direction);
	Move_kren(kren, _parameters.angle_kren_speed);

	return true;
}

//задает изменение крена на указанную величину и с заданной скоростью
bool TestPlatform::Move_kren(double x, double s)
{
	//TODO: —порно, возможно просто резать
	if (x < _parameters.angle_kren_min || x > _parameters.angle_kren_max)
		return false;

	_parameters.angle_kren_target = x;
	_parameters.angle_kren_speed = s;

	return true;
}

//задает изменение крена на указанную величину и с заданной скоростью
bool TestPlatform::Move_tangag(double y, double s)
{
	//TODO: —порно, возможно просто резать
	if (y < _parameters.angle_tangag_min || y > _parameters.angle_tangag_max)
		return false;

	_parameters.angle_tangag_target = y;
	_parameters.angle_tangag_speed = s;

	return true;
}

bool TestPlatform::Move_height(double x, double s)
{
	if (!IsWorking())
		return false;

	_parameters.Height_target = x;
	_parameters.Height_speed = s;

	return true;
}

bool TestPlatform::Move(double x, double y, double s)
{
	if (x < _parameters.angle_kren_min || x > _parameters.angle_kren_max)
		return false;

	if (y < _parameters.angle_tangag_min || y > _parameters.angle_tangag_max)
		return false;

	_parameters.angle_kren_target = x;
	_parameters.angle_kren_speed = s;
	_parameters.angle_tangag_target = y;
	_parameters.angle_tangag_speed = s;

	return true;
}

//TODO: не ясно -- вызываем дл¤ авто центровки
//автоматического центрировани¤ по вертикали
bool TestPlatform::Auto_centr()
{
	return false;
}
//автоматического приведени¤ в горизонтальное состо¤ние
bool TestPlatform::Auto_horizon()
{
	//TODO: уточнить центровку
	_parameters.angle_kren = 0;
	_parameters.angle_tangag = 0;

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Service methods 

bool TestPlatform::SetAutoMove(bool autoMove)
{
	_parameters._autoMove = autoMove;

	_parameters.Speed = 0;
	_parameters.Direction = 0;
	_rotate = 0;

	return true;
}

bool TestPlatform::Jump(double height, double speed)
{
	height_position_target = height;
	height_position_speed = speed;

	return true;
}

bool TestPlatform::SetSpeed(double speed)
{
	_parameters.Speed = speed;
	return true;
}

bool TestPlatform::SetDirection(double direction)
{
	_parameters.Direction = direction;
	_directionTarget = direction;
	return true;
}

bool TestPlatform::Rotate(double rotateSpeed)
{
	_rotate = rotateSpeed;
	return true;
}

bool TestPlatform::WriteWaterP(double value)
{
	_parameters.Firehose_waterP = value;
	return true;
}

double TestPlatform::ReadWaterP()
{
	return 	_parameters.Firehose_waterP;
}

bool TestPlatform::Write_WaterV(double value)
{
	_parameters.Firehose_waterV = value;
	return true;
}

double TestPlatform::Read_WaterV()
{
	return 	_parameters.Firehose_waterV;
}

bool TestPlatform::Write_PumpP(double value)
{
	_parameters.Firehose_pumpP = value;
	return true;
}

double TestPlatform::Read_PumpP()
{
	return 	_parameters.Firehose_pumpP;
}

bool TestPlatform::Write_Valve(double value)
{
	_parameters.Firehose_valve = value;
	return true;
}

double TestPlatform::Read_Valve()
{
	return 	_parameters.Firehose_valve;
}

bool TestPlatform::Write_Dispenser(double value)
{
	_parameters.Firehose_dispenser = value;
	return true;
}

double TestPlatform::Read_Dispenser()
{
	return 	_parameters.Firehose_dispenser;
}

bool TestPlatform::Write_Composition(double value)
{
	_parameters.Firehose_composition = value;
	return true;
}

double TestPlatform::Read_Composition()
{
	return 	_parameters.Firehose_composition;
}

bool TestPlatform::Write_Pulse(int id, double value)
{
	if (id == 0)
		_parameters.Firehose_pulse0 = value;
	else
		_parameters.Firehose_pulse1 = value;

	return true;
}

double TestPlatform::Read_Pulse(int id)
{
	return id == 0 ? _parameters.Firehose_pulse0 : _parameters.Firehose_pulse1;
}

bool TestPlatform::Write_AirP(int id, double value)
{
	if (id == 0)
		_parameters.Firehose_airP0 = value;
	else
		_parameters.Firehose_airP1 = value;

	return true;
}

double TestPlatform::Read_AirP(int id)
{
	return id == 0 ? _parameters.Firehose_airP0 : _parameters.Firehose_airP1;
}

void TestPlatform::RaiseEvent(const Event& event)
{
	PushEvent(event);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// protected methods 

void MoveU(double& parameter, double target, double speed, double delta)
{
	if (parameter == target)
		return;

	double dval = delta * speed;
	if (abs(parameter - target) > dval)
		parameter += copysign(dval, (double)target - parameter);
	else
		parameter = target;
}

void TestPlatform::run()
{
	//HACK заменить на функцию времени
	startTime = (double)clock() / (double)CLOCKS_PER_SEC;
	lastRunTime = startTime;

	while (!IsStopping())
	{
		//TODO: —делать класс Action

		//HACK заменить на функцию времени
		double currentTime = (double)clock() / (double)CLOCKS_PER_SEC;
		double delta = currentTime - lastRunTime;
		lastRunTime += delta;

		if (IsWorking())
		{
			RunWorking(delta);
		}
		else
		{
			_parameters.Speed = 0;
			_parameters.Direction = 0;
		}

		std::this_thread::sleep_for(10ms);
	}
}

void TestPlatform::RunWorking(double delta)
{
	MoveU(_parameters.Height, _parameters.Height_target, _parameters.Height_speed, delta);
	MoveU(_parameters.angle_kren, _parameters.angle_kren_target, _parameters.angle_kren_speed, delta);
	MoveU(_parameters.angle_tangag, _parameters.angle_tangag_target, _parameters.angle_tangag_speed, delta);
	MoveU(_parameters.Height_Position, height_position_target, height_position_speed, delta);

	Value += 1;
	if (Value > 1000) Value = 0;

	if (!_parameters._autoMove)
	{
		_directionTarget += _rotate * delta;
		if (_directionTarget < 0)
			_directionTarget = DIRECTION_360;
		else if (_directionTarget > DIRECTION_360)
			_directionTarget = 0;

		_parameters.Direction = _directionTarget;
	}
	else
	{
		moveTime += delta;
		if (moveTime > 5)
		{
			moveTime = 0;
			_parameters.Direction += DIRECTION_360 / 4 + 1;
			if (_parameters.Direction >= DIRECTION_360)
				_parameters.Direction = 0;
		}

		_parameters.Speed = HMN_SPEED;

	}
}