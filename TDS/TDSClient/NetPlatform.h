#ifndef NET_PLATFORM_HPP
#define NET_PLATFORM_HPP

#include <boost/asio.hpp>

#include "Platform.h"

//const static double HMN_SPEED = 1500;

class tds_client;

class NetPlatform : public Task
{
public:
	NetPlatform(const char* host, const char* port) : _host(host), _port(port) {}
	virtual ~NetPlatform() { Stop(); }

	virtual char StartPlatform()
	{
		_parameters.Started = true;
		return 1;
	}

	virtual char StopPlatform()
	{
		_parameters.Started = false;
		return 1;
	}

	virtual char Kalibration()
	{
		_parameters.Started = false;
		return 1;
	}

	virtual bool alarm(char code) override
	{
		_parameters.Alarm = code;
		return true;
	}

	virtual char Set_mode(char data) override
	{
		_parameters.Mode = data;
		return 1;
	}

	virtual bool Set_angle_kren(double xmin, double xmax) override
	{
		if (xmin > 360 || xmax > 360 || xmin > xmax)
			return false;

		//TODO: �� �������� ������ ��� ���������
		_parameters.angle_kren_min = xmin;
		_parameters.angle_kren_max = xmax;

		return true;
	}

	//TODO: �� �������� ������ ��� ���������
	//���������� ���� �������, ��������� �������� �� 0 �� 360.
	virtual bool Set_angle_tangag(double ymin, double ymax) override
	{
		if (ymin > 360 || ymax > 360 || ymin > ymax)
			return false;

		_parameters.angle_tangag_min = ymin;
		_parameters.angle_tangag_max = ymax;

		return true;
	}

	virtual bool Set_maxSpeed_deviation(double s) override
	{
		_parameters.maxSpeed_deviation = s;
		return true;
	}

	virtual bool Set_maxSpeed_move(double s) override
	{
		_parameters.maxSpeed_move = s;
		return true;
	}

	//���������� �������� �������������
	virtual bool Set_Speed_centering(double s) override
	{
		_parameters.Speed_centering = s;
		return true;
	}

	//����� ��������� ��������� ����
	virtual bool Move_azimuth(double angle) override;

	//����� ��������� ����� �� ��������� �������� � � �������� ���������
	virtual bool Move_kren(double x, double s) override;
	//����� ��������� ����� �� ��������� �������� � � �������� ���������
	virtual bool Move_tangag(double y, double s) override;
	virtual bool Move_height(double x, double s) override;
	virtual bool Move(double x, double y, double s) override;

	//TODO: �� ���� -- �������� ��� ���� ���������
	//��������������� ������������� �� ���������
	virtual bool Auto_centr() override
	{
		return false;
	}
	//��������������� ���������� � �������������� ���������
	virtual bool Auto_horizon() override
	{
		//TODO: �������� ���������
		_parameters.angle_kren = 0;
		_parameters.angle_tangag = 0;

		return true;
	}

	PlatformParameters& Parameters() { return _parameters; }

	virtual void Stop() override;
	virtual void Start() override;

	virtual bool SetAutoMove(bool autoMove) override;
	virtual bool Jump(double height, double speed) override;
	virtual bool SetSpeed(double speed)  override;
	virtual bool SetDirection(double direction)  override;
	virtual bool Rotate(double rotateSpeed)  override;

	virtual bool WriteWaterP(double value) override;
	virtual double ReadWaterP() override;
	virtual bool Write_WaterV(double value) override;
	virtual double Read_WaterV() override;
	virtual bool Write_Valve(double value) override; //���������� ����� �������� �������� (0 - 4095)
	virtual double Read_Valve() override; //�������� ����� �������� �������� (0 - 4095)

	virtual bool Write_Dispenser(double value) override; //���������� ��������� ����������� (0 - 4095)
	virtual bool Write_Composition(double value) override;  //���������� ��������� ����������� (0 - ����, 1 - ����)
	virtual bool Write_Pulse(int id, double value) override;  //���������� ��������� ������ id - ������������� ��������� (0, 1)
	virtual bool Write_AirP(int id, double value) override;  //���������� �������� � �������
	virtual double Read_Dispenser() override; //�������� ��������� ����������� (0 - 4095)
	virtual double Read_Composition() override; //�������� ��������� ����������� (0 - ����, 1 - ����)
	virtual double Read_Pulse(int id) override; //�������� ��������� ������ id - ������������� ��������� (0, 1)
	virtual double Read_AirP(int id) override; //�������� �������� � �������

	virtual bool Write_PumpP(double value) override;
	virtual double Read_PumpP() override;

	virtual void RaiseEvent(const Event& event) override;

	void AddEvent(const Event& event) { PushEvent(event); };

protected:
	virtual void run() override;
	virtual bool OnSetParameters() override;

private:
	const char* _host;
	const char* _port;
	tds_client* _pNetClient = 0;
	boost::asio::io_context _io_context;
	int _currentMsgNum = -1;

};

#endif