﻿#ifndef PLATFORM_HPP
#define PLATFORM_HPP

#include <queue>

#include "../API/TDSClientAPI.h"
#include "../API/PlatformParameters.h"
#include <mutex>

class Task
{
public:
	virtual ~Task() {
		 Stop(); 
	}

	virtual char StartPlatform() = 0;
	virtual char StopPlatform() = 0;
	virtual char Kalibration() = 0;

	//блокирует платформу в текущем положении до поступления сигнала о снятии тревоги
//1 - тревога (аварийный останов), 0 - отмена аварии. Возвращает 0 в случае удачи
//и 1 в случае не удачи
	virtual bool alarm(char code) = 0;

	//задаёт режим работы. 1 - рабочий режим, 2 - режим настройки и тестирования
	//возвращает 0 в случае успешного включения режима или код ошибки
	//различие режимов состоит в том, что в рабочем режиме нельзя менять настройки
	//функции настроек (значение записываются в файл и используются при работе платформы во всех режимах)
	virtual char Set_mode(char data) = 0;
	//установить угол крена, принимает значения от 0 до 360.
	virtual bool Set_angle_kren(double xmin, double xmax) = 0;
	//установить угол тангажа, принимает значения от 0 до 360.
	virtual bool Set_angle_tangag(double ymin, double ymax) = 0;
	//установить макс. скорость отклонения (наклона) мм*с.
	virtual bool Set_maxSpeed_deviation(double s) = 0;
	//установить макс. скорость перемещения (изменение положения по вертикали) мм*с.
	virtual bool Set_maxSpeed_move(double s) = 0;
	//установить скорость центрирования
	virtual bool Set_Speed_centering(double s) = 0;

	//возвращает текущее направление движения игрока (12-ти битное значение от 0 до 4095	(4095 = 360))
	double Read_azimuth() { return _parameters.angle_azimuth; }
	//возвращает текущее направление движения игрока (12-ти битное значение от 0 до 4095	(4095 = 360))
	double Read_direction_degree() { return 360 *  _parameters.Direction / DIRECTION_360; }
	//возвращает текущее направление движения игрока (12-ти битное значение от 0 до 4095	(4095 = 360))
	double Read_direction() { return _parameters.Direction; }
	//возвращает текущее значение скорости перемещения игрока (шага) мм*с, отрицательное значение
	//будет означать «пятиться назад»
	double Read_speed() { return _parameters.Speed; }

	//возвращает текущее значение высоты пояса игрока (12-ти битное значение от 0 до 4095)
	//калибровку предлагаю проводить под каждого игрока, т.е. перед запуском программы
	//считываем текущее положение высоты, отклонение от этого значения более чем на 100 мм
	//вниз считаем присядом, выше прыжком (100мм для примера)
	double Read_height_position() { return _parameters.Height_Position; }
	//Высота платфорсы
	//возвращает текущее значение высоты подвижности, значения от -34мм до 45мм.
	double Read_height() { return _parameters.Height; }
	//возвращает текущее значение угла крена, от 0 до 360.
	double Read_angle_kren() { return _parameters.angle_kren; }
	//возвращает текущее значение угла тангажа, от 0 до 360.
	double Read_angle_tangag() { return _parameters.angle_tangag; }

	///////////////////////////////////////////////////////////////////////////////////////////
	//функции команды

	//задаёт изменение курсового угла
	virtual bool Move_azimuth(double angle) = 0;
	//задаёт изменение крена на указанную величину и с заданной скоростью
	virtual bool Move_kren(double x, double s) = 0;
	//задаёт изменение крена на указанную величину и с заданной скоростью
	virtual bool Move_tangag(double y, double s) = 0;
	//задаёт изменение высоты на указанную величину и с заданной скоростью
	virtual bool Move_height(double x, double s) = 0;
	//TODO: не ясно - одновременное изменение тангажа и крена
//задаёт изменение по двум координатам на указанную величину и с заданной скоростью
//автоматические функции
	virtual bool Move(double x, double y, double s) = 0;
	//TODO: не ясно -- вызываем для авто центровки
	//автоматического центрирования по вертикали
	virtual bool Auto_centr() = 0;
	//автоматического приведения в горизонтальное состояние
	virtual bool Auto_horizon() = 0;

	virtual bool WriteWaterP(double value) = 0;
	virtual double ReadWaterP() = 0;
	virtual bool Write_WaterV(double value) = 0;
	virtual double Read_WaterV() = 0;
	virtual bool Write_Valve(double value) = 0; //Установить ручку открытия заслонки (0 - 4095)
	virtual double Read_Valve() = 0; //Получить ручки открытия заслонки (0 - 4095)

	virtual bool Write_Dispenser(double value)  = 0; //Установить состояние распылителя (0 - 4095)
	virtual bool Write_Composition(double value)  = 0;  //Установить состояние распылителя (0 - вода, 1 - пена)
	virtual bool Write_Pulse(int id, double value)  = 0;  //Установить состояние пульса id - идентификатор пожарного (0, 1)
	virtual bool Write_AirP(int id, double value)  = 0;  //Установить давление в баллоне
	virtual double Read_Dispenser()  = 0; //Получить состояние распылителя (0 - 4095)
	virtual double Read_Composition()  = 0; //Получить состояние распылителя (0 - вода, 1 - пена)
	virtual double Read_Pulse(int id)  = 0; //Получить состояние пульса id - идентификатор пожарного (0, 1)
	virtual double Read_AirP(int id)  = 0; //Получить давление в баллоне

	virtual bool Write_PumpP(double value) = 0;
	virtual double Read_PumpP() = 0;

	virtual void RaiseEvent(const Event& event) = 0;
	virtual int PopEvent(Event& event);

	/////////////////////////////////////////////////////////////////////////
	/// Service function

	virtual int GetTestValue()
	{
		return Value;
	}

	const PlatformParameters& GetParameters()
	{
		return _parameters;
	}

	bool SetParameters(PlatformParameters parameters)
	{
		_parameters = parameters;
		return OnSetParameters();
	}


	virtual bool SetAutoMove(bool autoMove) = 0;
	virtual bool Jump(double height, double speed) = 0;
	virtual bool SetSpeed(double speed) = 0;
	virtual bool SetDirection(double direction) = 0;
	virtual bool Rotate(double rotateSpeed) = 0;

	ParametersCmd PopParameterChanges() { ChangeParameters p = _parameterChanges; _parameterChanges.Parameter = PARAM_NO_CHANGES; return p; };
	void SetParameterChanges(ChangeParameters params) { _parameterChanges = params; };



	/////////////////////////////////////////////////////////////////////////
	/// Main process function
	virtual void Start();
	virtual void Stop();

protected:
	int Value = 0;
	PlatformParameters _parameters;
	ChangeParameters _parameterChanges;

	bool IsWorking() { return _parameters.Started && _parameters.Alarm == 0; }
	bool IsStopping() { return _stopping; }
	virtual void run() = 0;
	virtual bool OnSetParameters() { return true; };
	void PushEvent(const Event& event); // Помещаем event очередь, без генерации событий

private:
	//TODO: Change to smartpoint
	std::thread* pTaskThread = 0;
	bool _stopping = false;
	std::queue<Event> _events;
	std::mutex _mutex;

	static void Run(Task* pTask);
};

#endif