#include <thread>

#include "Platform.h"



void Task::Start()
{
	if (pTaskThread)
		return;

	pTaskThread = new std::thread(Run, this);

	Value = 101;
}

void Task::Stop()
{
	if (_stopping || pTaskThread == 0)
		return;

	_stopping = true;

	pTaskThread->join();

	delete pTaskThread;
	pTaskThread = 0;
}

void Task::Run(Task* pTask)
{
	pTask->run();
}

void Task::PushEvent(const Event& event)
{
	std::lock_guard<std::mutex> lock(_mutex);
	_events.push(event);
}

int Task::PopEvent(Event& event)
{
	std::lock_guard<std::mutex> lock(_mutex);

	if (_events.empty())
		return 0;

	event = _events.front();
	_events.pop();

	return _events.size() + 1;
}
