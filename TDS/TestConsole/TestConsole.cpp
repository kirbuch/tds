﻿// TestConsole.cpp : Defines the entry point for the application.
//

#include <thread>

#include "../API/TDSClientAPI.h"
#include "../API/TDSServiceAPI.h"
#include "../API/TDSMessage.h"
#include "TestConsole.h"

//#pragma warning(push)
//// хорошие, годные вакансии: www.abbyy.ru/vacancy
//#pragma warning(disable:9000)
//// код с предупреждением C9000
//#pragma warning(pop)

using namespace std;

bool Auto_horizon()//автоматического приведения в горизонтальное состояние
{
	cout << "Auto_horizon" << endl;
	return true;
}

void TestMessages(TDS_TASK task = 0)
{
	cout << "TestMessages" << endl;

	bool net_mode = task != 0;

	if (task == 0)
		task = StartTask();

	Event event;
	event.msgID = 1;
	event.parameter = 0;
	event.senderID = 1;

	cout << "Raise event" << endl;
	RaiseEvent2(task, event);

	event.msgID = 2;
	event.parameter = 3;
	event.senderID = 4;
	cout << "Raise event 2" << endl;
	RaiseEvent2(task, event);

	event.msgID = 3;
	event.parameter = 4;
	event.senderID = 5;
	cout << "Raise event 3" << endl;
	RaiseEvent2(task, event);

	if (net_mode)
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));

 	int n = 0;
	while ((n = PopEvent(task, event)) > 0)
		cout << "Pop event:" << n << " " << event.msgID << " " << event.senderID << " " << event.parameter << endl;
}

int main()
{
	cout << "Calculate test" << endl;

	cout << "alpha:" << "азимут" << " beta:" << "направление" << " gamma: " << "крен" << " tau: " << "тангаж" << endl;

	int count = 36;
	for (int i = 0; i < count; i++)
	{
		double beta = i * (360.0 / count);
		for (int j = -1; j <= 1; j++)
		{
			double alpha = 9 * j;
			cout << "направление:" << beta << " азимут:" << alpha << " крен: " << CalculateRoll(alpha, beta) << " тангаж: " << CalculatePitch(alpha, beta) << endl;
		}
	}


	//cout << "tau: " << CalculateTau(10, 10) << endl;

	return 0;

	TDS_TASK task = 0;

	task = StartTask("127.0.0.1");
	TestMessages(task);
	//SetAutoMove(task, true);
	//
	//std::cin.get();

	return 0;
	//cout << TDS_MSG_fitehose_take << endl;
	//cout << TDS_MSG_tankvehicle_left_disconnect << endl;
	//cout << TDS_MSG__num << endl;
	//
	//return 0;

	cout << "TestConsole" << endl;
	cout << "TDSClient version " << Version() << endl;
	//TDS_TASK task = StartTask("192.168.0.13");
	//std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	//cout << "Speed: " << Read_speed(task) << endl;
	//cout << "Directiorn: " << Read_direction(task) << endl;

	//cout << "size : " << sizeof(double) << endl;

//	return 0;

	//TDS_TASK task = StartTask("127.0.0.1");

	cout << "Create task: " << task << endl;

	if (task == 0)
		return -1;

	StartPlatform(task);
	cout << "Platform started: " << endl;

	//Write_WaterP(task, 2);
	//cout << "Write_WaterP: 2" << endl;

	double p = Read_WaterP(task);


	//PlatformParameters parameters;
	//parameters.Speed = 123;
	//SetParameters(task, parameters);

	//cout << "Stop task";
	//StopTask(task);

	for (int i = 0; i < 5; i++)
	{
		cout << "Set auto move" << endl;
		SetAutoMove(task, false);
		cin.get();
	}

	Move_azimuth(task, 10);
	Move_azimuth(task, 11);
	Move_azimuth(task, 12);

	//Сервисная функция работает только с тестовой платформой
	SetSpeed(task, 1100);
	//Сервисная функция работает только с тестовой платформой
	SetDirection(task, 123);
	//Сервисная функция работает только с тестовой платформой
	//Rotate(task, 100);

	double motion = 4;
	double height = Read_height(task);
	double target = height + motion;

	Move_height(task, target, 1);
	while (height != target)
	{
		height = Read_height(task);
		cout << (double)clock() / (double)CLOCKS_PER_SEC << " " << time(0) << " height: " << height << " direction:" << Read_direction(task) << " speed: " << Read_speed(task) << endl;
		double p = Read_WaterP(task);
		cout << p << endl;

	}

	target = height - motion;
	Move_height(task, target, 1);
	while (height != target)
	{
		height = Read_height(task);
		cout << (double)clock() / (double)CLOCKS_PER_SEC << " " << time(0) << " height: " << height << " direction:" << Read_direction(task) << " speed: " << Read_speed(task) << endl;
	}

	//Сервисная функция работает только с тестовой платформой
	SetSpeed(task, 900);
	//Сервисная функция работает только с тестовой платформой
	Rotate(task, -100);

	double param = Read_angle_kren(task);
	target = param + motion;
	Move_kren(task, target, 1);
	while (param != target)
	{
		param = Read_angle_kren(task);
		cout << (double)clock() / (double)CLOCKS_PER_SEC << " " << time(0) << " kren: " << param << " direction:" << Read_direction(task) << " speed: " << Read_speed(task) << endl;
	}

	target = param - motion;
	Move_kren(task, target, 1);
	while (param != target)
	{
		param = Read_angle_kren(task);
		cout << (double)clock() / (double)CLOCKS_PER_SEC << " " << time(0) << " kren: " << param << " direction:" << Read_direction(task) << " speed: " << Read_speed(task) << endl;
	}

	param = Read_angle_tangag(task);
	target = param + motion;
	Move_tangag(task, target, 1);
	while (param != target)
	{
		param = Read_angle_tangag(task);
		cout << (double)clock() / (double)CLOCKS_PER_SEC << " " << time(0) << " tangag: " << param << " direction:" << Read_direction(task) << " speed: " << Read_speed(task) << endl;
	}

	target = param - motion;
	Move_tangag(task, target, 1);
	while (param != target)
	{
		param = Read_angle_tangag(task);
		cout << (double)clock() / (double)CLOCKS_PER_SEC << " " << time(0) << " tangag: " << param << " direction:" << Read_direction(task) << " speed: " << Read_speed(task) << endl;
	}


	cout << "Stop task";
	StopTask(task);

	return 0;
}
