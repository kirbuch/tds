#include <cstdlib>
#include <iostream>
#include <thread>

#include <boost/asio.hpp>

using boost::asio::ip::udp;


int server_blocking(int port);
int main_async(int port);
int blocking_client(udp::resolver::query& query);

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		std::cerr << "Usage: TDSDeviceEmulator.exe  <device1 IP> <device2 IP> ....\n";
		//port = atoi(argv[1]);
		return -1;
	}

	for (int i = 1; i < argc; i++)
	{
		char* server = argv[i];
		std::cout << "Connect to device: " << server << std::endl;
		udp::resolver::query query(udp::v4(), server, "4210");
		std::thread* threadClient = new std::thread(blocking_client, query);
	}

	int port = 4444;
	std::thread threadServer(server_blocking, port);
	threadServer.join();

	return 0; // blocking_client("127.0.0.1", "4210");
}


