//
// blocking_udp_echo_client.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2010 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <boost/asio.hpp>

using boost::asio::ip::udp;

enum { max_length = 1024 };

char n = '1';

//int blocking_client(char* host, char* port)
int blocking_client(udp::resolver::query& query)
{
    try
    {
        boost::asio::io_service io_service;

        udp::socket s(io_service, udp::endpoint(udp::v4(), 0));

        udp::resolver resolver(io_service);
//        udp::resolver::query query(udp::v4(), host, port);
        udp::resolver::iterator iterator = resolver.resolve(query);

        using namespace std; // For strlen.
        //std::cout << "Enter message: ";
        //char request[max_length];
        //std::cin.getline(request, max_length);
        char request[14] = "A010000000001";
        request[2] = n;
        n++;

        for (;;)
        {
            size_t request_length = 13; //strlen(request);
            s.send_to(boost::asio::buffer(request, request_length), *iterator);
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        //char reply[max_length];
        //udp::endpoint sender_endpoint;
        //size_t reply_length = s.receive_from(
        //    boost::asio::buffer(reply, max_length), sender_endpoint);
        //std::cout << "Reply is: ";
        //std::cout.write(reply, reply_length);
        //std::cout << "\n";
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}