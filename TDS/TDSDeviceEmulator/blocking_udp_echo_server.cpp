//
// blocking_udp_echo_server.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2010 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdlib>
#include <iostream>
#include <boost/asio.hpp>

using boost::asio::ip::udp;

enum { max_length = 1024 };

void server(boost::asio::io_service& io_service, short port)
{
  udp::socket sock(io_service, udp::endpoint(udp::v4(), port));
  for (;;)
  {
    char data[max_length];
    udp::endpoint sender_endpoint;
    size_t length = sock.receive_from(
        boost::asio::buffer(data, max_length), sender_endpoint);

    data[length] = '\0';

    std::cout << "sender_endpoint:" << sender_endpoint.address() << ":" << sender_endpoint.port();
    std::cout << " recieve:" << data << std::endl;
    //data[length] = '\n';
    //sock.send_to(boost::asio::buffer(data, length+1), sender_endpoint);
  }
}

int server_blocking(int port)
{
  try
  {
    boost::asio::io_service io_service;

    using namespace std; // For atoi.
    server(io_service, port);
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}